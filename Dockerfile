FROM openjdk:11

ADD Deployment/rothis.server.application-0.0.2-SNAPSHOT.jar rothis.server.application-0.0.2-SNAPSHOT.jar

EXPOSE 8081

ENTRYPOINT ["java","-jar","-Dspring.profiles.active=prod","-XX:+UseSerialGC","-Xss512k","-XX:MaxRAM=150m","rothis.server.application-0.0.2-SNAPSHOT.jar"]