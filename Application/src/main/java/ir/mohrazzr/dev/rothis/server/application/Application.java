package ir.mohrazzr.dev.rothis.server.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication(scanBasePackages = "ir.mohrazzr.dev.rothis.server", exclude = {DataSourceAutoConfiguration.class})
@ComponentScan(basePackages = "ir.mohrazzr.dev.rothis.server")
@EnableJpaRepositories(basePackages = "ir.mohrazzr.dev.rothis.server")
@EntityScan(basePackages = "ir.mohrazzr.dev.rothis.server")public class Application {
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
}
