/*
* Data Base Name : RothisDB
* Engine : InnoDB
* Version : 0.0.1
* Description : Create data base struct
**/


#DROP DATAbase RothisDB
#CREATE DATABASE RothisDB;
USE RothisDB;
ALTER DATABASE RothisDB CHARACTER SET utf8 COLLATE utf8_persian_ci;

########################################################################################################################
############### create users and rules #################################################################################
#CREATE USER IF NOT EXISTS 'Admin'@'%' IDENTIFIED BY 'adminpass';
########################################################################################################################
############### begin creation of tables ###############################################################################

CREATE TABLE IF NOT EXISTS RothisDB.tbl_roles
(
  id        INT          NOT NULL AUTO_INCREMENT,
  role_name VARCHAR(100) NOT NULL,
  CONSTRAINT id_tbl_user_roles_pk PRIMARY KEY (id)
)
  ENGINE = InnoDB
  DEFAULT CHARACTER SET utf8
  COLLATE utf8_persian_ci;
CREATE TABLE IF NOT EXISTS RothisDB.tbl_public_ids
(
  id        BIGINT       NOT NULL AUTO_INCREMENT,
  public_id VARCHAR(255) NOT NULL,
  CONSTRAINT id_tbl_public_id_pk PRIMARY KEY (id),
  CONSTRAINT UNIQUE (public_id)
) ENGINE = InnoDB
  DEFAULT CHARACTER SET utf8
  COLLATE utf8_persian_ci;

CREATE TABLE IF NOT EXISTS RothisDB.tbl_users
(
  id                 BIGINT        NOT NULL AUTO_INCREMENT,
  public_id          BIGINT        NULL UNIQUE,
  nick_name          NVARCHAR(100) NULL,
  user_name          VARCHAR(255)  NOT NULL UNIQUE,
  encrypted_password VARCHAR(255)  NOT NULL,
  user_role          INT           NOT NULL,
  CONSTRAINT id_tbl_users_pk PRIMARY KEY (id),
  CONSTRAINT user_role_tbl_users_fk FOREIGN KEY (user_role) REFERENCES RothisDB.tbl_roles (id) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT public_id_tbl_users_fk FOREIGN KEY (public_id) REFERENCES RothisDB.tbl_public_ids (id) ON UPDATE CASCADE ON DELETE CASCADE
)
  ENGINE = InnoDB
  DEFAULT CHARACTER SET utf8
  COLLATE utf8_persian_ci;

CREATE TABLE IF NOT EXISTS RothisDB.tbl_persons
(
  id             BIGINT       NOT NULL AUTO_INCREMENT,
  user_id        BIGINT       NOT NULL,
  first_name     NVARCHAR(20) NOT NULL DEFAULT 'بینام',
  last_name      NVARCHAR(30) NOT NULL DEFAULT 'بینام',
  father_name    NVARCHAR(20) NOT NULL DEFAULT 'بینام',
  national_code  CHAR(20)     NOT NULL,
  pass_port_code VARCHAR(20)  NULL,
  gender         VARCHAR(10)  NOT NULL DEFAULT 'مرد',
  date_of_birth  VARCHAR(20)  NULL,
  CONSTRAINT person_id_tbl_persons_pk PRIMARY KEY (id),
  CONSTRAINT user_id_unique UNIQUE (user_id),
  CONSTRAINT national_code_unique UNIQUE (national_code),
  CONSTRAINT user_id_tbl_persons_fk FOREIGN KEY (user_id) REFERENCES RothisDB.tbl_users (id) ON UPDATE CASCADE ON DELETE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARACTER SET utf8
  COLLATE utf8_persian_ci;

CREATE TABLE IF NOT EXISTS RothisDB.tbl_emails
(
  id                       BIGINT       NOT NULL AUTO_INCREMENT,
  user_id                  BIGINT       NOT NULL,
  email                    VARCHAR(120) NOT NULL UNIQUE,
  is_active                BIT          NOT NULL,
  email_verification_token VARCHAR(255) NULL,
  is_primary               BIT          NOT NULL,
  CONSTRAINT id_tbl_emails_pk PRIMARY KEY (id),
  CONSTRAINT user_id_tbl_emails_fk FOREIGN KEY (user_id) REFERENCES RothisDB.tbl_users (id) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARACTER SET utf8
  COLLATE utf8_persian_ci;
CREATE TABLE IF NOT EXISTS RothisDB.tbl_states
(
  id            INT                                 NOT NULL AUTO_INCREMENT,
  country       INT(11)                             DEFAULT NULL,
  state_name    VARCHAR(64) COLLATE utf8_unicode_ci NOT NULL,
  state_name_en VARCHAR(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  latitude      DECIMAL(10, 8)                      DEFAULT NULL,
  longitude     DECIMAL(11, 8)                      DEFAULT NULL,
  CONSTRAINT id_tbl_states_pk PRIMARY KEY (id)
) ENGINE = InnoDB
  DEFAULT CHARACTER SET utf8
  COLLATE utf8_persian_ci;

CREATE TABLE IF NOT EXISTS RothisDB.tbl_cities
(
  id           INT                                 NOT NULL AUTO_INCREMENT,
  state_code   INT                                 NOT NULL,
  city_name    VARCHAR(64) COLLATE utf8_unicode_ci NOT NULL,
  city_name_en VARCHAR(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  latitude     DECIMAL(10, 8)                      DEFAULT NULL,
  longitude    DECIMAL(11, 8)                      DEFAULT NULL,
  CONSTRAINT id_tbl_cities_pk PRIMARY KEY (id),
  CONSTRAINT state_code_tbl_cities FOREIGN KEY (state_code) REFERENCES RothisDB.tbl_states (id) ON DELETE CASCADE ON UPDATE CASCADE

) ENGINE = InnoDB
  DEFAULT CHARACTER SET utf8
  COLLATE utf8_persian_ci;



CREATE TABLE IF NOT EXISTS RothisDB.tbl_addresses
(
  id            BIGINT NOT NULL AUTO_INCREMENT,
  persons_id    BIGINT NOT NULL,
  public_id     BIGINT NULL UNIQUE,
  is_primary    BIT    NOT NULL,
  city_code     INT    NOT NULL,
  postal_code   CHAR(30),
  branchAddress TEXT,
  CONSTRAINT id_tbl_addresses_pk PRIMARY KEY (id),
  CONSTRAINT persons_id_tbl_addresses_fk FOREIGN KEY (persons_id) REFERENCES RothisDB.tbl_persons (id) ON UPDATE CASCADE ON DELETE CASCADE,
  CONSTRAINT city_code_addresses_fk FOREIGN KEY (city_code) REFERENCES RothisDB.tbl_cities (id) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT public_id_tbl_addresses_fk FOREIGN KEY (public_id) REFERENCES RothisDB.tbl_public_ids (id) ON UPDATE CASCADE ON DELETE CASCADE


) ENGINE = InnoDB
  DEFAULT CHARACTER SET utf8
  COLLATE utf8_persian_ci;



CREATE TABLE IF NOT EXISTS RothisDB.tbl_phone_number_types
(
  id         INT          NOT NULL AUTO_INCREMENT,
  type_name  NVARCHAR(20) NOT NULL,
  type_value NVARCHAR(50) NOT NULL,
  CONSTRAINT id_tbl_phone_number_types PRIMARY KEY (id)

) ENGINE = InnoDB
  DEFAULT CHARACTER SET utf8
  COLLATE utf8_persian_ci;


CREATE TABLE IF NOT EXISTS RothisDB.tbl_phone_numbers
(
  id           BIGINT NOT NULL AUTO_INCREMENT,
  persons_id   BIGINT NOT NULL,
  public_id    BIGINT NULL UNIQUE,
  is_primary   BIT    NOT NULL,
  phone_number NVARCHAR(30),
  type_code    INT    NOT NULL,
  CONSTRAINT id_tbl_phone_numbers PRIMARY KEY (id),
  CONSTRAINT persons_id_tbl_phone_numbers_fk FOREIGN KEY (persons_id) REFERENCES RothisDB.tbl_persons (id) ON UPDATE CASCADE ON DELETE CASCADE,
  CONSTRAINT type_code_tbl_numbers FOREIGN KEY (type_code) REFERENCES tbl_phone_number_types (id) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT public_id_tbl_phone_numbers_fk FOREIGN KEY (public_id) REFERENCES RothisDB.tbl_public_ids (id) ON UPDATE CASCADE ON DELETE CASCADE

) ENGINE = InnoDB
  DEFAULT CHARACTER SET utf8
  COLLATE utf8_persian_ci;
CREATE TABLE IF NOT EXISTS RothisDB.tbl_resources_address
(
  id           BIGINT NOT NULL AUTO_INCREMENT,
  public_id    BIGINT NOT NULL UNIQUE,
  city_code    INT    NOT NULL,
  full_address TEXT   NOT NULL,
  latitude     DECIMAL(10, 8) DEFAULT NULL,
  longitude    DECIMAL(11, 8) DEFAULT NULL,
  CONSTRAINT PRIMARY KEY (id),
  CONSTRAINT city_code_resources_address_fk FOREIGN KEY (city_code) REFERENCES RothisDB.tbl_cities (id) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT public_id_resources_address_fk FOREIGN KEY (public_id) REFERENCES RothisDB.tbl_public_ids (id) ON UPDATE CASCADE ON DELETE CASCADE

) ENGINE = InnoDB
  DEFAULT CHARACTER
    SET utf8
  COLLATE utf8_persian_ci;
CREATE TABLE IF NOT EXISTS RothisDB.tbl_resources
(
  id                                 BIGINT               NOT NULL AUTO_INCREMENT,
  public_id                          BIGINT               NULL UNIQUE,
  identifier_code                    NVARCHAR(100) UNIQUE NULL,
  name                               NVARCHAR(250)        NOT NULL,
  resource_address_id                BIGINT               NULL UNIQUE,
  total_resource_portion             FLOAT                NOT NULL,
  total_unallocated_resource_portion FLOAT                NOT NULL,
  dong_value                         INT                  NOT NULL,
  total_resource_seconds             BIGINT               NOT NULL,
  resource_seconds_per_portion       BIGINT               NOT NULL,
  circuit_of_the_day                 INT                  NOT NULL,
  CONSTRAINT id_tbl_resources_pk PRIMARY KEY (id),
  CONSTRAINT address_code_tbl_resources_fk FOREIGN KEY (resource_address_id) REFERENCES RothisDB.tbl_resources_address (id) ON UPDATE CASCADE ON DELETE CASCADE,
  CONSTRAINT public_id_tbl_resources_fk FOREIGN KEY (public_id) REFERENCES RothisDB.tbl_public_ids (id) ON UPDATE CASCADE ON DELETE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARACTER SET utf8
  COLLATE utf8_persian_ci;

CREATE TABLE IF NOT EXISTS RothisDB.tbl_resource_managers
(
  id          BIGINT NOT NULL AUTO_INCREMENT,
  person_id   BIGINT NOT NULL,
  resource_id BIGINT NOT NULL,
  CONSTRAINT id_tbl_resource_managers_pk PRIMARY KEY (id),
  CONSTRAINT person_id_tbl_resource_managers_fk FOREIGN KEY (person_id) REFERENCES RothisDB.tbl_persons (id) ON UPDATE CASCADE ON DELETE RESTRICT,
  CONSTRAINT resource_id_tbl_resource_managers_fk FOREIGN KEY (resource_id) REFERENCES RothisDB.tbl_resources (id) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARACTER SET utf8
  COLLATE utf8_persian_ci;

CREATE TABLE IF NOT EXISTS RothisDB.tbl_bank_accounts
(
  id             BIGINT        NOT NULL AUTO_INCREMENT,
  public_id      BIGINT        NULL UNIQUE,
  resource_id    BIGINT        NOT NULL,
  bank_name      NVARCHAR(200) NOT NULL,
  account_number VARCHAR(50)   NOT NULL,
  name           NVARCHAR(100) NOT NULL,
  shaba_number   NVARCHAR(500) NULL,
  card_number    NVARCHAR(100) NULL,
  branch_code    VARCHAR(20),
  branch_name    NVARCHAR(50),
  branch_address TEXT          NULL,
  description    NVARCHAR(300),
  CONSTRAINT id_tbl_bank_accounts_pk PRIMARY KEY (id),
  CONSTRAINT resource_code_tbl_bank_accounts_fk FOREIGN KEY (resource_id) REFERENCES RothisDB.tbl_resources (id) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT public_id_tbl_bank_accounts_fk FOREIGN KEY (public_id) REFERENCES RothisDB.tbl_public_ids (id) ON UPDATE CASCADE ON DELETE CASCADE


) ENGINE = InnoDB
  DEFAULT CHARACTER SET utf8
  COLLATE utf8_persian_ci;
CREATE TABLE IF NOT EXISTS RothisDB.tbl_bank_account_owners
(
  id              BIGINT AUTO_INCREMENT,
  bank_account_id BIGINT NOT NULL,
  user_id         BIGINT NOT NULL,
  CONSTRAINT PRIMARY KEY (id),
  CONSTRAINT bank_account_id_tbl_bank_account_owners_pk FOREIGN KEY (bank_account_id) REFERENCES RothisDB.tbl_bank_accounts (id) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT user_id_tbl_bank_account_owners_fk FOREIGN KEY (user_id) REFERENCES RothisDB.tbl_users (id) ON UPDATE CASCADE ON DELETE NO ACTION
) ENGINE = InnoDB
  DEFAULT CHARACTER SET utf8
  COLLATE utf8_persian_ci;

CREATE TABLE IF NOT EXISTS RothisDB.tbl_portion_owners
(
  id                      BIGINT       NOT NULL AUTO_INCREMENT,
  public_id               BIGINT       NULL UNIQUE,
  resource_id             BIGINT       NOT NULL,
  person_id               BIGINT       NOT NULL,
  portion_identifier_code NVARCHAR(20) NOT NULL,
  portion_value           FLOAT        NOT NULL,
  portion_seconds         BIGINT       NOT NULL,
  CONSTRAINT id_tbl_portion_owners_pk PRIMARY KEY (id),
  CONSTRAINT resource_code_tbl_portion_owners_fk FOREIGN KEY (resource_id) REFERENCES RothisDB.tbl_resources (id) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT person_id_tbl_portion_owners_fk FOREIGN KEY (person_id) REFERENCES RothisDB.tbl_persons (id) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT public_id_tbl_portion_owners_fk FOREIGN KEY (public_id) REFERENCES RothisDB.tbl_public_ids (id) ON UPDATE CASCADE ON DELETE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARACTER SET utf8
  COLLATE utf8_persian_ci;


CREATE TABLE IF NOT EXISTS RothisDB.tbl_resource_bills
(
  id                          BIGINT          NOT NULL AUTO_INCREMENT,
  public_id                   BIGINT          NULL UNIQUE,
  resource_id                 BIGINT          NOT NULL,
  bank_account_id             BIGINT          NOT NULL,
  total_price                 DECIMAL(65, 30) NOT NULL,
  price_per_second            DECIMAL(65, 30) NOT NULL,
  bill_count                  BIGINT          NULL,
  resource_bill_identity_code NVARCHAR(20)    NOT NULL,
  round_count                 INT             NOT NULL,
  export_time                 TIMESTAMP       NOT NULL,
  concern                     NVARCHAR(200)   NOT NULL,
  descriptions                NVARCHAR(300)   NULL,
  CONSTRAINT id_tbl_bill_of_exchanges_pk PRIMARY KEY (id),
  CONSTRAINT resource_id_tbl_resource_bills_fk FOREIGN KEY (resource_id) REFERENCES RothisDB.tbl_resources (id) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT public_id_tbl_resource_bills_fk FOREIGN KEY (public_id) REFERENCES RothisDB.tbl_public_ids (id) ON UPDATE CASCADE ON DELETE CASCADE,
  CONSTRAINT bank_account_id_tbl_resource_bills_fk FOREIGN KEY (bank_account_id) REFERENCES RothisDB.tbl_bank_accounts (id) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARACTER SET utf8
  COLLATE utf8_persian_ci;

CREATE TABLE IF NOT EXISTS RothisDB.tbl_bills
(
  id                 BIGINT          NOT NULL AUTO_INCREMENT,
  public_id          BIGINT          NULL UNIQUE,
  resource_bills_id  BIGINT          NOT NULL,
  portion_owner_id   BIGINT          NOT NULL,
  createdAt          TIMESTAMP       NOT NULL,
  bill_price         DECIMAL(65, 30) NOT NULL,
  bill_identity_code NVARCHAR(20)    NOT NULL,
  bill_number        INT,
  CONSTRAINT id_tbl_bills_pk PRIMARY KEY (id),
  CONSTRAINT resource_bills_id_tbl_bills_fk FOREIGN KEY (resource_bills_id) REFERENCES RothisDB.tbl_resource_bills (id) ON UPDATE CASCADE ON DELETE CASCADE,
  CONSTRAINT portion_owner_code_tbl_bills_fk FOREIGN KEY (portion_owner_id) REFERENCES RothisDB.tbl_portion_owners (id) ON UPDATE CASCADE ON DELETE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARACTER SET utf8
  COLLATE utf8_persian_ci;



CREATE TABLE IF NOT EXISTS RothisDB.tbl_employees
(
  id                     BIGINT NOT NULL AUTO_INCREMENT,
  user_id                BIGINT NOT NULL,
  resource_code          BIGINT NOT NULL,
  start_work_time_in_day TIME   NULL,
  end_work_time_in_day   TIME   NULL,
  start_work_date        TIME   NULL,
  end_work_date          TIME   NULL,
  CONSTRAINT id_tbl_employees_pk PRIMARY KEY (id),
  CONSTRAINT user_id_tbl_employees_fk FOREIGN KEY (user_id) REFERENCES RothisDB.tbl_users (id) ON UPDATE CASCADE ON DELETE CASCADE,
  CONSTRAINT resource_code_tbl_employees_fk FOREIGN KEY (resource_code) REFERENCES RothisDB.tbl_resources (id) ON UPDATE CASCADE ON DELETE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARACTER SET utf8
  COLLATE utf8_persian_ci;
