package ir.mohrazzr.dev.rothis.server.api.config.mysql;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.*;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.*;
import org.springframework.core.env.Environment;
import org.springframework.core.type.AnnotatedTypeMetadata;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.util.ClassUtils;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.Arrays;
import java.util.Objects;
import java.util.Properties;

/**
 * Created by IntelliJ IDEA at Tuesday in 2019/10/29 - 8:09 PM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : Rothis
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Email : mhrz.dev@gmail.com
 * Class Name          : MysqlConfiguration
 **/

@Configuration
@ConditionalOnClass(DataSource.class)
@PropertySources(
        {@PropertySource( value = "classpath:properties/mysql.properties"),
        @PropertySource( value = "classpath:properties/hibernate.properties")})
@EnableTransactionManagement
public class MysqlConfiguration {
    private final Environment env;

    @Autowired
    public MysqlConfiguration(Environment env) {
        this.env = env;
    }


    @Bean(name = "dataSource")
    @ConfigurationProperties(prefix = "rothis.datasource")
    @ConditionalOnProperty(name = "rothis.datasource.mysql.use", havingValue = "local")
    public DataSource dataSource() {
        return DataSourceBuilder.create()
                .driverClassName(Objects.requireNonNull(env.getProperty("rothis.datasource.mysql.driver")))
                .url(Objects.requireNonNull(env.getProperty("rothis.datasource.mysql.url")))
                .username(env.getProperty("rothis.datasource.mysql.username") != null ? env.getProperty("rothis.datasource.mysql.username") : "")
                .password(env.getProperty("rothis.datasource.mysql.password") != null ? env.getProperty("rothis.datasource.mysql.password") : "")
                .build();
    }

    @Bean
    @DependsOn("dataSource")
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
        final LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource(dataSource());
        em.setPackagesToScan("ir.mohrazzr.dev.rothis.server");
        em.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        if (additionalProperties() != null) {
            em.setJpaProperties(additionalProperties());
        }
        return em;
    }

    @Bean
    @ConditionalOnBean(name = "entityManagerFactory")
    public JpaTransactionManager transactionManager(final EntityManagerFactory entityManagerFactory) {
        final JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory);
        return transactionManager;
    }
    @ConditionalOnResource(resources = "classpath:properties/hibernate.properties")
    @Conditional(HibernateCondition.class)
    private Properties additionalProperties() {
        final Properties hibernateProperties = new Properties();
        hibernateProperties.setProperty("hibernate.hbm2ddl.auto", env.getProperty("rothis.datasource.mysql.hibernate.hbm2ddl.auto"));
        hibernateProperties.setProperty("hibernate.dialect", env.getProperty("rothis.datasource.mysql.hibernate.dialect"));
        return hibernateProperties;
    }

    static class HibernateCondition extends SpringBootCondition {
        private static final String[] CLASS_NAMES = {"org.hibernate.ejb.HibernateEntityManager", "org.hibernate.jpa.HibernateEntityManager"};

        @Override
        public ConditionOutcome getMatchOutcome(ConditionContext context, AnnotatedTypeMetadata metadata) {
            ConditionMessage.Builder message = ConditionMessage.forCondition("Hibernate");
            return Arrays.stream(CLASS_NAMES).filter(className -> ClassUtils.isPresent(className, context.getClassLoader())).map(className -> ConditionOutcome.match(message.found("class").items(ConditionMessage.Style.NORMAL, className))).findAny()
                    .orElseGet(() -> ConditionOutcome.noMatch(message.didNotFind("class", "classes").items(ConditionMessage.Style.NORMAL, Arrays.asList(CLASS_NAMES))));
        }
    }
}
