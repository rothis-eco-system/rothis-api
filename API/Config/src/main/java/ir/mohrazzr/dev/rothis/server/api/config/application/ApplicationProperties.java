package ir.mohrazzr.dev.rothis.server.api.config.application;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.util.Objects;

/**
 * Created by IntelliJ IDEA at Sunday in 2019/09/15 - 8:04 PM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : Addrephone
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Email : mhrz.dev@gmail.com
 * Class Name          : AppProperties
 **/
@Component
@PropertySources({@PropertySource(value = "classpath:security/tokens.properties"), @PropertySource(value = "classpath:security/constants.properties")})
public class ApplicationProperties {
    @Autowired
    private Environment environment;

    public String getTokenSecretProperty() {
        return Objects.requireNonNull(environment.getProperty("rothis.application.token.secret"));
    }

    public String getTokenPrefixProperty() {
        return Objects.requireNonNull(environment.getProperty("rothis.security.constants.token.prefix"));

    }

    public String getExpirationTimeProperty() {
        return Objects.requireNonNull(environment.getProperty("rothis.security.constants.expiration.time"));

    }

    public String getHeaderStringProperty() {
        return Objects.requireNonNull(environment.getProperty("rothis.security.constants.header.authorization"));
    }
}
