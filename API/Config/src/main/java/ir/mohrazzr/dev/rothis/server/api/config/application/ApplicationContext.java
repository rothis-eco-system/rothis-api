package ir.mohrazzr.dev.rothis.server.api.config.application;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

/**
 * Created by IntelliJ IDEA at Saturday in 2019/11/02 - 9:32 PM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : Rothis
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Email : mhrz.dev@gmail.com
 * Class Name          : AppContext
 **/
@Component
public class ApplicationContext {

    @Bean
    public SpringApplicationContext springApplicationContext(){return new SpringApplicationContext();}
    @Bean(name = "applicationProperties")
    public ApplicationProperties appProperties(){return new ApplicationProperties();}
}
