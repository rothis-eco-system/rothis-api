package ir.mohrazzr.dev.rothis.server.api.security.controller;

/**
 * Created by IntelliJ IDEA at Wednesday in 2019/11/06 - 11:10 AM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : Rothis
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Email : mhrz.dev@gmail.com
 * Class Name          : JwtAuthenticationController
 **/


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import ir.mohrazzr.dev.rothis.server.api.billing.presentation.controller.router.ROUTES;
import ir.mohrazzr.dev.rothis.server.api.billing.presentation.model.request.UserLoginRequest;
import ir.mohrazzr.dev.rothis.server.api.billing.presentation.model.response.UserLoginResponse;
import ir.mohrazzr.dev.rothis.server.api.security.validator.JwtTokenUtil;
import ir.mohrazzr.dev.rothis.server.api.billing.service.service.iservice.IPublicIDsService;
import ir.mohrazzr.dev.rothis.server.api.billing.service.service.iservice.IUsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
@CrossOrigin
@Api(value="JwtAuthenticationController", description="Json Web Token Authentication")
public class JwtAuthenticationController {
    private final AuthenticationManager authenticationManager;
    private final JwtTokenUtil jwtTokenUtil;
    private final IUsersService usersService;
    private final IPublicIDsService publicIDsService;

    @Autowired
    public JwtAuthenticationController(AuthenticationManager authenticationManager, JwtTokenUtil jwtTokenUtil, IUsersService usersService, IPublicIDsService publicIDsService) {
        this.authenticationManager = authenticationManager;
        this.jwtTokenUtil = jwtTokenUtil;
        this.usersService = usersService;
        this.publicIDsService = publicIDsService;
    }
    @ApiOperation(value = "Create Authentication Token", response = ResponseEntity.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @RequestMapping(value = ROUTES.SIGN_IN_URL, method = RequestMethod.POST)
    public ResponseEntity<?> createAuthenticationToken(@RequestBody UserLoginRequest request) throws Exception {
        authenticate(request.getEmail(), request.getPassword());
        final UserDetails userDetails = usersService
                .loadUserByUsername(request.getEmail());
        final String token = jwtTokenUtil.generateToken(userDetails);
        final String publicUserId = usersService.findUserByUserName(userDetails.getUsername()).getPublicUserIdString();
        return ResponseEntity.ok(new UserLoginResponse(publicUserId,token));
    }

    private void authenticate(String username, String password) throws Exception {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
                    username,
                    password,
                    new ArrayList<>()));
        } catch (DisabledException e) {
            throw new Exception("USER_DISABLED", e);
        } catch (BadCredentialsException e) {
            throw new Exception("INVALID_CREDENTIALS", e);
        }
    }
}
