package ir.mohrazzr.dev.rothis.server.api.security.constants;


import ir.mohrazzr.dev.rothis.server.api.billing.presentation.controller.router.ROUTES;
import ir.mohrazzr.dev.rothis.server.api.config.application.ApplicationProperties;
import ir.mohrazzr.dev.rothis.server.api.config.application.SpringApplicationContext;
import org.springframework.stereotype.Component;

/**
 * Created by IntelliJ IDEA at Sunday in 2019/11/03 - 8:54 AM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : Rothis
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Email : mhrz.dev@gmail.com
 * Class Name          : SecurityConstants
 **/
@Component
public class SecurityConstants {
    public static final String SIGN_UP_URL = ROUTES.SIGN_UP_URL;
    public static final String SIGN_IN_URL = ROUTES.SIGN_IN_URL;


    public String getExpirationTime() {
        ApplicationProperties properties = (ApplicationProperties) SpringApplicationContext.getBean("applicationProperties");
        return properties.getExpirationTimeProperty();

    }

    public String getTokenSecret() {
        ApplicationProperties properties = (ApplicationProperties) SpringApplicationContext.getBean("applicationProperties");

        return properties.getTokenSecretProperty();
    }

    public String getTokenPrefix() {
        ApplicationProperties properties = (ApplicationProperties) SpringApplicationContext.getBean("applicationProperties");

        return properties.getTokenPrefixProperty();
    }

    public String getHeaderString() {
        ApplicationProperties properties = (ApplicationProperties) SpringApplicationContext.getBean("applicationProperties");

        return properties.getHeaderStringProperty();

    }
}
