package ir.mohrazzr.dev.rothis.server.api.billing.presentation.controller.rest;

import ir.mohrazzr.dev.rothis.server.api.billing.presentation.model.request.EmailsRequest;
import ir.mohrazzr.dev.rothis.server.api.billing.presentation.model.response.EmailsResponse;
import ir.mohrazzr.dev.rothis.server.api.billing.presentation.controller.router.ROUTES;
import ir.mohrazzr.dev.rothis.server.api.billing.service.DTO.EmailsDTO;
import ir.mohrazzr.dev.rothis.server.api.billing.service.service.iservice.IEmailsService;
import org.springframework.beans.BeanUtils;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by IntelliJ IDEA at Wednesday in 2019/11/06 - 11:54 PM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : Rothis
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Email : mhrz.dev@gmail.com
 * Class Name          : EmailController
 **/
@RestController
@RequestMapping(value = ROUTES.USER_EMAIL_URL)
public class EmailsController {
    private final IEmailsService emailsService;

    public EmailsController(IEmailsService iEmailsService) {
        this.emailsService = iEmailsService;
    }

    @PostMapping(
            consumes = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
    public EmailsResponse createUser(@RequestBody EmailsRequest emailsRequest) throws Exception {
        EmailsResponse response = new EmailsResponse();
        EmailsDTO emailsDTO = new EmailsDTO();
        BeanUtils.copyProperties(emailsRequest, emailsDTO);
        EmailsDTO resultDTO = emailsService.createEmail(emailsDTO, emailsRequest.getPublicUserId());
        BeanUtils.copyProperties(resultDTO, response);
        response.setPublicUserId(emailsRequest.getPublicUserId());
        return response;
    }
}
