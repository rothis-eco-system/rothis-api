package ir.mohrazzr.dev.rothis.server.api.billing.presentation.model.response;

import java.util.List;

/**
 * Created by IntelliJ IDEA at Friday in 2019/11/08 - 7:44 PM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : Rothis
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Email : mhrz.dev@gmail.com
 * Class Name          : PersonsExtraResponse
 **/

public class PersonsExtraResponse {
    private String publicUserId;
    private String userName;
    private String firstName;
    private String lastName;
    private String fatherName;
    private String nationalCode;
    private String passPortCode;
    private String gender;
    private String dateOfBirth;
    private List<AddressesResponse> addresses;
    private List<PhoneNumbersResponse> phoneNumbers;

    public PersonsExtraResponse() {
    }

    public PersonsExtraResponse(String publicUserId, String userName, String firstName, String lastName, String fatherName, String nationalCode, String passPortCode, String gender, String dateOfBirth, List<AddressesResponse> addresses, List<PhoneNumbersResponse> phoneNumbers) {
        this.publicUserId = publicUserId;
        this.userName = userName;
        this.firstName = firstName;
        this.lastName = lastName;
        this.fatherName = fatherName;
        this.nationalCode = nationalCode;
        this.passPortCode = passPortCode;
        this.gender = gender;
        this.dateOfBirth = dateOfBirth;
        this.addresses = addresses;
        this.phoneNumbers = phoneNumbers;
    }

    public String getPublicUserId() {
        return publicUserId;
    }

    public void setPublicUserId(String publicUserId) {
        this.publicUserId = publicUserId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFatherName() {
        return fatherName;
    }

    public void setFatherName(String fatherName) {
        this.fatherName = fatherName;
    }

    public String getNationalCode() {
        return nationalCode;
    }

    public void setNationalCode(String nationalCode) {
        this.nationalCode = nationalCode;
    }

    public String getPassPortCode() {
        return passPortCode;
    }

    public void setPassPortCode(String passPortCode) {
        this.passPortCode = passPortCode;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public List<AddressesResponse> getAddresses() {
        return addresses;
    }

    public void setAddresses(List<AddressesResponse> addresses) {
        this.addresses = addresses;
    }

    public List<PhoneNumbersResponse> getPhoneNumbers() {
        return phoneNumbers;
    }

    public void setPhoneNumbers(List<PhoneNumbersResponse> phoneNumbers) {
        this.phoneNumbers = phoneNumbers;
    }
}
