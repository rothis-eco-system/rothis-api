package ir.mohrazzr.dev.rothis.server.api.billing.presentation.controller.rest;

import ir.mohrazzr.dev.rothis.server.api.billing.presentation.controller.router.ROUTES;
import ir.mohrazzr.dev.rothis.server.api.billing.reporter.model.ResourceBillsReportResponse;
import ir.mohrazzr.dev.rothis.server.api.billing.reporter.service.iservice.IReportResourceBillsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

/**
 * Created by IntelliJ IDEA at Tuesday in 2019/12/31 - 10:59 AM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : Rothis
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Email : mhrz.dev@gmail.com
 * Class Name          : ReporterController
 **/

@RestController
@RequestMapping(path = ROUTES.RESOURCES_BILLS_LIST_REPORT_URL)
public class ReporterController {
    private IReportResourceBillsService reportResourceBillsService;

    @Autowired
    public ReporterController(IReportResourceBillsService reportResourceBillsService) {
        this.reportResourceBillsService = reportResourceBillsService;
    }

    @GetMapping(path = "/{resourcePublicId}/{managerPublicUserId}/bill",
            produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
    public ResourceBillsReportResponse readResourceBill(@PathVariable(value = "resourcePublicId") String resourcePublicId,
                                                        @PathVariable(value = "managerPublicUserId") String managerPublicUserId,
                                                        @RequestParam(value = "resourceBillsPublicId", defaultValue = "null") String resourceBillsPublicId,
                                                        @RequestParam(value = "resourceBillIdentityCode", defaultValue = "null") String resourceBillIdentityCode,
                                                        @RequestParam(value = "reportTemplatesPath") String reportTemplatesPath,
                                                        @RequestParam(value = "generatedReportPath") String generatedReportPath,
                                                        @RequestParam(value = "billIdentityCode") String billIdentityCode,
                                                        @RequestParam(value = "message") String message
    ) throws Exception {
        return reportResourceBillsService.generateResourceBillsListReport(resourcePublicId, managerPublicUserId, resourceBillsPublicId, resourceBillIdentityCode, reportTemplatesPath, generatedReportPath,billIdentityCode, message);
    }
}
