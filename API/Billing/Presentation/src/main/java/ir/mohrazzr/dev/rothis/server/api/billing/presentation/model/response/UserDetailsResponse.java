package ir.mohrazzr.dev.rothis.server.api.billing.presentation.model.response;

/**
 * Created by IntelliJ IDEA at Wednesday in 2019/10/30 - 10:58 PM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : Rothis
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @userName : mhrz.dev@gmail.com
 * Class Name          : UserDetailsResponse
 **/

public class UserDetailsResponse {

    private String publicUserIdString;
    private String nickName;
    private String userName;
    private String email;
    private Integer userRole;

    public UserDetailsResponse() {

    }

    public UserDetailsResponse(String publicUserIdString, String nickName, String userName, String email, Integer userRole) {
        this.publicUserIdString = publicUserIdString;
        this.nickName = nickName;
        this.userName = userName;
        this.userRole = userRole;
        this.email = email;
    }

    public String getPublicUserIdString() {
        return publicUserIdString;
    }

    public void setPublicUserIdString(String publicUserIdString) {
        this.publicUserIdString = publicUserIdString;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getUserRole() {
        return userRole;
    }

    public void setUserRole(Integer userRole) {
        this.userRole = userRole;
    }
}
