package ir.mohrazzr.dev.rothis.server.api.billing.presentation.model.response;

import java.util.List;

/**
 * Created by IntelliJ IDEA at Thursday in 2019/11/21 - 12:11 PM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : Rothis
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Email : mhrz.dev@gmail.com
 * Class Name          : BankAccountsResponse
 **/

public class BankAccountsResponse {
    private String publicIdString;
    private String resourcePublicId;
    private String bankName;
    private String accountNumber;
    private String name;
    private String shabaNumber;
    private String cardNumber;
    private String branchCode;
    private String branchName;
    private String branchAddress;
    private String description;
    private List<PersonsResponse> owners;

    public BankAccountsResponse() {
    }

    public BankAccountsResponse(String publicIdString, String resourcePublicId, String bankName, String accountNumber, String name, String shabaNumber, String cardNumber, String branchCode, String branchName, String branchAddress, String description, List<PersonsResponse> owners) {
        this.publicIdString = publicIdString;
        this.resourcePublicId = resourcePublicId;
        this.bankName = bankName;
        this.accountNumber = accountNumber;
        this.name = name;
        this.shabaNumber = shabaNumber;
        this.cardNumber = cardNumber;
        this.branchCode = branchCode;
        this.branchName = branchName;
        this.branchAddress = branchAddress;
        this.description = description;
        this.owners = owners;
    }

    public String getPublicIdString() {
        return publicIdString;
    }

    public void setPublicIdString(String publicIdString) {
        this.publicIdString = publicIdString;
    }

    public String getResourcePublicId() {
        return resourcePublicId;
    }

    public void setResourcePublicId(String resourcePublicId) {
        this.resourcePublicId = resourcePublicId;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShabaNumber() {
        return shabaNumber;
    }

    public void setShabaNumber(String shabaNumber) {
        this.shabaNumber = shabaNumber;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getBranchCode() {
        return branchCode;
    }

    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String getBranchAddress() {
        return branchAddress;
    }

    public void setBranchAddress(String branchAddress) {
        this.branchAddress = branchAddress;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<PersonsResponse> getOwners() {
        return owners;
    }

    public void setOwners(List<PersonsResponse> owners) {
        this.owners = owners;
    }
}
