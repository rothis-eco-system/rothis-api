package ir.mohrazzr.dev.rothis.server.api.billing.presentation.model.response;

/**
 * Created by IntelliJ IDEA at Friday in 2019/11/08 - 7:46 PM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : Rothis
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Email : mhrz.dev@gmail.com
 * Class Name          : PhoneNumbersResponse
 **/

public class PhoneNumbersResponse {
    private String publicPhoneNumberId;
    private Boolean isPrimary;
    private String phoneNumber;
    private String type;

    public PhoneNumbersResponse() {
    }

    public PhoneNumbersResponse(String publicPhoneNumberId, Boolean isPrimary, String phoneNumber, String type) {
        this.publicPhoneNumberId = publicPhoneNumberId;
        this.isPrimary = isPrimary;
        this.phoneNumber = phoneNumber;
        this.type = type;
    }

    public String getPublicPhoneNumberId() {
        return publicPhoneNumberId;
    }

    public void setPublicPhoneNumberId(String publicPhoneNumberId) {
        this.publicPhoneNumberId = publicPhoneNumberId;
    }

    public Boolean getPrimary() {
        return isPrimary;
    }

    public void setPrimary(Boolean primary) {
        isPrimary = primary;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
