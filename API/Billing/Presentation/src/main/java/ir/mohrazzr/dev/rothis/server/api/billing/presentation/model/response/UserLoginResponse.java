package ir.mohrazzr.dev.rothis.server.api.billing.presentation.model.response;

/**
 * Created by IntelliJ IDEA at Wednesday in 2019/11/06 - 11:14 AM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : Rothis
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Email : mhrz.dev@gmail.com
 * Class Name          : UserLoginResponse
 **/

import java.io.Serializable;

public class UserLoginResponse implements Serializable {
    private static final long serialVersionUID = -8091879091924046844L;
    private final String publicUserId;
    private final String token;

    public UserLoginResponse(String publicUserId, String token) {
        this.token = token;
        this.publicUserId = publicUserId;
    }

    public String getToken() {
        return token;
    }
    public String getPublicUserId() {
        return publicUserId;
    }
}