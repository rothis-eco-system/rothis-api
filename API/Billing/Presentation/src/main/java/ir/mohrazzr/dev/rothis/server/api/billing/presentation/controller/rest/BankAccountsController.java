package ir.mohrazzr.dev.rothis.server.api.billing.presentation.controller.rest;

import ir.mohrazzr.dev.rothis.server.api.billing.presentation.model.request.BankAccountsRequest;
import ir.mohrazzr.dev.rothis.server.api.billing.presentation.model.response.BankAccountsResponse;
import ir.mohrazzr.dev.rothis.server.api.billing.presentation.model.response.opreations.OperationStatusResponse;
import ir.mohrazzr.dev.rothis.server.api.billing.presentation.controller.router.ROUTES;
import ir.mohrazzr.dev.rothis.server.api.billing.service.DTO.BankAccountsDTO;
import ir.mohrazzr.dev.rothis.server.api.billing.service.DTO.OperationStatusDTO;
import ir.mohrazzr.dev.rothis.server.api.billing.service.service.iservice.IBankAccountsService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by IntelliJ IDEA at Thursday in 2019/11/21 - 12:49 PM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : Rothis
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Email : mhrz.dev@gmail.com
 * Class Name          : BankAccountsController
 **/
@RestController
@RequestMapping(value = ROUTES.BANK_ACCOUNTS_URL)
public class BankAccountsController {
    private IBankAccountsService bankAccountsService;
    private ModelMapper modelMapper;

    public BankAccountsController(IBankAccountsService bankAccountsService) {
        this.bankAccountsService = bankAccountsService;
        this.modelMapper = new ModelMapper();
    }

    @PostMapping(path = "/{resourcePublicId}/{managerPublicUserId}/account/new",
            consumes = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
    public BankAccountsResponse createBankAccount(@RequestBody BankAccountsRequest bankAccountsRequest,
                                                  @PathVariable(value = "resourcePublicId") String resourcePublicId,
                                                  @PathVariable(value = "managerPublicUserId") String managerPublicUserId
    ) throws Exception {
        BankAccountsResponse bankAccountsResponse = new BankAccountsResponse();
        BankAccountsDTO bankAccountsDTO = new BankAccountsDTO();
        modelMapper.map(bankAccountsRequest, bankAccountsDTO);
        BankAccountsDTO resultBankAccountsDTO = bankAccountsService.createBankAccount(bankAccountsDTO, managerPublicUserId, resourcePublicId);
        modelMapper.map(resultBankAccountsDTO, bankAccountsResponse);
        return bankAccountsResponse;
    }
    @PostMapping(path = "/{resourcePublicId}/{managerPublicUserId}/account/{bankAccountPublicId}/owner/new",
            consumes = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
    public BankAccountsResponse createBankAccountOwner(@PathVariable(value = "resourcePublicId") String resourcePublicId,
                                                       @PathVariable(value = "managerPublicUserId") String managerPublicUserId,
                                                       @PathVariable(value = "bankAccountPublicId") String bankAccountPublicId,
                                                       @RequestParam(value = "newOwnerPublicUserId") String newOwnerPublicUserId
    ) throws Exception {
        BankAccountsResponse bankAccountsResponse = new BankAccountsResponse();
        BankAccountsDTO resultBankAccountsDTO = bankAccountsService.createBankAccountOwner(managerPublicUserId, bankAccountPublicId, resourcePublicId,newOwnerPublicUserId);
        modelMapper.map(resultBankAccountsDTO, bankAccountsResponse);
        return bankAccountsResponse;
    }
    @GetMapping(path = "/{resourcePublicId}/{managerPublicUserId}/account/{bankAccountPublicId}",
            produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
    public BankAccountsResponse readBankAccount(@PathVariable(value = "resourcePublicId") String resourcePublicId,
                                                @PathVariable(value = "managerPublicUserId") String managerPublicUserId,
                                                @PathVariable(value = "bankAccountPublicId") String bankAccountPublicId
    ) throws Exception {
        BankAccountsResponse bankAccountsResponse = new BankAccountsResponse();
        BankAccountsDTO resultBankAccountsDTO = bankAccountsService.readBankAccount(managerPublicUserId, resourcePublicId, bankAccountPublicId);
        modelMapper.map(resultBankAccountsDTO, bankAccountsResponse);
        return bankAccountsResponse;
    }

    @GetMapping(path = "/{resourcePublicId}/{managerPublicUserId}/accounts",
            produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
    public List<BankAccountsResponse> readAllBankAccount(@PathVariable(value = "resourcePublicId") String resourcePublicId,
                                                         @PathVariable(value = "managerPublicUserId") String managerPublicUserId

    ) throws Exception {
        List<BankAccountsResponse> bankAccountsResponseList = new ArrayList<>();
        List<BankAccountsDTO> resultBankAccountsDTOList = bankAccountsService.readAllBankAccount(resourcePublicId, managerPublicUserId);
        for (BankAccountsDTO bankAccountsDTO : resultBankAccountsDTOList) {
            BankAccountsResponse bankAccountsResponse = new BankAccountsResponse();
            modelMapper = new ModelMapper();
            modelMapper.map(bankAccountsDTO, bankAccountsResponse);
            bankAccountsResponseList.add(bankAccountsResponse);
        }
        return bankAccountsResponseList;
    }


    @PutMapping(path = "/{resourcePublicId}/{managerPublicUserId}/account/{bankAccountPublicId}",
            produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
    public BankAccountsResponse updateBankAccount(@RequestBody BankAccountsRequest bankAccountsRequest,
                                                  @PathVariable(value = "resourcePublicId") String resourcePublicId,
                                                  @PathVariable(value = "managerPublicUserId") String managerPublicUserId,
                                                  @PathVariable(value = "bankAccountPublicId") String bankAccountPublicId
    ) throws Exception {
        BankAccountsResponse bankAccountsResponse = new BankAccountsResponse();
        BankAccountsDTO bankAccountsDTO = new BankAccountsDTO();
        modelMapper.map(bankAccountsRequest, bankAccountsDTO);
        BankAccountsDTO resultBankAccountsDTO = bankAccountsService.updateBankAccount(bankAccountsDTO, managerPublicUserId, resourcePublicId, bankAccountPublicId);
        modelMapper.map(resultBankAccountsDTO, bankAccountsResponse);
        return bankAccountsResponse;
    }

    @PutMapping(path = "/{resourcePublicId}/{managerPublicUserId}/account/{bankAccountPublicId}/owner",
            produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
    public BankAccountsResponse updateBankAccountOwner(@PathVariable(value = "resourcePublicId") String resourcePublicId,
                                                  @PathVariable(value = "managerPublicUserId") String managerPublicUserId,
                                                  @PathVariable(value = "bankAccountPublicId") String bankAccountPublicId,
                                                  @RequestParam(value = "oldOwnerPublicUserId") String oldOwnerPublicUserId,
                                                  @RequestParam(value = "newOwnerPublicUserId") String newOwnerPublicUserId
    ) throws Exception {
        BankAccountsResponse bankAccountsResponse = new BankAccountsResponse();
        BankAccountsDTO resultBankAccountsDTO = bankAccountsService.updateBankAccountOwner(managerPublicUserId, oldOwnerPublicUserId, newOwnerPublicUserId, bankAccountPublicId, resourcePublicId);
        modelMapper.map(resultBankAccountsDTO, bankAccountsResponse);
        return bankAccountsResponse;
    }

    @DeleteMapping(path = "/{resourcePublicId}/{managerPublicUserId}/account/{bankAccountPublicId}")
    public OperationStatusResponse deletePerson(@PathVariable String resourcePublicId,
                                                @PathVariable String managerPublicUserId,
                                                @PathVariable String bankAccountPublicId
    ) throws Exception {
        OperationStatusResponse response = new OperationStatusResponse();
        OperationStatusDTO operationStatusDTO = bankAccountsService.deleteBankAccount(managerPublicUserId, resourcePublicId, bankAccountPublicId);
        BeanUtils.copyProperties(operationStatusDTO, response);
        return response;
    }
}
