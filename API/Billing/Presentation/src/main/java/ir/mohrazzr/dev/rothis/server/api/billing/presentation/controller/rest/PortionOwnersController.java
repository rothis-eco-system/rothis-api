package ir.mohrazzr.dev.rothis.server.api.billing.presentation.controller.rest;

import ir.mohrazzr.dev.rothis.server.api.billing.presentation.model.request.PortionOwnersRequest;
import ir.mohrazzr.dev.rothis.server.api.billing.presentation.model.response.PortionOwnersResponse;
import ir.mohrazzr.dev.rothis.server.api.billing.presentation.model.response.opreations.OperationStatusResponse;
import ir.mohrazzr.dev.rothis.server.api.billing.presentation.controller.router.ROUTES;
import ir.mohrazzr.dev.rothis.server.api.billing.service.DTO.OperationStatusDTO;
import ir.mohrazzr.dev.rothis.server.api.billing.service.DTO.PortionOwnersDTO;
import ir.mohrazzr.dev.rothis.server.api.billing.service.service.iservice.IPortionOwnersService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by IntelliJ IDEA at Monday in 2019/11/18 - 8:17 PM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : Rothis
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Email : mhrz.dev@gmail.com
 * Class Name          : PortionOwnersController
 **/
@RestController
@RequestMapping(ROUTES.RESOURCES_URL)
public class PortionOwnersController {
    private IPortionOwnersService portionOwnersService;
    private ModelMapper modelMapper;

    @Autowired
    public PortionOwnersController(IPortionOwnersService ownersService) {
        this.portionOwnersService = ownersService;
        this.modelMapper=new ModelMapper();
    }

    @PostMapping(path = "/{publicResourceId}/{managerPublicUserId}/portions/create/{publicOwnerUserId}",
            consumes = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
    public PortionOwnersResponse createPortion(@RequestBody PortionOwnersRequest portionOwnersRequest,
                                               @PathVariable(value = "managerPublicUserId") String managerPublicUserId,
                                               @PathVariable(value = "publicResourceId") String publicResourceId,
                                               @PathVariable(value = "publicOwnerUserId") String publicOwnerUserId
    ) throws Exception {
        PortionOwnersResponse response = new PortionOwnersResponse();
        PortionOwnersDTO portionOwnersDTO = new PortionOwnersDTO();
        portionOwnersDTO.setPortionValue(portionOwnersRequest.getPortionValue());
        PortionOwnersDTO resultPortionOwnersDTO = portionOwnersService.createPortion(portionOwnersDTO, managerPublicUserId, publicResourceId, publicOwnerUserId);
        modelMapper.map(resultPortionOwnersDTO, response);
        return response;
    }

    @GetMapping(path = "/{publicResourceId}/portions/read/{ownerPublicUserId}",
            produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
    public PortionOwnersResponse readPortion(@PathVariable(value = "publicResourceId") String publicResourceId,
                                             @PathVariable(value = "ownerPublicUserId") String ownerPublicUserId,
                                             @RequestParam(value = "publicPortionId") String publicPortionId) {
        PortionOwnersResponse portionOwnersResponse = new PortionOwnersResponse();
        PortionOwnersDTO portionOwnersDTO = portionOwnersService.readPortion(ownerPublicUserId, publicResourceId, publicPortionId);
        modelMapper.map(portionOwnersDTO, portionOwnersResponse);
        return portionOwnersResponse;
    }

    @GetMapping(path = "/{publicResourceId}/{managerPublicUserId}/portions/read/all", produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
    public List<PortionOwnersResponse> readAllResourcePortion(@PathVariable(value = "managerPublicUserId") String managerPublicUserId,
                                                              @PathVariable(value = "publicResourceId") String publicResourceId,
                                                              @RequestParam(value = "page") int page,
                                                              @RequestParam(value = "limit") int limit) {
        List<PortionOwnersResponse> portionOwnersResponseList = new ArrayList<>();
        List<PortionOwnersDTO> portionOwnersDTOList = portionOwnersService.readAllPortion(publicResourceId, managerPublicUserId, page, limit);
        for (PortionOwnersDTO portionOwnersDTO : portionOwnersDTOList) {
            PortionOwnersResponse portionOwnersResponse = new PortionOwnersResponse();
            ModelMapper modelMapper = new ModelMapper();
            modelMapper.map(portionOwnersDTO, portionOwnersResponse);
            portionOwnersResponseList.add(portionOwnersResponse);
        }
        return portionOwnersResponseList;
    }

    @PutMapping(path = "/{publicResourceId}/{managerPublicUserId}/portions/update/{ownerPublicUserId}", produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
    public PortionOwnersResponse updatePortion(@RequestBody PortionOwnersRequest portionOwnersRequest,
                                               @PathVariable(value = "managerPublicUserId") String managerPublicUserId,
                                               @PathVariable(value = "publicResourceId") String publicResourceId,
                                               @PathVariable(value = "ownerPublicUserId") String ownerPublicUserId,
                                               @RequestParam(value = "portionPublicId") String portionPublicId) {
        PortionOwnersResponse portionOwnersResponse = new PortionOwnersResponse();
        PortionOwnersDTO inDTO = new PortionOwnersDTO();
        modelMapper.map(portionOwnersRequest, inDTO);
        PortionOwnersDTO portionOwnersDTO = portionOwnersService.updatePortion(inDTO, managerPublicUserId, publicResourceId, ownerPublicUserId, portionPublicId);
        modelMapper.map(portionOwnersDTO, portionOwnersResponse);
        return portionOwnersResponse;
    }

    @DeleteMapping(path = "/{publicResourceId}/{managerPublicUserId}/portions/delete/{ownerPublicUserId}", produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
    public OperationStatusResponse deletePortion(@PathVariable(value = "managerPublicUserId") String managerPublicUserId,
                                                 @PathVariable(value = "publicResourceId") String publicResourceId,
                                                 @PathVariable(value = "ownerPublicUserId") String ownerPublicUserId,
                                                 @RequestParam(value = "portionPublicId") String portionPublicId) throws Exception {
        OperationStatusResponse response = new OperationStatusResponse();
        OperationStatusDTO operationStatusDTO = portionOwnersService.deletePortion(managerPublicUserId, publicResourceId, ownerPublicUserId, portionPublicId);
        modelMapper.map(operationStatusDTO, response);
        return response;
    }
}
