package ir.mohrazzr.dev.rothis.server.api.billing.presentation.controller.rest;

import ir.mohrazzr.dev.rothis.server.api.billing.presentation.model.request.ResourceBillsRequest;
import ir.mohrazzr.dev.rothis.server.api.billing.presentation.model.response.ResourceBillsResponse;
import ir.mohrazzr.dev.rothis.server.api.billing.presentation.model.response.opreations.OperationStatusResponse;
import ir.mohrazzr.dev.rothis.server.api.billing.presentation.controller.router.ROUTES;
import ir.mohrazzr.dev.rothis.server.api.billing.service.DTO.OperationStatusDTO;
import ir.mohrazzr.dev.rothis.server.api.billing.service.DTO.ResourceBillsDTO;
import ir.mohrazzr.dev.rothis.server.api.billing.service.service.iservice.IResourceBillsService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by IntelliJ IDEA at Tuesday in 2019/11/26 - 10:21 AM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : Rothis
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Email : mhrz.dev@gmail.com
 * Class Name          : ResourceBillsController
 **/
@RestController
@RequestMapping(ROUTES.RESOURCES_BILLS_URL)
public class ResourceBillsController {
    private IResourceBillsService resourceBillsService;
    private ModelMapper modelMapper;

    @Autowired
    public ResourceBillsController(IResourceBillsService resourceBillsService) {
        this.resourceBillsService = resourceBillsService;
        this.modelMapper = new ModelMapper();
    }

    @PostMapping(path = "/{resourcePublicId}/{managerPublicUserId}/new",
            consumes = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
    public ResourceBillsResponse createResourceBill(@RequestBody ResourceBillsRequest resourcesRequest,
                                                    @PathVariable(value = "resourcePublicId") String resourcePublicId,
                                                    @PathVariable(value = "managerPublicUserId") String managerPublicUserId,
                                                    @RequestParam(value = "bankAccountPublicId") String bankAccountPublicId
    ) throws Exception {
        ResourceBillsResponse response = new ResourceBillsResponse();
        ResourceBillsDTO dto = new ResourceBillsDTO();
        modelMapper.map(resourcesRequest, dto);
        ResourceBillsDTO resultResourceBillsDTO = resourceBillsService.createResourceBill(dto, resourcePublicId, managerPublicUserId, bankAccountPublicId);
        modelMapper.map(resultResourceBillsDTO, response);
        return response;
    }

    @GetMapping(path = "/{resourcePublicId}/{managerPublicUserId}/bill",
            consumes = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
    public ResourceBillsResponse readResourceBill(@PathVariable(value = "resourcePublicId") String resourcePublicId,
                                                  @PathVariable(value = "managerPublicUserId") String managerPublicUserId,
                                                  @RequestParam(value = "resourceBillsPublicId", defaultValue = "null") String resourceBillsPublicId,
                                                  @RequestParam(value = "resourceBillIdentityCode", defaultValue = "null") String resourceBillIdentityCode
    ) throws Exception {
        ResourceBillsResponse response = new ResourceBillsResponse();
        ResourceBillsDTO resultResourceBillsDTO = resourceBillsService.readResourceBill(resourcePublicId, managerPublicUserId,
                resourceBillsPublicId, resourceBillIdentityCode);
        modelMapper.map(resultResourceBillsDTO, response);
        return response;
    }

    @PutMapping(path = "/{resourcePublicId}/{managerPublicUserId}/bill/{resourceBillPublicId}",
            consumes = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
    public ResourceBillsResponse updateResourceBill(@RequestBody ResourceBillsRequest resourcesRequest,
                                                    @PathVariable(value = "resourcePublicId") String resourcePublicId,
                                                    @PathVariable(value = "managerPublicUserId") String managerPublicUserId,
                                                    @PathVariable(value = "resourceBillPublicId") String resourceBillPublicId,
                                                    @RequestParam(value = "newBankAccountPublicId") String newBankAccountPublicId
    ) throws Exception {
        ResourceBillsResponse response = new ResourceBillsResponse();
        ResourceBillsDTO dto = new ResourceBillsDTO();
        modelMapper.map(resourcesRequest, dto);
        ResourceBillsDTO resultResourceBillsDTO = resourceBillsService.updateResourceBill(dto, resourcePublicId, managerPublicUserId, resourceBillPublicId, newBankAccountPublicId);
        modelMapper.map(resultResourceBillsDTO, response);
        return response;
    }

    @DeleteMapping(path = "/{resourcePublicId}/{managerPublicUserId}/bill/{resourceBillPublicId}")
    public OperationStatusResponse deleteResourceBill(@PathVariable(value = "resourcePublicId") String resourcePublicId,
                                                      @PathVariable(value = "managerPublicUserId") String managerPublicUserId,
                                                      @PathVariable(value = "resourceBillPublicId") String resourceBillPublicId
    ) throws Exception {
        OperationStatusResponse response = new OperationStatusResponse();
        OperationStatusDTO operationStatusDTO = resourceBillsService.deleteResourceBill(resourcePublicId, managerPublicUserId, resourceBillPublicId);
        BeanUtils.copyProperties(operationStatusDTO, response);
        return response;
    }

    @GetMapping(path = "/{resourcePublicId}/{managerPublicUserId}/bills",
            consumes = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
    public List<ResourceBillsResponse> readAllResourceBill(@PathVariable(value = "resourcePublicId") String resourcePublicId,
                                                           @PathVariable(value = "managerPublicUserId") String managerPublicUserId,
                                                           @RequestParam(value = "page", defaultValue = "0") int page,
                                                           @RequestParam(value = "limit", defaultValue = "100") int limit
    ) throws Exception {
        List<ResourceBillsResponse> responseList = new ArrayList<>();
        List<ResourceBillsDTO> resultResourceBillsDTOList = resourceBillsService.readAllResourceBills(
                resourcePublicId, managerPublicUserId,
                page, limit);
        for (ResourceBillsDTO resourceBillsDTO:resultResourceBillsDTOList){
            ResourceBillsResponse resourceBillsResponse = new ResourceBillsResponse();
            modelMapper = new ModelMapper();
            modelMapper.map(resourceBillsDTO,resourceBillsResponse);
            responseList.add(resourceBillsResponse);
        }
        return responseList;
    }

}
