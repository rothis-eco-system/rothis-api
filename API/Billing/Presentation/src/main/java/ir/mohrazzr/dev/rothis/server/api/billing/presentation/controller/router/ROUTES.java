package ir.mohrazzr.dev.rothis.server.api.billing.presentation.controller.router;

/**
 * Created by IntelliJ IDEA at Wednesday in 2019/10/30 - 10:49 PM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : Rothis
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Email : mhrz.dev@gmail.com
 * Class Name          : Routes
 **/

public class ROUTES {
    private final static String VERSION = "/v1";
    private final static String API = "/api";
    private final static String HOST_URL = API + VERSION;
    public final static String USERS_URL = HOST_URL + "/users";
    public final static String SIGN_UP_URL = USERS_URL + "/signup";
    public final static String SIGN_IN_URL = USERS_URL + "/login";
    public final static String SIGN_OUT = USERS_URL + "/logout";
    public final static String PERSONS_URL = HOST_URL + "/persons";
    public static final String USER_EMAIL_URL = USERS_URL + "/email";
    public final static String PERSON_ADDRESSES = PERSONS_URL;
    public static final String PERSONS_PHONE_NUMBERS_URL = PERSONS_URL;
    public static final String RESOURCES_URL = HOST_URL + "/resources";
    public static final String BANK_ACCOUNTS_URL = HOST_URL+"/bankaccounts";
    public static final String RESOURCES_BILLS_URL = RESOURCES_URL+"/bills";
    public static final String RESOURCES_BILLS_LIST_REPORT_URL = RESOURCES_BILLS_URL + "/reports";

}
