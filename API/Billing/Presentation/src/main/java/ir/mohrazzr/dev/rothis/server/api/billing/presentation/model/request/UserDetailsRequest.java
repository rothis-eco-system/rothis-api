package ir.mohrazzr.dev.rothis.server.api.billing.presentation.model.request;

/**
 * Created by IntelliJ IDEA at Wednesday in 2019/10/30 - 11:06 PM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : Rothis
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @userName : mhrz.dev@gmail.com
 * Class Name          : UserDetailsRequest
 **/

public class UserDetailsRequest {

    private String nickName;
    private String userName;
    private String password;

    public UserDetailsRequest() {
    }

    public UserDetailsRequest(String userName, String password,String nickName) {
        this.userName = userName;
        this.password = password;
        this.nickName=nickName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }
}
