package ir.mohrazzr.dev.rothis.server.api.billing.presentation.model.request;

import java.util.List;

/**
 * Created by IntelliJ IDEA at Tuesday in 2019/11/12 - 10:24 AM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : Rothis
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Email : mhrz.dev@gmail.com
 * Class Name          : PersonsExtraRequest
 **/

public class PersonsExtraRequest {
    private String firstName;
    private String lastName;
    private String fatherName;
    private String nationalCode;
    private String passPortCode;
    private String gender;
    private String dateOfBirth;
    private List<AddressesRequest> addresses;
    private List<PhoneNumbersRequest> phoneNumbers;

    public PersonsExtraRequest() {
    }

    public PersonsExtraRequest(String firstName, String lastName, String fatherName, String nationalCode, String passPortCode, String gender, String dateOfBirth, List<AddressesRequest> addresses, List<PhoneNumbersRequest> phoneNumbers) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.fatherName = fatherName;
        this.nationalCode = nationalCode;
        this.passPortCode = passPortCode;
        this.gender = gender;
        this.dateOfBirth = dateOfBirth;
        this.addresses = addresses;
        this.phoneNumbers = phoneNumbers;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFatherName() {
        return fatherName;
    }

    public void setFatherName(String fatherName) {
        this.fatherName = fatherName;
    }

    public String getNationalCode() {
        return nationalCode;
    }

    public void setNationalCode(String nationalCode) {
        this.nationalCode = nationalCode;
    }

    public String getPassPortCode() {
        return passPortCode;
    }

    public void setPassPortCode(String passPortCode) {
        this.passPortCode = passPortCode;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public List<AddressesRequest> getAddresses() {
        return addresses;
    }

    public void setAddresses(List<AddressesRequest> addresses) {
        this.addresses = addresses;
    }

    public List<PhoneNumbersRequest> getPhoneNumbers() {
        return phoneNumbers;
    }

    public void setPhoneNumbers(List<PhoneNumbersRequest> phoneNumbers) {
        this.phoneNumbers = phoneNumbers;
    }

    @Override
    public String toString() {
        return "PersonsRequest{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", fatherName='" + fatherName + '\'' +
                ", nationalCode='" + nationalCode + '\'' +
                ", passPortCode='" + passPortCode + '\'' +
                ", gender='" + gender + '\'' +
                ", dateOfBirth='" + dateOfBirth + '\'' +
                '}';
    }
}
