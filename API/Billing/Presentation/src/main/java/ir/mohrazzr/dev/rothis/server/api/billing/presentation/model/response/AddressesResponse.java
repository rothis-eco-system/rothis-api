package ir.mohrazzr.dev.rothis.server.api.billing.presentation.model.response;

/**
 * Created by IntelliJ IDEA at Friday in 2019/11/08 - 7:45 PM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : Rothis
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Email : mhrz.dev@gmail.com
 * Class Name          : AddressesResponse
 **/

public class AddressesResponse {
    private String publicAddressId;
    private Boolean isPrimary;
    private Integer cityCode;
    private String postalCode;
    private String address;

    public AddressesResponse() {
    }

    public AddressesResponse(String publicAddressId, Boolean isPrimary, Integer cityCode, String postalCode, String address) {
        this.publicAddressId = publicAddressId;
        this.isPrimary = isPrimary;
        this.cityCode = cityCode;
        this.postalCode = postalCode;
        this.address = address;
    }

    public String getPublicAddressId() {
        return publicAddressId;
    }

    public void setPublicAddressId(String publicAddressId) {
        this.publicAddressId = publicAddressId;
    }

    public Boolean getPrimary() {
        return isPrimary;
    }

    public void setPrimary(Boolean primary) {
        isPrimary = primary;
    }

    public Integer getCityCode() {
        return cityCode;
    }

    public void setCityCode(Integer cityCode) {
        this.cityCode = cityCode;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }


    @Override
    public String toString() {
        return "AddressesResponse{" +
                "isPrimary=" + isPrimary +
                ", cityCode=" + cityCode +
                ", postalCode='" + postalCode + '\'' +
                ", address='" + address + '\'' +
                '}';
    }
}
