package ir.mohrazzr.dev.rothis.server.api.billing.presentation.model.request;

/**
 * Created by IntelliJ IDEA at Friday in 2019/11/08 - 7:45 PM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : Rothis
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Email : mhrz.dev@gmail.com
 * Class Name          : PhoneNumbersRequest
 **/

public class PhoneNumbersRequest {
    private Boolean isPrimary;
    private String phoneNumber;
    private Integer typeCode;

    public PhoneNumbersRequest() {
    }

    public PhoneNumbersRequest( Boolean isPrimary, String phoneNumber, Integer typeCode) {
        this.isPrimary = isPrimary;
        this.phoneNumber = phoneNumber;
        this.typeCode = typeCode;
    }


    public Boolean getPrimary() {
        return isPrimary;
    }

    public void setPrimary(Boolean primary) {
        isPrimary = primary;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Integer getTypeCode() {
        return typeCode;
    }

    public void setTypeCode(Integer typeCode) {
        this.typeCode = typeCode;
    }
}
