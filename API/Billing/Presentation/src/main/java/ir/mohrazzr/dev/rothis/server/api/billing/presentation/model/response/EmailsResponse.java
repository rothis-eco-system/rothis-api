package ir.mohrazzr.dev.rothis.server.api.billing.presentation.model.response;

/**
 * Created by IntelliJ IDEA at Thursday in 2019/11/07 - 8:28 AM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : Rothis
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Email : mhrz.dev@gmail.com
 * Class Name          : EmailsResponse
 **/

public class EmailsResponse {
    private String publicUserId;
    private String email;
    private Boolean isActive;
    private String emailVerificationToken;
    private Boolean isPrimary;

    public EmailsResponse() {

    }

    public EmailsResponse(String publicUserId, String email, Boolean isActive, String emailVerificationToken, Boolean isPrimary) {
        this.publicUserId = publicUserId;
        this.email = email;
        this.isActive = isActive;
        this.emailVerificationToken = emailVerificationToken;
        this.isPrimary = isPrimary;
    }

    public String getPublicUserId() {
        return publicUserId;
    }

    public void setPublicUserId(String publicUserId) {
        this.publicUserId = publicUserId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean isActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public String getEmailVerificationToken() {
        return emailVerificationToken;
    }

    public void setEmailVerificationToken(String emailVerificationToken) {
        this.emailVerificationToken = emailVerificationToken;
    }

    public Boolean isPrimary() {
        return isPrimary;
    }

    public void setIsPrimary(Boolean primary) {
        isPrimary = primary;
    }
}
