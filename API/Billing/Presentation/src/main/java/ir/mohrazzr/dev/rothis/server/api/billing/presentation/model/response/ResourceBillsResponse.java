package ir.mohrazzr.dev.rothis.server.api.billing.presentation.model.response;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

/**
 * Created by IntelliJ IDEA at Monday in 2019/11/25 - 7:20 PM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : Rothis
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Email : mhrz.dev@gmail.com
 * Class Name          : ResourceBillsResponse
 **/

public class ResourceBillsResponse {
    private String publicIdString;
    private String resourceIdString;
    private BankAccountsResponse bankAccount;
    private String resourceBillIdentityCode;
    private Date paymentDeadLine;
    private String paymentDeadLinePersianDate;
    private BigDecimal totalPrice;
    private String totalPriceInAlphabet;
    private BigDecimal pricePerSecond;
    private String pricePerPortionInAlphabet;
    private Long billCount;
    private Integer roundCount;
    private String roundCountInAlphabet;
    private Timestamp exportTime;
    private String exportTimePersianDate;
    private String concern;
    private String descriptions;
    private List<BillsResponse> bills;

    public ResourceBillsResponse() {
    }

    public ResourceBillsResponse(String publicIdString, String resourceIdString, BankAccountsResponse bankAccount, String resourceBillIdentityCode, Date paymentDeadLine, String paymentDeadLinePersianDate, BigDecimal totalPrice, String totalPriceInAlphabet, BigDecimal pricePerSecond, String pricePerPortionInAlphabet, Long billCount, Integer roundCount, String roundCountInAlphabet, Timestamp exportTime, String exportTimePersianDate, String concern, String descriptions, List<BillsResponse> bills) {
        this.publicIdString = publicIdString;
        this.resourceIdString = resourceIdString;
        this.bankAccount = bankAccount;
        this.resourceBillIdentityCode = resourceBillIdentityCode;
        this.paymentDeadLine = paymentDeadLine;
        this.paymentDeadLinePersianDate = paymentDeadLinePersianDate;
        this.totalPrice = totalPrice;
        this.totalPriceInAlphabet = totalPriceInAlphabet;
        this.pricePerSecond = pricePerSecond;
        this.pricePerPortionInAlphabet = pricePerPortionInAlphabet;
        this.billCount = billCount;
        this.roundCount = roundCount;
        this.roundCountInAlphabet = roundCountInAlphabet;
        this.exportTime = exportTime;
        this.exportTimePersianDate = exportTimePersianDate;
        this.concern = concern;
        this.descriptions = descriptions;
        this.bills = bills;
    }

    public String getPublicIdString() {
        return publicIdString;
    }

    public void setPublicIdString(String publicIdString) {
        this.publicIdString = publicIdString;
    }

    public String getResourceIdString() {
        return resourceIdString;
    }

    public void setResourceIdString(String resourceIdString) {
        this.resourceIdString = resourceIdString;
    }

    public String getResourceBillIdentityCode() {
        return resourceBillIdentityCode;
    }

    public void setResourceBillIdentityCode(String resourceBillIdentityCode) {
        this.resourceBillIdentityCode = resourceBillIdentityCode;
    }

    public Date getPaymentDeadLine() {
        return paymentDeadLine;
    }

    public void setPaymentDeadLine(Date paymentDeadLine) {
        this.paymentDeadLine = paymentDeadLine;
    }

    public String getPaymentDeadLinePersianDate() {
        return paymentDeadLinePersianDate;
    }

    public void setPaymentDeadLinePersianDate(String paymentDeadLinePersianDate) {
        this.paymentDeadLinePersianDate = paymentDeadLinePersianDate;
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getTotalPriceInAlphabet() {
        return totalPriceInAlphabet;
    }

    public void setTotalPriceInAlphabet(String totalPriceInAlphabet) {
        this.totalPriceInAlphabet = totalPriceInAlphabet;
    }

    public BigDecimal getPricePerSecond() {
        return pricePerSecond;
    }

    public void setPricePerSecond(BigDecimal pricePerSecond) {
        this.pricePerSecond = pricePerSecond;
    }

    public String getPricePerPortionInAlphabet() {
        return pricePerPortionInAlphabet;
    }

    public void setPricePerPortionInAlphabet(String pricePerPortionInAlphabet) {
        this.pricePerPortionInAlphabet = pricePerPortionInAlphabet;
    }

    public Long getBillCount() {
        return billCount;
    }

    public void setBillCount(Long billCount) {
        this.billCount = billCount;
    }

    public Integer getRoundCount() {
        return roundCount;
    }

    public void setRoundCount(Integer roundCount) {
        this.roundCount = roundCount;
    }

    public String getRoundCountInAlphabet() {
        return roundCountInAlphabet;
    }

    public void setRoundCountInAlphabet(String roundCountInAlphabet) {
        this.roundCountInAlphabet = roundCountInAlphabet;
    }

    public Timestamp getExportTime() {
        return exportTime;
    }

    public void setExportTime(Timestamp exportTime) {
        this.exportTime = exportTime;
    }

    public String getExportTimePersianDate() {
        return exportTimePersianDate;
    }

    public void setExportTimePersianDate(String exportTimePersianDate) {
        this.exportTimePersianDate = exportTimePersianDate;
    }

    public String getConcern() {
        return concern;
    }

    public void setConcern(String concern) {
        this.concern = concern;
    }

    public String getDescriptions() {
        return descriptions;
    }

    public void setDescriptions(String descriptions) {
        this.descriptions = descriptions;
    }

    public List<BillsResponse> getBills() {
        return bills;
    }

    public void setBills(List<BillsResponse> bills) {
        this.bills = bills;
    }

    public BankAccountsResponse getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(BankAccountsResponse bankAccount) {
        this.bankAccount = bankAccount;
    }
}
