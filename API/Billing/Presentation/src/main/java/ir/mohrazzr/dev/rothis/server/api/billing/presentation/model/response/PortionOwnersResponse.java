package ir.mohrazzr.dev.rothis.server.api.billing.presentation.model.response;

/**
 * Created by IntelliJ IDEA at Monday in 2019/11/18 - 8:21 PM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : Rothis
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Email : mhrz.dev@gmail.com
 * Class Name          : PortionOwnersResponse
 **/

public class PortionOwnersResponse {
    private String publicIdString;
    private String portionIdentifierCode;
    private Float portionValue;
    private Long portionSeconds;
    private PersonsExtraResponse owner;

    public PortionOwnersResponse() {
    }

    public PortionOwnersResponse(PersonsExtraResponse owner, String publicIdString, String portionIdentifierCode, Float portionValue, Long portionSeconds) {
        this.owner = owner;
        this.publicIdString = publicIdString;
        this.portionIdentifierCode = portionIdentifierCode;
        this.portionValue = portionValue;
        this.portionSeconds = portionSeconds;
    }

    public PersonsExtraResponse getOwner() {
        return owner;
    }

    public void setOwner(PersonsExtraResponse owner) {
        this.owner = owner;
    }

    public String getPortionIdentifierCode() {
        return portionIdentifierCode;
    }

    public void setPortionIdentifierCode(String portionIdentifierCode) {
        this.portionIdentifierCode = portionIdentifierCode;
    }

    public String getPublicIdString() {
        return publicIdString;
    }

    public void setPublicIdString(String publicIdString) {
        this.publicIdString = publicIdString;
    }

    public Float getPortionValue() {
        return portionValue;
    }

    public void setPortionValue(Float portionValue) {
        this.portionValue = portionValue;
    }

    public Long getPortionSeconds() {
        return portionSeconds;
    }

    public void setPortionSeconds(Long portionSeconds) {
        this.portionSeconds = portionSeconds;
    }
}
