package ir.mohrazzr.dev.rothis.server.api.billing.presentation.model.request;

/**
 * Created by IntelliJ IDEA at Friday in 2019/11/15 - 9:43 AM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : Rothis
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Email : mhrz.dev@gmail.com
 * Class Name          : ResourcesRequest
 **/

public class ResourcesRequest {
    private String name;
    private String identifierCode;
    private Integer dongValue;
    private Integer circuitOfTheDay;

    public ResourcesRequest() {
    }

    public ResourcesRequest(String name, String identifierCode, Integer dongValue, Integer circuitOfTheDay) {
        this.name = name;
        this.identifierCode = identifierCode;
        this.dongValue = dongValue;
        this.circuitOfTheDay = circuitOfTheDay;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIdentifierCode() {
        return identifierCode;
    }

    public void setIdentifierCode(String identifierCode) {
        this.identifierCode = identifierCode;
    }

    public Integer getDongValue() {
        return dongValue;
    }

    public void setDongValue(Integer dongValue) {
        this.dongValue = dongValue;
    }

    public Integer getCircuitOfTheDay() {
        return circuitOfTheDay;
    }

    public void setCircuitOfTheDay(Integer circuitOfTheDay) {
        this.circuitOfTheDay = circuitOfTheDay;
    }

}
