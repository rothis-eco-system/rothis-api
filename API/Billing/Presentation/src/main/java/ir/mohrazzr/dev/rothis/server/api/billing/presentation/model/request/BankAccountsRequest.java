package ir.mohrazzr.dev.rothis.server.api.billing.presentation.model.request;

import java.util.List;

/**
 * Created by IntelliJ IDEA at Thursday in 2019/11/21 - 11:40 AM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : Rothis
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Email : mhrz.dev@gmail.com
 * Class Name          : BankAccountsRequest
 **/

public class BankAccountsRequest {
    private String bankName;
    private String accountNumber;
    private String name;
    private String shabaNumber;
    private String cardNumber;
    private String branchCode;
    private String branchName;
    private String branchAddress;
    private String description;
    private List<String> ownerList;

    public BankAccountsRequest() {
    }

    public BankAccountsRequest(String bankName, String accountNumber, String name, String shabaNumber, String cardNumber, String branchCode, String branchName, String branchAddress, String description, List<String> ownerList) {
        this.bankName = bankName;
        this.accountNumber = accountNumber;
        this.name = name;
        this.shabaNumber = shabaNumber;
        this.cardNumber = cardNumber;
        this.branchCode = branchCode;
        this.branchName = branchName;
        this.branchAddress = branchAddress;
        this.description = description;
        this.ownerList = ownerList;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShabaNumber() {
        return shabaNumber;
    }

    public void setShabaNumber(String shabaNumber) {
        this.shabaNumber = shabaNumber;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getBranchCode() {
        return branchCode;
    }

    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String getBranchAddress() {
        return branchAddress;
    }

    public void setBranchAddress(String branchAddress) {
        this.branchAddress = branchAddress;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public List<String> getOwnerList() {
        return ownerList;
    }

    public void setOwnerList(List<String> ownersPublicUserId) {
        this.ownerList = ownersPublicUserId;
    }
}
