package ir.mohrazzr.dev.rothis.server.api.billing.presentation.model.request;

import java.math.BigDecimal;

/**
 * Created by IntelliJ IDEA at Monday in 2019/11/25 - 7:18 PM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : Rothis
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Email : mhrz.dev@gmail.com
 * Class Name          : ResourceBillsRequest
 **/

public class ResourceBillsRequest {
    private BigDecimal totalPrice;
    private String concern;
    private Integer paymentDeadLineDays;
    private String descriptions;

    public ResourceBillsRequest() {
    }

    public ResourceBillsRequest(BigDecimal totalPrice, String concern, Integer paymentDeadLineDays, String descriptions) {
        this.totalPrice = totalPrice;
        this.concern = concern;
        this.paymentDeadLineDays = paymentDeadLineDays;
        this.descriptions = descriptions;
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getConcern() {
        return concern;
    }

    public void setConcern(String concern) {
        this.concern = concern;
    }

    public Integer getPaymentDeadLineDays() {
        return paymentDeadLineDays;
    }

    public void setPaymentDeadLineDays(Integer paymentDeadLineDays) {
        this.paymentDeadLineDays = paymentDeadLineDays;
    }

    public String getDescriptions() {
        return descriptions;
    }

    public void setDescriptions(String descriptions) {
        this.descriptions = descriptions;
    }
}
