package ir.mohrazzr.dev.rothis.server.api.billing.exception;

import ir.mohrazzr.dev.rothis.server.api.billing.exception.handler.RothisExceptionService;
import ir.mohrazzr.dev.rothis.server.api.billing.exception.message.ErrorMessage;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import java.util.Date;

/**
 * Created by IntelliJ IDEA at Sunday in 2019/11/03 - 11:06 AM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : Rothis
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Email : mhrz.dev@gmail.com
 * Class Name          : AppExceptionHandler
 **/


@ControllerAdvice
public class AppExceptionHandler {

    @ExceptionHandler(value = {RothisExceptionService.class})
    public ResponseEntity<Object> handleUserServiceException(RothisExceptionService ex, WebRequest request) {
        ErrorMessage message = new ErrorMessage(new Date(), ex.getMessage());
        return new ResponseEntity<>(message, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
    }
    @ExceptionHandler(value = {Exception.class})
    public ResponseEntity<Object> handleOthersException(Exception ex, WebRequest request) {
        ErrorMessage message = new ErrorMessage(new Date(), ex.getMessage());
        return new ResponseEntity<>(message, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
    }
}