package ir.mohrazzr.dev.rothis.server.api.billing.service.service.impl;

import ir.mohrazzr.dev.rothis.server.api.billing.domain.entity.PublicIDsEntity;
import ir.mohrazzr.dev.rothis.server.api.billing.domain.repository.PublicIDsRepository;
import ir.mohrazzr.dev.rothis.server.api.billing.exception.handler.RothisExceptionService;
import ir.mohrazzr.dev.rothis.server.api.billing.exception.message.Errors;
import ir.mohrazzr.dev.rothis.server.api.billing.service.DTO.PublicIDsDTO;
import ir.mohrazzr.dev.rothis.server.api.billing.service.service.iservice.IPublicIDsService;
import ir.mohrazzr.dev.rothis.server.api.utils.generator.Generator;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * Created by IntelliJ IDEA at Sunday in 2019/11/10 - 3:20 PM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : Rothis
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Email : mhrz.dev@gmail.com
 * Class Name          : PublicIDsServiceImpl
 **/
@Service
public class PublicIDsServiceImpl implements IPublicIDsService {
    private PublicIDsRepository publicIDsRepository;
    private Generator generator;

    @Autowired
    public PublicIDsServiceImpl(PublicIDsRepository publicIDsRepository, Generator generator) {
        this.publicIDsRepository = publicIDsRepository;
        this.generator = generator;
    }

    @Override
    public PublicIDsDTO generatePublicId(Integer length) {
        PublicIDsDTO publicIDsDTO = new PublicIDsDTO();
        PublicIDsEntity publicIdsEntity = new PublicIDsEntity();
        publicIdsEntity.setPublicId(generator.generatePublicId(length));
        while (publicIDsRepository.findByPublicId(publicIdsEntity.getPublicId()) != null) {
            publicIdsEntity.setPublicId(generator.generatePublicId(length));
        }
        BeanUtils.copyProperties(publicIDsRepository.save(publicIdsEntity), publicIDsDTO);
        return publicIDsDTO;
    }

    @Override
    public PublicIDsDTO findById(Long publicUserId) {
        PublicIDsDTO publicIDsDTO = new PublicIDsDTO();
        Optional<PublicIDsEntity> publicIdsEntity = publicIDsRepository.findById(publicUserId);
        if (publicIdsEntity.isPresent()) {
            publicIdsEntity.ifPresent(p -> BeanUtils.copyProperties(p, publicIDsDTO));
            return publicIDsDTO;
        } else {
            throw new RothisExceptionService(Errors.NO_USER_FOUND_WITH_PUBLIC_USER_ID.getMessage() + publicUserId);
        }
    }

    @Override
    public PublicIDsDTO findByPublicId(String publicId) {
        PublicIDsDTO publicIDsDTO = new PublicIDsDTO();
        PublicIDsEntity publicIdsEntity = publicIDsRepository.findByPublicId(publicId);
        if (publicIdsEntity != null) {
            BeanUtils.copyProperties(publicIdsEntity, publicIDsDTO);
            return publicIDsDTO;
        } else {
            throw new RothisExceptionService(Errors.NO_USER_FOUND_WITH_PUBLIC_USER_ID.getMessage() + publicId);
        }
    }

    @Override
    public PublicIDsDTO updatePublicId(String publicId) {
        PublicIDsEntity publicIdsEntity = publicIDsRepository.findByPublicId(publicId);
        if (publicIdsEntity != null) {
            return checkAndUpdate(publicIdsEntity);
        } else {
            throw new RothisExceptionService(Errors.NO_PERSON_FOUND_WITH_PUBLIC_USER_ID.getMessage() + publicId);
        }
    }

    @Override
    public PublicIDsDTO updatePublicId(Long id) {
        Optional<PublicIDsEntity> publicIdsEntity = publicIDsRepository.findById(id);
        PublicIDsEntity foundedPublicIDsEntity = new PublicIDsEntity();
        publicIdsEntity.ifPresentOrElse(e -> {
                    BeanUtils.copyProperties(e, foundedPublicIDsEntity);
                },
                () -> {
                    throw new RothisExceptionService(Errors.NO_PERSON_FOUND_WITH_PUBLIC_USER_ID.getMessage() + id);
                });
        return checkAndUpdate(foundedPublicIDsEntity);
    }

    @Override
    public void deletePublicId(Long id) {
        publicIDsRepository.deleteById(id);
    }

    private PublicIDsDTO checkAndUpdate(PublicIDsEntity publicIdsEntity) {
        PublicIDsDTO publicIDsDTO = new PublicIDsDTO();
        publicIdsEntity.setPublicId(generator.generatePublicId(publicIdsEntity.getPublicId().length()));
        PublicIDsEntity resultUpdatedPublicIDsEntity = publicIDsRepository.save(publicIdsEntity);
        BeanUtils.copyProperties(resultUpdatedPublicIDsEntity, publicIDsDTO);
        return publicIDsDTO;
    }

}
