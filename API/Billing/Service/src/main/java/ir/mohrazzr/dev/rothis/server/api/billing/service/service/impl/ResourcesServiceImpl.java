package ir.mohrazzr.dev.rothis.server.api.billing.service.service.impl;

import ir.mohrazzr.dev.rothis.server.api.billing.domain.entity.ResourcesEntity;
import ir.mohrazzr.dev.rothis.server.api.billing.domain.repository.ResourcesRepository;
import ir.mohrazzr.dev.rothis.server.api.billing.exception.handler.RothisExceptionService;
import ir.mohrazzr.dev.rothis.server.api.billing.exception.message.Errors;
import ir.mohrazzr.dev.rothis.server.api.billing.service.DTO.*;
import ir.mohrazzr.dev.rothis.server.api.billing.service.DTO.enums.OperationCode;
import ir.mohrazzr.dev.rothis.server.api.billing.service.DTO.enums.OperationName;
import ir.mohrazzr.dev.rothis.server.api.billing.service.DTO.enums.OperationStatus;
import ir.mohrazzr.dev.rothis.server.api.billing.service.service.iservice.*;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static ir.mohrazzr.dev.rothis.server.api.utils.calculator.ResourcesCalculation.*;

/**
 * Created by IntelliJ IDEA at Thursday in 2019/11/14 - 1:51 PM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : Rothis
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Email : mhrz.dev@gmail.com
 * Class Name          : ResourcesServiceImpl
 **/
@Service
public class ResourcesServiceImpl implements IResourcesService {

    private ResourcesRepository resourcesRepository;
    private IPublicIDsService publicIDsService;
    private IUsersService usersService;
    private IResourceManagerService resourceManagerService;
    private IResolverService idResolverService;

    @Autowired
    public ResourcesServiceImpl(ResourcesRepository resourcesRepository, IPublicIDsService publicIDsService, IUsersService usersService, IResourceManagerService resourceManagerService, IResolverService idResolverService) {
        this.resourcesRepository = resourcesRepository;
        this.publicIDsService = publicIDsService;
        this.usersService = usersService;
        this.resourceManagerService = resourceManagerService;
        this.idResolverService = idResolverService;
    }


    @Override
    public ResourcesDTO createResource(ResourcesDTO inDTO, String publicUserId) {
        ResourcesDTO resourcesDTO = new ResourcesDTO();
        ResourcesEntity resourcesEntity = new ResourcesEntity();
        UsersDTO usersDTO = usersService.findUserByPublicUserId(publicUserId);
        if (usersDTO.getUserRole() == 2 || usersDTO.getUserRole() == 1) {
            if (resourcesRepository.findResourceByIdentifierCode(inDTO.getIdentifierCode()) == null) {
                PublicIDsDTO publicIDsDTO = publicIDsService.generatePublicId(20);
                BeanUtils.copyProperties(inDTO, resourcesEntity);
                resourcesEntity.setPublicId(publicIDsDTO.getId());
                ResourcesEntity resultResourcesEntity = getResourceEntity(resourcesEntity);
                if (resultResourcesEntity != null) {
                    ResourceManagersDTO resourceManagersDTO = resourceManagerService.createManager(publicUserId, publicIDsDTO.getPublicId());
                    if (resourceManagersDTO == null) {
                        resourcesRepository.deleteById(resultResourcesEntity.getId());
                    }
                    BeanUtils.copyProperties(resultResourcesEntity, resourcesDTO);
                    resourcesDTO.setPublicIdString(publicIDsDTO.getPublicId());
                    return resourcesDTO;
                } else
                    throw new RothisExceptionService(Errors.COULD_NOT_INSERT_RECORD.getMessage() + "," + Errors.RESOURCE_ALREADY_EXIST.getMessage() + resourcesEntity.getIdentifierCode());
            } else {
                throw new RothisExceptionService(Errors.COULD_NOT_INSERT_RECORD.getMessage() + "," + Errors.RESOURCE_ALREADY_EXIST.getMessage() + resourcesEntity.getIdentifierCode());
            }

        } else
            throw new RothisExceptionService(Errors.NO_PERMISSION_TO_CREATE_RESOURCE.getMessage());
    }

    @Override
    public ResourcesDTO readResource(String publicUserId, String publicResourceId) {
        ResourcesDTO resourcesDTO = new ResourcesDTO();
        PublicIDsDTO publicResourceIdDTO = publicIDsService.findByPublicId(publicResourceId);
        Long resourceId = idResolverService.getResourceID(publicResourceIdDTO.getPublicId());
        if (resourceId != null) {
            ResourceManagersDTO resourceManagersDTO = resourceManagerService.findResourceByManagerId(idResolverService.getPersonID(publicUserId), resourceId);
            if (resourceManagersDTO != null) {
                ModelMapper modelMapper = new ModelMapper();
                ResourcesEntity resourcesEntity = resourcesRepository.findResourceByPublicId(publicResourceIdDTO.getId());
                modelMapper.map(resourcesEntity, resourcesDTO);
                resourcesDTO.setPublicIdString(publicResourceId);
                return resourcesDTO;
            } else throw new RothisExceptionService(Errors.NO_RECORD_FOUND.getMessage());
        } else
            throw new RothisExceptionService(Errors.NO_RECORD_FOUND.getMessage());
    }

    @Override
    public List<ResourcesDTO> readAllResourcesByManagerPublicId(String publicUserId) {
        List<ResourcesDTO> resourcesDTOList = new ArrayList<>();
        List<Long> resourceIds = resourceManagerService.findAllResourceIdByManagerId(publicUserId);
        if (!resourceIds.isEmpty()) {
            for (Long id:resourceIds) {
                ResourcesDTO dto = new ResourcesDTO();
                Optional<ResourcesEntity> resourcesEntity = resourcesRepository.findById(id);
                ModelMapper modelMapper = new ModelMapper();
                resourcesEntity.ifPresent(en->{
                    modelMapper.map(en, dto);
                    PublicIDsDTO resourcesPublicIDsDTO = publicIDsService.findById(en.getPublicId());
                    dto.setPublicIdString(resourcesPublicIDsDTO.getPublicId());
                });
                resourcesDTOList.add(dto);
            }
            return resourcesDTOList;
        } else
            throw new RothisExceptionService(Errors.NO_RECORD_FOUND.getMessage());
    }

    @Override
    public ResourcesDTO updateResource(ResourcesDTO inDTO, String managerPublicUserId, String publicResourceId) {
        ResourcesDTO resourcesDTO = new ResourcesDTO();
        ResourcesEntity resourcesEntity = new ResourcesEntity();
        Long resourceId = idResolverService.getResourceID(publicResourceId);
        if (resourceManagerService.findResourceByManagerId(idResolverService.getPersonID(managerPublicUserId), resourceId) != null) {
            BeanUtils.copyProperties(inDTO, resourcesEntity);
            resourcesEntity.setId(resourceId);
            PublicIDsDTO publicIDsDTO = publicIDsService.updatePublicId(publicResourceId);
            resourcesEntity.setPublicId(publicIDsDTO.getId());
            resourcesEntity.setDongValue(inDTO.getDongValue());
            resourcesEntity.setCircuitOfTheDay(inDTO.getCircuitOfTheDay());
            ResourcesEntity resultResourcesEntity = getResourceEntity(resourcesEntity);
            if (resultResourcesEntity != null) {
                BeanUtils.copyProperties(resultResourcesEntity, resourcesDTO);
                resourcesDTO.setPublicIdString(publicIDsDTO.getPublicId());
            }
        } else
            throw new RothisExceptionService(Errors.NOT_FOUND_RESOURCE_WHIT_THIS_ID_OR_MANAGER.getMessage());

        return resourcesDTO;
    }

    private ResourcesEntity getResourceEntity(ResourcesEntity resourcesEntity) {
        resourcesEntity.setTotalResourcePortion(getTotalResourcePortion(resourcesEntity.getDongValue()));
        resourcesEntity.setTotalResourceSeconds(getTotalResourceSeconds(resourcesEntity.getCircuitOfTheDay()));
        resourcesEntity.setResourceSecondsPerPortion(getResourceSecondsPerPortion(resourcesEntity.getTotalResourceSeconds(), resourcesEntity.getTotalResourcePortion().longValue()));
        resourcesEntity.setTotalUnallocatedResourcePortion(getTotalUnallocatedResourcePortion(resourcesEntity.getTotalResourcePortion(), 0F));
        return resourcesRepository.save(resourcesEntity);
    }

    @Override
    public OperationStatusDTO deleteResource(String publicUserId, String publicResourceId) {
        Long resourceId = idResolverService.getResourceID(publicResourceId);
        ResourceManagersDTO resourceManagersDTO = resourceManagerService.findResourceByManagerId(idResolverService.getPersonID(publicUserId), resourceId);
        if (resourceManagersDTO != null) {
            Optional<ResourcesEntity> resourcesEntity = resourcesRepository.findById(resourceManagersDTO.getResourceId());
            if (resourceManagerService.deleteResource(resourceManagersDTO)) {
                resourcesEntity.ifPresent(e -> resourcesRepository.delete(e));
                return new OperationStatusDTO(OperationName.DELETE.name(),
                        OperationStatus.SUCCESS.name(), OperationCode.CODE200.name(),
                        Errors.DELETE_RECORD_SUCCESSFULLY.getMessage());
            }
        }
        return new OperationStatusDTO(OperationName.DELETE.name(),
                OperationStatus.ERROR.name(), OperationCode.CODE400.name(),
                Errors.COULD_NOT_DELETE_RECORD.getMessage());

    }

    @Override
    public ResourcesDTO updateUnallocatedResourcePortionValue(Long managerId, Long resourceId, Float newAllocatedValue) {
        if (resourceManagerService.findResourceByManagerId(managerId, resourceId) != null) {
            ResourcesDTO resourcesDTO = new ResourcesDTO();
            Optional<ResourcesEntity> resourcesEntity = resourcesRepository.findById(resourceId);
            resourcesEntity.ifPresent(e -> {
                if (e.getTotalUnallocatedResourcePortion() > 0) {
                    e.setTotalUnallocatedResourcePortion(getTotalUnallocatedResourcePortion(e.getTotalUnallocatedResourcePortion(), newAllocatedValue));
                    BeanUtils.copyProperties(resourcesRepository.save(e), resourcesDTO);
                }
            });
            return resourcesDTO;
        } else
            throw new RothisExceptionService(Errors.NOT_FOUND_RESOURCE_WHIT_THIS_ID_OR_MANAGER.getMessage());
    }

    @Override
    public ResourcesDTO updateUnallocatedResourcePortionValue(Long managerId, Long resourceId, Float lastAllocatedValue, Float newAllocatedValue) {
        if (resourceManagerService.findResourceByManagerId(managerId, resourceId) != null) {
            ResourcesDTO resourcesDTO = new ResourcesDTO();
            Optional<ResourcesEntity> resourcesEntity = resourcesRepository.findById(resourceId);
            resourcesEntity.ifPresent(e -> {
                e.setTotalUnallocatedResourcePortion(getTotalUnallocatedResourcePortion(e.getTotalUnallocatedResourcePortion(), lastAllocatedValue, newAllocatedValue));
                BeanUtils.copyProperties(resourcesRepository.save(e), resourcesDTO);

            });
            return resourcesDTO;
        } else
            throw new RothisExceptionService(Errors.NOT_FOUND_RESOURCE_WHIT_THIS_ID_OR_MANAGER.getMessage());
    }
}
