package ir.mohrazzr.dev.rothis.server.api.billing.service.DTO;

import java.io.Serializable;

/**
 * Created by IntelliJ IDEA at Sunday in 2019/11/10 - 3:12 PM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : Rothis
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Email : mhrz.dev@gmail.com
 * Class Name          : PublicIDsDTO
 **/

public class PublicIDsDTO implements Serializable {
    private static final long serialVersionUID = 9188845372060172739L;

    private Long id;
    private String publicId;

    public PublicIDsDTO() {
    }

    public PublicIDsDTO(Long id, String publicId) {
        this.id = id;
        this.publicId = publicId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPublicId() {
        return publicId;
    }

    public void setPublicId(String publicId) {
        this.publicId = publicId;
    }
}
