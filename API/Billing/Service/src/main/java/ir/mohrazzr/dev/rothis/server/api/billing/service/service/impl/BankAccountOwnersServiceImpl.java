package ir.mohrazzr.dev.rothis.server.api.billing.service.service.impl;

import ir.mohrazzr.dev.rothis.server.api.billing.domain.entity.BankAccountOwnersEntity;
import ir.mohrazzr.dev.rothis.server.api.billing.domain.repository.BankAccountOwnersRepository;
import ir.mohrazzr.dev.rothis.server.api.billing.exception.handler.RothisExceptionService;
import ir.mohrazzr.dev.rothis.server.api.billing.exception.message.Errors;
import ir.mohrazzr.dev.rothis.server.api.billing.service.DTO.BankAccountOwnersDTO;
import ir.mohrazzr.dev.rothis.server.api.billing.service.service.iservice.IBankAccountOwnersService;
import ir.mohrazzr.dev.rothis.server.api.billing.service.service.iservice.IResolverService;
import ir.mohrazzr.dev.rothis.server.api.billing.service.service.iservice.IResourceManagerService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by IntelliJ IDEA at Friday in 2019/11/22 - 4:51 PM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : Rothis
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Email : mhrz.dev@gmail.com
 * Class Name          : BankAccountOwnersServiceImpl
 **/
@Service
public class BankAccountOwnersServiceImpl implements IBankAccountOwnersService {
    private BankAccountOwnersRepository bankAccountOwnersRepository;
    private IResolverService idResolverService;
    private IResourceManagerService resourceManagerService;

    @Autowired
    public BankAccountOwnersServiceImpl(BankAccountOwnersRepository bankAccountOwnersRepository, IResolverService idResolverService, IResourceManagerService resourceManagerService) {
        this.bankAccountOwnersRepository = bankAccountOwnersRepository;
        this.idResolverService = idResolverService;
        this.resourceManagerService = resourceManagerService;
    }

    @Override
    public BankAccountOwnersDTO setOwnerToAccount(String managerPublicUserId, String bankAccountPublicId, String resourcePublicId, String ownerPublicUserId) {
        BankAccountOwnersDTO bankAccountOwnersDTO = new BankAccountOwnersDTO();
        BankAccountOwnersEntity bankAccountOwnersEntity = new BankAccountOwnersEntity();
        if (resourceManagerService.ifManagerOfResource(managerPublicUserId, resourcePublicId)) {
            BankAccountOwnersEntity foundedBankAccountOwnersEntity = bankAccountOwnersRepository.findByUserIdAndBankAccountId(idResolverService.getPersonID(ownerPublicUserId), idResolverService.getBankAccountID(bankAccountPublicId));
            if (foundedBankAccountOwnersEntity == null)
                return getBankAccountOwnersDTO(ownerPublicUserId, bankAccountPublicId, bankAccountOwnersDTO, bankAccountOwnersEntity);
            else
                throw new RothisExceptionService(Errors.RECORD_ALREADY_EXISTS.getMessage() + " , " + Errors.BANK_ACCOUNT_OWNER_ALREADY_EXISTS.getMessage());
        }
        throw new RothisExceptionService(Errors.NOT_FOUND_RESOURCE_WHIT_THIS_ID_OR_MANAGER.getMessage());
    }

    @Override
    public List<BankAccountOwnersDTO> readAllOwnerOfAccount(String managerPublicUserId, String resourcePublicId, String bankAccountPublicId) {
        List<BankAccountOwnersDTO> bankAccountOwnersDTOList = new ArrayList<>();
        if (resourceManagerService.ifManagerOfResource(managerPublicUserId, resourcePublicId)) {
            List<BankAccountOwnersEntity> bankAccountOwnersEntityList = bankAccountOwnersRepository.findAllByBankAccountId(idResolverService.getBankAccountID(bankAccountPublicId));
            for (BankAccountOwnersEntity bankAccountOwnersEntity : bankAccountOwnersEntityList) {
                BankAccountOwnersDTO bankAccountOwnersDTO = new BankAccountOwnersDTO();
                ModelMapper modelMapper = new ModelMapper();
                modelMapper.map(bankAccountOwnersEntity,bankAccountOwnersDTO);
                bankAccountOwnersDTOList.add(bankAccountOwnersDTO);
            }
            return bankAccountOwnersDTOList;
        } else throw new RothisExceptionService(Errors.NOT_FOUND_RESOURCE_WHIT_THIS_ID_OR_MANAGER.getMessage());
    }

    @Override
    public BankAccountOwnersDTO updateOwnerToAccount(String managerPublicUserId, String oldOwnerPublicId, String newOwnerPublicId, String bankAccountPublicId, String resourcePublicId) {
        BankAccountOwnersDTO bankAccountOwnersDTO = new BankAccountOwnersDTO();
        if (resourceManagerService.ifManagerOfResource(managerPublicUserId, resourcePublicId)) {
            if (resourceManagerService.ifManagerOfResource(managerPublicUserId, resourcePublicId)) {
                BankAccountOwnersEntity foundedBankAccountOwnersEntity = bankAccountOwnersRepository.findByUserIdAndBankAccountId(idResolverService.getPersonID(oldOwnerPublicId), idResolverService.getBankAccountID(bankAccountPublicId));
                if (foundedBankAccountOwnersEntity != null)
                    return getBankAccountOwnersDTO(newOwnerPublicId, bankAccountPublicId, bankAccountOwnersDTO, foundedBankAccountOwnersEntity);
                else
                    throw new RothisExceptionService(Errors.COULD_NOT_UPDATE_RECORD.getMessage() + " , " + Errors.NO_PERSON_FOUND_WITH_PUBLIC_USER_ID.getMessage());
            } else
                throw new RothisExceptionService(Errors.NOT_FOUND_RESOURCE_WHIT_THIS_ID_OR_MANAGER.getMessage() + " New account owner public user id : " + newOwnerPublicId);
        } else
            throw new RothisExceptionService(Errors.NOT_FOUND_RESOURCE_WHIT_THIS_ID_OR_MANAGER.getMessage());
    }

    private BankAccountOwnersDTO getBankAccountOwnersDTO(String newOwnerPublicId, String bankAccountPublicId, BankAccountOwnersDTO bankAccountOwnersDTO, BankAccountOwnersEntity foundedBankAccountOwnersEntity) {
        foundedBankAccountOwnersEntity.setBankAccountId(idResolverService.getBankAccountID(bankAccountPublicId));
        foundedBankAccountOwnersEntity.setUserId(idResolverService.getPersonID(newOwnerPublicId));
        BankAccountOwnersEntity resultBankAccountOwnersEntity = bankAccountOwnersRepository.save(foundedBankAccountOwnersEntity);
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.map(resultBankAccountOwnersEntity, bankAccountOwnersDTO);
        return bankAccountOwnersDTO;
    }
}
