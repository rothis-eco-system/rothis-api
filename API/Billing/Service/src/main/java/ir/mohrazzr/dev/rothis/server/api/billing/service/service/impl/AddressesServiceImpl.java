package ir.mohrazzr.dev.rothis.server.api.billing.service.service.impl;

import ir.mohrazzr.dev.rothis.server.api.billing.domain.entity.AddressesEntity;
import ir.mohrazzr.dev.rothis.server.api.billing.domain.repository.AddressesRepository;
import ir.mohrazzr.dev.rothis.server.api.billing.exception.handler.RothisExceptionService;
import ir.mohrazzr.dev.rothis.server.api.billing.exception.message.Errors;
import ir.mohrazzr.dev.rothis.server.api.billing.service.DTO.AddressesDTO;
import ir.mohrazzr.dev.rothis.server.api.billing.service.DTO.OperationStatusDTO;
import ir.mohrazzr.dev.rothis.server.api.billing.service.DTO.PublicIDsDTO;
import ir.mohrazzr.dev.rothis.server.api.billing.service.DTO.enums.OperationCode;
import ir.mohrazzr.dev.rothis.server.api.billing.service.DTO.enums.OperationName;
import ir.mohrazzr.dev.rothis.server.api.billing.service.DTO.enums.OperationStatus;
import ir.mohrazzr.dev.rothis.server.api.billing.service.service.iservice.IAddressesService;
import ir.mohrazzr.dev.rothis.server.api.billing.service.service.iservice.IPublicIDsService;
import ir.mohrazzr.dev.rothis.server.api.billing.service.service.iservice.IResolverService;
import ir.mohrazzr.dev.rothis.server.api.billing.service.service.iservice.IUsersService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by IntelliJ IDEA at Saturday in 2019/11/09 - 5:02 PM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : Rothis
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Email : mhrz.dev@gmail.com
 * Class Name          : AddressesServiceImpl
 **/
@Service
public class AddressesServiceImpl implements IAddressesService {
    private AddressesRepository addressesRepository;
    private IPublicIDsService publicIDsService;
    private IResolverService idResolverService;
    private IUsersService usersService;

    @Autowired
    public AddressesServiceImpl(AddressesRepository addressesRepository, IPublicIDsService publicIDsService, IResolverService idResolverService, IUsersService usersService) {
        this.addressesRepository = addressesRepository;
        this.publicIDsService = publicIDsService;
        this.idResolverService = idResolverService;
        this.usersService = usersService;
    }

    @Override
    public AddressesDTO createAddress(AddressesDTO inDTO, Long personId) {
        return getAddressesDTO(inDTO, personId);
    }

    @Override
    public AddressesDTO createAddress(AddressesDTO inDTO, String personPublicId) {
        return getAddressesDTO(inDTO, idResolverService.getPersonID(personPublicId));
    }

    @Override
    public AddressesDTO readAddress(String publicUserId, String addressId) {
        AddressesDTO addressesDTO = new AddressesDTO();
        Long personId = idResolverService.getPersonID(publicUserId);
        PublicIDsDTO publicIDsDTO = publicIDsService.findByPublicId(addressId);
        AddressesEntity addressesEntity = addressesRepository.findByPersonsIdAndPublicId(personId, publicIDsDTO.getId());
        BeanUtils.copyProperties(addressesEntity, addressesDTO);
        addressesDTO.setPublicAddressId(publicIDsDTO.getPublicId());
        return addressesDTO;
    }

    @Override
    public AddressesDTO updateAddress(AddressesDTO inDTO, String publicUserId, String publicAddressId) {
        PublicIDsDTO publicIDsDTO = publicIDsService.findByPublicId(publicAddressId);
        Long personId = idResolverService.getPersonID(publicUserId);
        AddressesEntity foundedAddressesEntity = addressesRepository.findByPersonsIdAndPublicId(personId, publicIDsDTO.getId());
        AddressesEntity resultAddressesEntity = new AddressesEntity();
        if (foundedAddressesEntity != null) {
            PublicIDsDTO updatedPublicIDsDTO = publicIDsService.updatePublicId(publicIDsDTO.getPublicId());
            foundedAddressesEntity.setPublicId(updatedPublicIDsDTO.getId());
            foundedAddressesEntity.setIsPrimary(inDTO.getPrimary());
            foundedAddressesEntity.setCityCode(inDTO.getCityCode());
            foundedAddressesEntity.setAddress(inDTO.getAddress());
            foundedAddressesEntity.setPostalCode(inDTO.getPostalCode());
            resultAddressesEntity = new ModelMapper().map(foundedAddressesEntity, AddressesEntity.class);
            AddressesDTO resultAddressesDTO = new ModelMapper().map(resultAddressesEntity, AddressesDTO.class);
            resultAddressesDTO.setPublicAddressId(updatedPublicIDsDTO.getPublicId());
            return resultAddressesDTO;
        } else
            throw new RothisExceptionService(Errors.COULD_NOT_UPDATE_RECORD.getMessage() + "," + Errors.NOT_FOUND_PERSON_ADDRESS.getMessage());
    }

    @Override
    public OperationStatusDTO deleteAddress(String publicUserId, String publicId) {
        if (usersService.findUserByPublicUserId(publicUserId) != null) {
            PublicIDsDTO publicIDsDTO = publicIDsService.findByPublicId(publicId);
            if (publicIDsDTO != null) {
                AddressesEntity foundedAddressesEntity = addressesRepository.findByPublicId(publicIDsDTO.getId());
                if (foundedAddressesEntity != null) {
                    addressesRepository.delete(foundedAddressesEntity);
                    return new OperationStatusDTO(OperationName.DELETE.name(),
                            OperationStatus.SUCCESS.name(), OperationCode.CODE200.name(),
                            Errors.DELETE_RECORD_SUCCESSFULLY.getMessage());
                }
            }
        }
        return new OperationStatusDTO(OperationName.DELETE.name(),
                OperationStatus.ERROR.name(), OperationCode.CODE400.name(),
                Errors.COULD_NOT_DELETE_RECORD.getMessage());
    }

    @Override
    public List<AddressesDTO> readAllAddressesByPersonId(Long personId) {
        List<AddressesDTO> addressesDTOList = new ArrayList<>();
        List<AddressesEntity> addressesEntityList = addressesRepository.findAllByPersonsId(personId);
        for (AddressesEntity addressesEntity : addressesEntityList) {
            PublicIDsDTO addressPublicIDsDTO = publicIDsService.findById(addressesEntity.getPublicId());
            AddressesDTO addressesDTO = new AddressesDTO();
            BeanUtils.copyProperties(addressesEntity, addressesDTO);
            addressesDTO.setPublicAddressId(addressPublicIDsDTO.getPublicId());
            addressesDTOList.add(addressesDTO);
        }
        return addressesDTOList;
    }

    @Override
    public List<AddressesDTO> readAllAddressesByPublicUserId(String publicUserId) {
        Long personId = idResolverService.getPersonID(publicUserId);
        return readAllAddressesByPersonId(personId);
    }

    private AddressesDTO getAddressesDTO(AddressesDTO inDTO, Long personId) {
        AddressesDTO resultAddressesDTO = new AddressesDTO();
        AddressesEntity addressesEntity = new AddressesEntity();
        PublicIDsDTO publicIDsDTO = publicIDsService.generatePublicId(10);
        BeanUtils.copyProperties(inDTO, addressesEntity);
        addressesEntity.setPersonsId(personId);
        addressesEntity.setPublicId(publicIDsDTO.getId());
        AddressesEntity resultEntity = addressesRepository.save(addressesEntity);
        BeanUtils.copyProperties(resultEntity, resultAddressesDTO);
        resultAddressesDTO.setPublicAddressId(publicIDsDTO.getPublicId());
        return resultAddressesDTO;
    }

}
