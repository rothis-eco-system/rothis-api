package ir.mohrazzr.dev.rothis.server.api.billing.service.service.impl;

import ir.mohrazzr.dev.rothis.server.api.billing.domain.entity.EmailsEntity;
import ir.mohrazzr.dev.rothis.server.api.billing.domain.repository.EmailsRepository;
import ir.mohrazzr.dev.rothis.server.api.billing.exception.handler.RothisExceptionService;
import ir.mohrazzr.dev.rothis.server.api.billing.exception.message.Errors;
import ir.mohrazzr.dev.rothis.server.api.billing.service.DTO.EmailsDTO;
import ir.mohrazzr.dev.rothis.server.api.billing.service.DTO.OperationStatusDTO;
import ir.mohrazzr.dev.rothis.server.api.billing.service.DTO.UsersDTO;
import ir.mohrazzr.dev.rothis.server.api.billing.service.service.iservice.IEmailsService;
import ir.mohrazzr.dev.rothis.server.api.billing.service.service.iservice.IUsersService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by IntelliJ IDEA at Thursday in 2019/11/07 - 8:43 AM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : Rothis
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Email : mhrz.dev@gmail.com
 * Class Name          : EmailsServiceImpl
 **/
@Service
public class EmailsServiceImpl implements IEmailsService {

    private EmailsRepository emailsRepository;
    private IUsersService usersService;

    @Autowired
    public EmailsServiceImpl(EmailsRepository emailsRepository, IUsersService iUsersService) {
        this.emailsRepository = emailsRepository;
        this.usersService = iUsersService;
    }

    @Override
    public EmailsDTO createEmail(EmailsDTO inDTO, String publicUserId) {
        EmailsDTO emailsDTO = new EmailsDTO();
        EmailsEntity emailsEntity = new EmailsEntity();
        UsersDTO usersDTO = usersService.findUserByPublicUserId(publicUserId);
        BeanUtils.copyProperties(inDTO, emailsEntity);
        EmailsEntity foundedEmailsEntity = emailsRepository.findByEmail(emailsEntity.getEmail());
        if (foundedEmailsEntity == null) {
            emailsEntity.setUserId(usersDTO.getId());
            emailsEntity.setIsActive(false);
            emailsEntity.setIsPrimary(true);
            EmailsEntity resultEntity = emailsRepository.save(emailsEntity);
            BeanUtils.copyProperties(resultEntity, emailsDTO);
            return emailsDTO;
        } else
            throw new RothisExceptionService(Errors.EMAIL_ADDRESS_EXISTS.getMessage() + inDTO.getEmail());
    }

    @Override
    public EmailsDTO readEmail(EmailsDTO inDTO, String publicUserId) {
        EmailsDTO emailsDTO = new EmailsDTO();
        UsersDTO usersDTO = usersService.findUserByPublicUserId(publicUserId);
        EmailsEntity foundedEmailsEntity = emailsRepository.findByUserId(usersDTO.getId());
        if (foundedEmailsEntity != null) {
            BeanUtils.copyProperties(foundedEmailsEntity, emailsDTO);
            return emailsDTO;
        } else
            throw new RothisExceptionService(Errors.EMAIL_ADDRESS_EXISTS.getMessage() + inDTO.getEmail());
    }

    @Override
    public EmailsDTO updateEmail(EmailsDTO inDTO, String publicUserId) {
        return null;
    }

    @Override
    public OperationStatusDTO deleteEmail(EmailsDTO inDTO, String publicUserId) {
        return null;
    }
}
