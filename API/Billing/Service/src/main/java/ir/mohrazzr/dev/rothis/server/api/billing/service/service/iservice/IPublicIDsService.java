package ir.mohrazzr.dev.rothis.server.api.billing.service.service.iservice;

import ir.mohrazzr.dev.rothis.server.api.billing.service.DTO.PublicIDsDTO;

/**
 * Created by IntelliJ IDEA at Sunday in 2019/11/10 - 3:18 PM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : Rothis
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Email : mhrz.dev@gmail.com
 * Class Name          : IPublicIdsService
 **/

public interface IPublicIDsService {
    PublicIDsDTO generatePublicId(Integer length);
    PublicIDsDTO findById(Long publicUserId);
    PublicIDsDTO findByPublicId(String publicUserId);
    PublicIDsDTO updatePublicId(String publicId);
    PublicIDsDTO updatePublicId(Long id);
    void deletePublicId(Long id);
}
