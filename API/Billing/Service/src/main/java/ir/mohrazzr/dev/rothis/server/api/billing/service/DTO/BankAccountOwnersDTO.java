package ir.mohrazzr.dev.rothis.server.api.billing.service.DTO;

/**
 * Created by IntelliJ IDEA at Friday in 2019/11/22 - 4:37 PM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : Rothis
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Email : mhrz.dev@gmail.com
 * Class Name          : BankAccountOwnersDTO
 **/

public class BankAccountOwnersDTO {
    private Long id;
    private Long bankAccountId;
    private Long userId;

    public BankAccountOwnersDTO() {
    }

    public BankAccountOwnersDTO(Long id, Long bankAccountId, Long userId) {
        this.id = id;
        this.bankAccountId = bankAccountId;
        this.userId = userId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getBankAccountId() {
        return bankAccountId;
    }

    public void setBankAccountId(Long bankAccountId) {
        this.bankAccountId = bankAccountId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "BankAccountOwnersDTO{" +
                "id=" + id +
                ", bankAccountId=" + bankAccountId +
                ", userId=" + userId +
                '}';
    }
}
