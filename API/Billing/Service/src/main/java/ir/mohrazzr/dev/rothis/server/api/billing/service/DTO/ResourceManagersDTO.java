package ir.mohrazzr.dev.rothis.server.api.billing.service.DTO;

import java.io.Serializable;

/**
 * Created by IntelliJ IDEA at Thursday in 2019/11/14 - 12:25 PM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : Rothis
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Email : mhrz.dev@gmail.com
 * Class Name          : ResourceManagersDTO
 **/

public class ResourceManagersDTO implements Serializable {
    private static final long serialVersionUID = 3950564365774149417L;

    private Long id;
    private Long personId;
    private Long resourceId;

    public ResourceManagersDTO() {
    }

    public ResourceManagersDTO(Long id, Long personId, Long resourceId) {
        this.id = id;
        this.personId = personId;
        this.resourceId = resourceId;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPersonId() {
        return personId;
    }

    public void setPersonId(Long personId) {
        this.personId = personId;
    }

    public Long getResourceId() {
        return resourceId;
    }

    public void setResourceId(Long resourceId) {
        this.resourceId = resourceId;
    }
}
