package ir.mohrazzr.dev.rothis.server.api.billing.service.service.impl;

import ir.mohrazzr.dev.rothis.server.api.billing.domain.entity.ResourceManagersEntity;
import ir.mohrazzr.dev.rothis.server.api.billing.domain.repository.ResourceManagersRepository;
import ir.mohrazzr.dev.rothis.server.api.billing.service.DTO.ResourceManagersDTO;
import ir.mohrazzr.dev.rothis.server.api.billing.exception.handler.RothisExceptionService;
import ir.mohrazzr.dev.rothis.server.api.billing.exception.message.Errors;
import ir.mohrazzr.dev.rothis.server.api.billing.service.service.iservice.IPublicIDsService;
import ir.mohrazzr.dev.rothis.server.api.billing.service.service.iservice.IResolverService;
import ir.mohrazzr.dev.rothis.server.api.billing.service.service.iservice.IResourceManagerService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by IntelliJ IDEA at Thursday in 2019/11/14 - 5:08 PM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : Rothis
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Email : mhrz.dev@gmail.com
 * Class Name          : ResourceManagersServiceImpl
 **/
@Service
public class ResourceManagersServiceImpl implements IResourceManagerService {
    private ResourceManagersRepository resourceManagersRepository;
    private IPublicIDsService publicIDsService;
    private IResolverService idResolverService;

    @Autowired
    public ResourceManagersServiceImpl(ResourceManagersRepository resourceManagersRepository, IPublicIDsService publicIDsService, IResolverService idResolverService) {
        this.resourceManagersRepository = resourceManagersRepository;
        this.publicIDsService = publicIDsService;
        this.idResolverService = idResolverService;
    }

    @Override
    public ResourceManagersDTO createManager(String publicUserId, String publicResourceId) {
        ResourceManagersDTO resourceManagersDTO = new ResourceManagersDTO();
        ResourceManagersEntity resourceManagersEntity = new ResourceManagersEntity();
        resourceManagersEntity.setPersonId(idResolverService.getPersonID(publicUserId));
        resourceManagersEntity.setResourceId(idResolverService.getResourceID(publicResourceId));
        ResourceManagersEntity resultResourceManagersEntity = resourceManagersRepository.save(resourceManagersEntity);
        BeanUtils.copyProperties(resultResourceManagersEntity, resourceManagersDTO);
        return resourceManagersDTO;
    }

    @Override
    public ResourceManagersDTO findResourceByManagerId(Long personId, Long resourceId) {
        ResourceManagersDTO resourceManagersDTO = new ResourceManagersDTO();
        ResourceManagersEntity resourceManagersEntity = resourceManagersRepository.findByPersonIdAndResourceId(personId, resourceId);
        if (resourceManagersEntity != null) {
            BeanUtils.copyProperties(resourceManagersEntity, resourceManagersDTO);
            return resourceManagersDTO;
        } else
            throw new RothisExceptionService(Errors.NOT_FOUND_RESOURCE_WHIT_THIS_ID_OR_MANAGER.getMessage());
    }

    @Override
    public List<Long> findAllResourceIdByManagerId(String publicUserId) {
        List<ResourceManagersEntity> managersEntityList = resourceManagersRepository.findAllByPersonId(idResolverService.getPersonID(publicUserId));
        List<Long> ids = new ArrayList<>();
        for (ResourceManagersEntity resourceManagersEntity : managersEntityList) {
            ids.add(resourceManagersEntity.getResourceId());
        }
        return ids;
    }

    @Override
    public Boolean ifManagerOfResource(String managerPublicId, String resourcePublicId) {
        Long personId = idResolverService.getPersonID(managerPublicId);
        Long resourceId = idResolverService.getResourceID(resourcePublicId);
        return resourceManagersRepository.existsByPersonIdAndResourceId(personId, resourceId);
    }

    @Override
    public Boolean deleteResource(ResourceManagersDTO resourceManagersDTO) {
        try {
            ResourceManagersEntity resourceManagersEntity = new ModelMapper().map(resourceManagersDTO, ResourceManagersEntity.class);
            resourceManagersRepository.delete(resourceManagersEntity);
            return true;
        } catch (RothisExceptionService e) {
            throw new RothisExceptionService(Errors.COULD_NOT_DELETE_RECORD.getMessage());
        }
    }
}
