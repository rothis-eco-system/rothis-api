package ir.mohrazzr.dev.rothis.server.api.billing.service.DTO;

/**
 * Created by IntelliJ IDEA at Wednesday in 2019/11/20 - 8:30 AM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : Rothis
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Email : mhrz.dev@gmail.com
 * Class Name          : IdResolverDTO
 **/

public class IdResolverDTO {
    private Long managerId;
    private Long ownerId;
    private Long resourceId;
    private Long bankAccountsId;

    public IdResolverDTO() {
    }

    public IdResolverDTO(Long managerId, Long ownerId, Long resourceId, Long bankAccountsId) {
        this.managerId = managerId;
        this.ownerId = ownerId;
        this.resourceId = resourceId;
        this.bankAccountsId = bankAccountsId;
    }

    public Long getManagerId() {
        return managerId;
    }

    public void setManagerId(Long managerId) {
        this.managerId = managerId;
    }

    public Long getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(Long ownerId) {
        this.ownerId = ownerId;
    }

    public Long getResourceId() {
        return resourceId;
    }

    public void setResourceId(Long resourceId) {
        this.resourceId = resourceId;
    }

    public Long getBankAccountsId() {
        return bankAccountsId;
    }

    public void setBankAccountsId(Long bankAccountsId) {
        this.bankAccountsId = bankAccountsId;
    }
}
