package ir.mohrazzr.dev.rothis.server.api.billing.service.DTO;

import java.io.Serializable;
import java.util.List;

/**
 * Created by IntelliJ IDEA at Friday in 2019/11/08 - 7:20 PM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : Rothis
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Email : mhrz.dev@gmail.com
 * Class Name          : PersonsDTO
 **/

public class PersonsDTO implements Serializable {
    private static final long serialVersionUID = 7942233998841816133L;
    private Long id;
    private Long userId;
    private String publicUserId;
    private String userName;
    private String firstName;
    private String lastName;
    private String fatherName;
    private String nationalCode;
    private String passPortCode;
    private String gender;
    private String dateOfBirth;
    private List<AddressesDTO> addresses;
    private List<PhoneNumbersDTO> phoneNumbers;

    public PersonsDTO() {
    }

    public PersonsDTO(Long id, Long userId, String publicUserId, String userName, String firstName, String lastName, String fatherName, String nationalCode, String passPortCode, String gender, String dateOfBirth, List<AddressesDTO> addresses, List<PhoneNumbersDTO> phoneNumbers) {
        this.id = id;
        this.userId = userId;
        this.userName = userName;
        this.publicUserId = publicUserId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.fatherName = fatherName;
        this.nationalCode = nationalCode;
        this.passPortCode = passPortCode;
        this.gender = gender;
        this.dateOfBirth = dateOfBirth;
        this.addresses = addresses;
        this.phoneNumbers = phoneNumbers;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getPublicUserId() {
        return publicUserId;
    }

    public void setPublicUserId(String publicUserId) {
        this.publicUserId = publicUserId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFatherName() {
        return fatherName;
    }

    public void setFatherName(String fatherName) {
        this.fatherName = fatherName;
    }

    public String getNationalCode() {
        return nationalCode;
    }

    public void setNationalCode(String nationalCode) {
        this.nationalCode = nationalCode;
    }

    public String getPassPortCode() {
        return passPortCode;
    }

    public void setPassPortCode(String passPortCode) {
        this.passPortCode = passPortCode;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public List<AddressesDTO> getAddresses() {
        return addresses;
    }

    public void setAddresses(List<AddressesDTO> addresses) {
        this.addresses = addresses;
    }

    public List<PhoneNumbersDTO> getPhoneNumbers() {
        return phoneNumbers;
    }

    public void setPhoneNumbers(List<PhoneNumbersDTO> phoneNumbers) {
        this.phoneNumbers = phoneNumbers;
    }

    @Override
    public String toString() {
        return "PersonsDTO{" +
                "id=" + id +
                ", userId=" + userId +
                ", publicUserId='" + publicUserId + '\'' +
                ", userName='" + userName + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", fatherName='" + fatherName + '\'' +
                ", nationalCode='" + nationalCode + '\'' +
                ", passPortCode='" + passPortCode + '\'' +
                ", gender='" + gender + '\'' +
                ", dateOfBirth='" + dateOfBirth + '\'' +
                ", addresses=" + addresses.toString() +
                ", phoneNumbers=" + phoneNumbers.toString() +
                '}';
    }
}
