package ir.mohrazzr.dev.rothis.server.api.billing.service.DTO.enums;

/**
 * Created by IntelliJ IDEA at Thursday in 2019/10/31 - 5:11 PM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : Rothis
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Email : mhrz.dev@gmail.com
 * Class Name          : OperationName
 **/

public enum OperationName {
DELETE
}
