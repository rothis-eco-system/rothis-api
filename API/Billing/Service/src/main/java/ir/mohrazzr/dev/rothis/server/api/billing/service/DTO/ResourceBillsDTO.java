package ir.mohrazzr.dev.rothis.server.api.billing.service.DTO;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

/**
 * Created by IntelliJ IDEA at Monday in 2019/11/25 - 7:13 PM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : Rothis
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Email : mhrz.dev@gmail.com
 * Class Name          : ResourceBillsDTO
 **/

public class ResourceBillsDTO implements Serializable {
    private static final long serialVersionUID = 7202567010193703566L;

    private Long id;
    private Long publicId;
    private Long resourceId;
    private Long bankAccountId;
    private String publicIdString;
    private String resourceIdString;
    private String bankAccountIdString;
    private BankAccountsDTO bankAccount;
    private String resourceBillIdentityCode;
    private Long exporterManagerId;
    private Integer paymentDeadLineDays;
    private Date paymentDeadLine;
    private String paymentDeadLinePersianDate;
    private BigDecimal totalPrice;
    private BigDecimal pricePerSecond;
    private BigDecimal pricePerPortion;
    private String totalPriceInAlphabet;
    private String pricePerPortionInAlphabet;
    private Long billCount;
    private Integer roundCount;
    private String roundCountInAlphabet;
    private Timestamp exportTime;
    private String exportTimePersianDate;
    private String concern;
    private String descriptions;
   private List<BillsDTO> bills;
    public ResourceBillsDTO() {
    }

    public ResourceBillsDTO(Long id, Long publicId, Long resourceId, Long bankAccountId, String publicIdString, String resourceIdString, String bankAccountIdString, BankAccountsDTO bankAccount, String resourceBillIdentityCode, Long exporterManagerId, Integer paymentDeadLineDays, Date paymentDeadLine, String paymentDeadLinePersianDate, BigDecimal totalPrice, BigDecimal pricePerSecond, BigDecimal pricePerPortion, String totalPriceInAlphabet, String pricePerPortionInAlphabet, Long billCount, Integer roundCount, String roundCountInAlphabet, Timestamp exportTime, String exportTimePersianDate, String concern, String descriptions, List<BillsDTO> bills) {
        this.id = id;
        this.publicId = publicId;
        this.resourceId = resourceId;
        this.bankAccountId = bankAccountId;
        this.publicIdString = publicIdString;
        this.resourceIdString = resourceIdString;
        this.bankAccountIdString = bankAccountIdString;
        this.bankAccount = bankAccount;
        this.resourceBillIdentityCode = resourceBillIdentityCode;
        this.exporterManagerId = exporterManagerId;
        this.paymentDeadLineDays = paymentDeadLineDays;
        this.paymentDeadLine = paymentDeadLine;
        this.paymentDeadLinePersianDate = paymentDeadLinePersianDate;
        this.totalPrice = totalPrice;
        this.pricePerSecond = pricePerSecond;
        this.pricePerPortion = pricePerPortion;
        this.totalPriceInAlphabet = totalPriceInAlphabet;
        this.pricePerPortionInAlphabet = pricePerPortionInAlphabet;
        this.billCount = billCount;
        this.roundCount = roundCount;
        this.roundCountInAlphabet = roundCountInAlphabet;
        this.exportTime = exportTime;
        this.exportTimePersianDate = exportTimePersianDate;
        this.concern = concern;
        this.descriptions = descriptions;
        this.bills = bills;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPublicId() {
        return publicId;
    }

    public void setPublicId(Long publicId) {
        this.publicId = publicId;
    }

    public Long getResourceId() {
        return resourceId;
    }

    public void setResourceId(Long resourceId) {
        this.resourceId = resourceId;
    }

    public Long getBankAccountId() {
        return bankAccountId;
    }

    public void setBankAccountId(Long bankAccountId) {
        this.bankAccountId = bankAccountId;
    }

    public String getPublicIdString() {
        return publicIdString;
    }

    public void setPublicIdString(String publicIdString) {
        this.publicIdString = publicIdString;
    }

    public String getResourceIdString() {
        return resourceIdString;
    }

    public void setResourceIdString(String resourceIdString) {
        this.resourceIdString = resourceIdString;
    }

    public String getBankAccountIdString() {
        return bankAccountIdString;
    }

    public String getResourceBillIdentityCode() {
        return resourceBillIdentityCode;
    }

    public void setResourceBillIdentityCode(String resourceBillIdentityCode) {
        this.resourceBillIdentityCode = resourceBillIdentityCode;
    }

    public Long getExporterManagerId() {
        return exporterManagerId;
    }

    public void setExporterManagerId(Long exporterManagerId) {
        this.exporterManagerId = exporterManagerId;
    }

    public Integer getPaymentDeadLineDays() {
        return paymentDeadLineDays;
    }

    public void setPaymentDeadLineDays(Integer paymentDeadLineDays) {
        this.paymentDeadLineDays = paymentDeadLineDays;
    }

    public Date getPaymentDeadLine() {
        return paymentDeadLine;
    }

    public void setPaymentDeadLine(Date paymentDeadLine) {
        this.paymentDeadLine = paymentDeadLine;
    }

    public String getPaymentDeadLinePersianDate() {
        return paymentDeadLinePersianDate;
    }

    public void setPaymentDeadLinePersianDate(String paymentDeadLinePersianDate) {
        this.paymentDeadLinePersianDate = paymentDeadLinePersianDate;
    }

    public void setBankAccountIdString(String bankAccountIdString) {
        this.bankAccountIdString = bankAccountIdString;
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }

    public BigDecimal getPricePerSecond() {
        return pricePerSecond;
    }

    public void setPricePerSecond(BigDecimal pricePerSecond) {
        this.pricePerSecond = pricePerSecond;
    }

    public String getTotalPriceInAlphabet() {
        return totalPriceInAlphabet;
    }

    public void setTotalPriceInAlphabet(String totalPriceInAlphabet) {
        this.totalPriceInAlphabet = totalPriceInAlphabet;
    }

    public String getPricePerPortionInAlphabet() {
        return pricePerPortionInAlphabet;
    }

    public void setPricePerPortionInAlphabet(String pricePerPortionInAlphabet) {
        this.pricePerPortionInAlphabet = pricePerPortionInAlphabet;
    }

    public Long getBillCount() {
        return billCount;
    }

    public void setBillCount(Long billCount) {
        this.billCount = billCount;
    }

    public Integer getRoundCount() {
        return roundCount;
    }

    public void setRoundCount(Integer roundCount) {
        this.roundCount = roundCount;
    }

    public String getRoundCountInAlphabet() {
        return roundCountInAlphabet;
    }

    public void setRoundCountInAlphabet(String roundCountInAlphabet) {
        this.roundCountInAlphabet = roundCountInAlphabet;
    }

    public Timestamp getExportTime() {
        return exportTime;
    }

    public void setExportTime(Timestamp exportTime) {
        this.exportTime = exportTime;
    }

    public String getExportTimePersianDate() {
        return exportTimePersianDate;
    }

    public void setExportTimePersianDate(String exportTimePersianDate) {
        this.exportTimePersianDate = exportTimePersianDate;
    }

    public String getConcern() {
        return concern;
    }

    public void setConcern(String concern) {
        this.concern = concern;
    }

    public String getDescriptions() {
        return descriptions;
    }

    public void setDescriptions(String descriptions) {
        this.descriptions = descriptions;
    }

    public List<BillsDTO> getBills() {
        return bills;
    }

    public void setBills(List<BillsDTO> bills) {
        this.bills = bills;
    }

    public BankAccountsDTO getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(BankAccountsDTO bankAccount) {
        this.bankAccount = bankAccount;
    }

    public BigDecimal getPricePerPortion() {
        return pricePerPortion;
    }

    public void setPricePerPortion(BigDecimal pricePerPortion) {
        this.pricePerPortion = pricePerPortion;
    }

    @Override
    public String toString() {
        return "ResourceBillsDTO{" +
                "id=" + id +
                ", publicId=" + publicId +
                ", resourceId=" + resourceId +
                ", bankAccountId=" + bankAccountId +
                ", publicIdString='" + publicIdString + '\'' +
                ", resourceIdString='" + resourceIdString + '\'' +
                ", bankAccountIdString='" + bankAccountIdString + '\'' +
                ", bankAccount=" + bankAccount +
                ", resourceBillIdentityCode='" + resourceBillIdentityCode + '\'' +
                ", exporterManagerId=" + exporterManagerId +
                ", paymentDeadLineDays=" + paymentDeadLineDays +
                ", paymentDeadLine=" + paymentDeadLine +
                ", paymentDeadLinePersianDate='" + paymentDeadLinePersianDate + '\'' +
                ", totalPrice=" + totalPrice +
                ", pricePerSecond=" + pricePerSecond +
                ", pricePerPortion=" + pricePerPortion +
                ", totalPriceInAlphabet='" + totalPriceInAlphabet + '\'' +
                ", pricePerPortionInAlphabet='" + pricePerPortionInAlphabet + '\'' +
                ", billCount=" + billCount +
                ", roundCount=" + roundCount +
                ", roundCountInAlphabet='" + roundCountInAlphabet + '\'' +
                ", exportTime=" + exportTime +
                ", exportTimePersianDate='" + exportTimePersianDate + '\'' +
                ", concern='" + concern + '\'' +
                ", descriptions='" + descriptions + '\'' +
                ", bills=" + bills +
                '}';
    }
}
