package ir.mohrazzr.dev.rothis.server.api.billing.service.service.impl;

import ir.mohrazzr.dev.rothis.server.api.billing.domain.entity.BankAccountsEntity;
import ir.mohrazzr.dev.rothis.server.api.billing.domain.repository.BankAccountsRepository;
import ir.mohrazzr.dev.rothis.server.api.billing.service.DTO.*;
import ir.mohrazzr.dev.rothis.server.api.billing.service.DTO.enums.OperationCode;
import ir.mohrazzr.dev.rothis.server.api.billing.service.DTO.enums.OperationName;
import ir.mohrazzr.dev.rothis.server.api.billing.service.DTO.enums.OperationStatus;
import ir.mohrazzr.dev.rothis.server.api.billing.service.service.iservice.*;
import ir.mohrazzr.dev.rothis.server.api.billing.exception.handler.RothisExceptionService;
import ir.mohrazzr.dev.rothis.server.api.billing.exception.message.Errors;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by IntelliJ IDEA at Thursday in 2019/11/21 - 12:21 PM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : Rothis
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Email : mhrz.dev@gmail.com
 * Class Name          : BankAccountsServiceImpl
 **/
@Service
public class BankAccountsServiceImpl implements IBankAccountsService {
    private BankAccountsRepository bankAccountsRepository;
    private IResolverService idResolverService;
    private IResourceManagerService resourceManagerService;
    private IPublicIDsService publicIDsService;
    private IBankAccountOwnersService bankAccountOwnersService;
    private IPersonsService personsService;
    private ModelMapper modelMapper;

    @Autowired
    public BankAccountsServiceImpl(BankAccountsRepository bankAccountsRepository, IResolverService idResolverService, IResourceManagerService resourceManagerService, IPublicIDsService publicIDsService, IBankAccountOwnersService bankAccountOwnersService, IPersonsService personsService) {
        this.bankAccountsRepository = bankAccountsRepository;
        this.idResolverService = idResolverService;
        this.resourceManagerService = resourceManagerService;
        this.publicIDsService = publicIDsService;
        this.bankAccountOwnersService = bankAccountOwnersService;
        this.personsService = personsService;
        this.modelMapper = new ModelMapper();
    }

    @Override
    public BankAccountsDTO createBankAccount(BankAccountsDTO inDTO, String managerPublicUserId, String publicResourceId) {
        BankAccountsDTO bankAccountsDTO = new BankAccountsDTO();
        if (resourceManagerService.ifManagerOfResource(managerPublicUserId, publicResourceId)) {
            if (bankAccountsRepository.findByAccountNumberOrCardNumber(inDTO.getAccountNumber(), inDTO.getCardNumber()) == null) {
                BankAccountsEntity bankAccountsEntity = new BankAccountsEntity();
                modelMapper.map(inDTO, bankAccountsEntity);
                bankAccountsEntity.setResourceId(idResolverService.getResourceID(publicResourceId));
                PublicIDsDTO publicIDsDTO = publicIDsService.generatePublicId(25);
                bankAccountsEntity.setPublicId(publicIDsDTO.getId());
                BankAccountsEntity resultBankAccountsEntity = bankAccountsRepository.save(bankAccountsEntity);
                if (resultBankAccountsEntity != null) {
                    modelMapper.map(resultBankAccountsEntity, bankAccountsDTO);
                    bankAccountsDTO.setPublicIdString(publicIDsDTO.getPublicId());
                    bankAccountsDTO.setResourcePublicId(publicResourceId);
                    List<PersonsDTO> personsDTOList = new ArrayList<>();
                    for (String owner : inDTO.getOwnerList()) {
                        BankAccountOwnersDTO bankAccountOwnersDTO = bankAccountOwnersService.setOwnerToAccount(managerPublicUserId, publicIDsDTO.getPublicId(), publicResourceId, owner);
                        personsDTOList.add(personsService.findById(bankAccountOwnersDTO.getUserId()));
                    }
                    bankAccountsDTO.setOwners(personsDTOList);

                }
                return bankAccountsDTO;
            } else
                throw new RothisExceptionService(Errors.COULD_NOT_INSERT_RECORD.getMessage() + " , " + Errors.BANK_ACCOUNT_ALREADY_EXISTS.getMessage() + inDTO.getAccountNumber());
        } else
            throw new RothisExceptionService(Errors.NOT_FOUND_RESOURCE_WHIT_THIS_ID_OR_MANAGER.getMessage());
    }

    @Override
    public BankAccountsDTO createBankAccountOwner(String managerPublicUserId, String bankAccountPublicId, String resourcePublicId, String newOwnerPublicId) {
        BankAccountOwnersDTO bankAccountOwnersDTO = bankAccountOwnersService.setOwnerToAccount(managerPublicUserId, bankAccountPublicId, resourcePublicId, newOwnerPublicId);
        if (bankAccountOwnersDTO != null)
            return readBankAccount(managerPublicUserId, resourcePublicId, bankAccountPublicId);
        else
            return null;
    }

    @Override
    public BankAccountsDTO readBankAccount(String managerPublicUserId, String publicResourceId, String bankAccountPublicId) {
        BankAccountsDTO bankAccountsDTO = new BankAccountsDTO();
        if (resourceManagerService.ifManagerOfResource(managerPublicUserId, publicResourceId)) {
            PublicIDsDTO bankAccountPublicIDsDTO = publicIDsService.findByPublicId(bankAccountPublicId);
            BankAccountsEntity bankAccountsEntity = bankAccountsRepository.findByPublicId(bankAccountPublicIDsDTO.getId());
            if (bankAccountsEntity != null) {
                ModelMapper modelMapper = new ModelMapper();
                modelMapper.map(bankAccountsEntity, bankAccountsDTO);
                bankAccountsDTO.setPublicIdString(bankAccountPublicIDsDTO.getPublicId());
                bankAccountsDTO.setResourcePublicId(publicResourceId);
                bankAccountsDTO.setOwners(getPersonsDTOList(managerPublicUserId, publicResourceId, bankAccountPublicIDsDTO.getPublicId()));
                return bankAccountsDTO;
            } else
                throw new RothisExceptionService(Errors.NOT_FOUND_BANK_ACCOUNT_WHIT_THIS_PUBLIC_ID.getMessage() + bankAccountPublicIDsDTO.getPublicId());
        } else throw new RothisExceptionService(Errors.NOT_FOUND_RESOURCE_WHIT_THIS_ID_OR_MANAGER.getMessage());
    }

    @Override
    public List<BankAccountsDTO> readAllBankAccount(String publicResourceId, String managerPublicUserId) {
        List<BankAccountsDTO> bankAccountsDTOList = new ArrayList<>();
        if (resourceManagerService.ifManagerOfResource(managerPublicUserId, publicResourceId)) {
            PublicIDsDTO resourcePublicIDsDTO = publicIDsService.findByPublicId(publicResourceId);
            List<BankAccountsEntity> bankAccountsEntityList = bankAccountsRepository.findAllByResourceId(idResolverService.getResourceID(resourcePublicIDsDTO.getPublicId()));
            for (BankAccountsEntity bankAccountsEntity : bankAccountsEntityList) {
                PublicIDsDTO bankAccountPublicIDsDTO = publicIDsService.findById(bankAccountsEntity.getPublicId());
                BankAccountsDTO bankAccountsDTO = new BankAccountsDTO();
                modelMapper.map(bankAccountsEntity, bankAccountsDTO);
                bankAccountsDTO.setResourcePublicId(resourcePublicIDsDTO.getPublicId());
                bankAccountsDTO.setPublicIdString(bankAccountPublicIDsDTO.getPublicId());
                bankAccountsDTO.setOwners(getPersonsDTOList(managerPublicUserId, publicResourceId, bankAccountPublicIDsDTO.getPublicId()));
                bankAccountsDTOList.add(bankAccountsDTO);
            }
            return bankAccountsDTOList;
        } else
            throw new RothisExceptionService(Errors.NOT_FOUND_RESOURCE_WHIT_THIS_ID_OR_MANAGER.getMessage());
    }

    @Override
    public BankAccountsDTO updateBankAccount(BankAccountsDTO inDTO, String managerPublicUserId, String publicResourceId, String bankAccountPublicId) {
        BankAccountsDTO bankAccountsDTO = new BankAccountsDTO();
        if (resourceManagerService.ifManagerOfResource(managerPublicUserId, publicResourceId)) {
            BankAccountsEntity bankAccountsEntity = bankAccountsRepository.findByPublicId(publicIDsService.findByPublicId(bankAccountPublicId).getId());
            bankAccountsEntity.setBankName(inDTO.getBankName());
            bankAccountsEntity.setAccountNumber(inDTO.getAccountNumber());
            bankAccountsEntity.setName(inDTO.getName());
            bankAccountsEntity.setShabaNumber(inDTO.getShabaNumber());
            bankAccountsEntity.setCardNumber(inDTO.getCardNumber());
            bankAccountsEntity.setBranchCode(inDTO.getBranchCode());
            bankAccountsEntity.setBranchName(inDTO.getBranchName());
            bankAccountsEntity.setBranchAddress(inDTO.getBranchAddress());
            bankAccountsEntity.setDescription(inDTO.getDescription());
            BankAccountsEntity resultBankAccountsEntity = bankAccountsRepository.save(bankAccountsEntity);
            if (resultBankAccountsEntity != null) {
                PublicIDsDTO bankAccountPublicIDsDTO = publicIDsService.updatePublicId(bankAccountPublicId);
                modelMapper.map(resultBankAccountsEntity, bankAccountsDTO);
                bankAccountsDTO.setPublicIdString(bankAccountPublicIDsDTO.getPublicId());
                bankAccountsDTO.setResourcePublicId(publicResourceId);
                bankAccountsDTO.setOwners(getPersonsDTOList(managerPublicUserId, publicResourceId, bankAccountPublicIDsDTO.getPublicId()));
                return bankAccountsDTO;
            }
        } else
            throw new RothisExceptionService(Errors.NOT_FOUND_RESOURCE_WHIT_THIS_ID_OR_MANAGER.getMessage());
        return null;
    }

    @Override
    public BankAccountsDTO updateBankAccountOwner(String managerPublicUserId, String oldOwnerPublicId, String newOwnerPublicId, String bankAccountPublicId, String resourcePublicId) {
        BankAccountsEntity bankAccountsEntity = bankAccountsRepository.findByPublicId(publicIDsService.findByPublicId(bankAccountPublicId).getId());
        if (resourceManagerService.ifManagerOfResource(managerPublicUserId, resourcePublicId)) {
            if (bankAccountsEntity != null) {
                bankAccountOwnersService.updateOwnerToAccount(managerPublicUserId, oldOwnerPublicId, newOwnerPublicId, bankAccountPublicId, resourcePublicId);
                return readBankAccount(managerPublicUserId, resourcePublicId, bankAccountPublicId);
            }
        }

        return null;
    }

    @Override
    public OperationStatusDTO deleteBankAccount(String managerPublicUserId, String publicResourceId, String bankAccountPublicId) {
        if (resourceManagerService.ifManagerOfResource(managerPublicUserId, publicResourceId)) {
            BankAccountsEntity bankAccountsEntity = bankAccountsRepository.findByPublicId(publicIDsService.findByPublicId(bankAccountPublicId).getId());
            if (bankAccountsEntity != null) {
                if (checkResourceToBankAccount(publicResourceId, bankAccountPublicId)) {
                    bankAccountsRepository.delete(bankAccountsEntity);
                    return new OperationStatusDTO(OperationName.DELETE.name(),
                            OperationStatus.SUCCESS.name(), OperationCode.CODE200.name(),
                            Errors.DELETE_RECORD_SUCCESSFULLY.getMessage());
                }
            }
        }
        return new OperationStatusDTO(OperationName.DELETE.name(),
                OperationStatus.ERROR.name(), OperationCode.CODE400.name(),
                Errors.COULD_NOT_DELETE_RECORD.getMessage());
    }

    @Override
    public boolean bankAccountExists(String resourcePublicId, String bankAccountPublicId) {
        return checkResourceToBankAccount(resourcePublicId, bankAccountPublicId);
    }

    @Override
    public BankAccountsDTO findById(Long id) {
        BankAccountsDTO bankAccountsDTO = new BankAccountsDTO();
        ModelMapper modelMapper = new ModelMapper();
        Optional<BankAccountsEntity> bankAccountsEntity = bankAccountsRepository.findById(id);
        bankAccountsEntity.ifPresent(e ->modelMapper.map(e,bankAccountsDTO));
        return bankAccountsDTO;
    }

    private boolean checkResourceToBankAccount(String publicResourceId, String bankAccountPublicId) {
        return bankAccountsRepository.findByPublicId(publicIDsService.findByPublicId(bankAccountPublicId).getId()).getResourceId().equals(idResolverService.getResourceID(publicResourceId));
    }

    private List<PersonsDTO> getPersonsDTOList(String managerPublicUserId, String resourcePublicId, String bankAccountPublicId) {
        List<PersonsDTO> personsDTOList = new ArrayList<>();
        List<BankAccountOwnersDTO> bankAccountOwnersDTOList = bankAccountOwnersService.readAllOwnerOfAccount(managerPublicUserId, resourcePublicId, bankAccountPublicId);
        for (BankAccountOwnersDTO dto : bankAccountOwnersDTOList) {
            PersonsDTO personsDTO = personsService.findById(dto.getUserId());
            personsDTOList.add(personsDTO);
        }
        return personsDTOList;
    }
}
