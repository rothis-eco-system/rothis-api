package ir.mohrazzr.dev.rothis.server.api.billing.service.service.impl;

import ir.mohrazzr.dev.rothis.server.api.billing.domain.entity.*;
import ir.mohrazzr.dev.rothis.server.api.billing.domain.repository.*;
import ir.mohrazzr.dev.rothis.server.api.billing.service.service.iservice.IResolverService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

/**
 * Created by IntelliJ IDEA at Tuesday in 2019/11/12 - 10:58 AM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : Rothis
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Email : mhrz.dev@gmail.com
 * Class Name          : IIdResolverServiceImpl
 **/
@Service
public class ResolverServiceImpl implements IResolverService {
    private PersonsRepository personsRepository;
    private UsersRepository usersRepository;
    private ResourcesRepository resourcesRepository;
    private BankAccountsRepository bankAccountsRepository;
    private PublicIDsRepository publicIDsRepository;
    private ResourceBillsRepository resourceBillsRepository;

    @Autowired
    public ResolverServiceImpl(PersonsRepository personsRepository, UsersRepository usersRepository, ResourcesRepository resourcesRepository, BankAccountsRepository bankAccountsRepository, PublicIDsRepository publicIDsRepository, ResourceBillsRepository resourceBillsRepository) {
        this.personsRepository = personsRepository;
        this.usersRepository = usersRepository;
        this.resourcesRepository = resourcesRepository;
        this.bankAccountsRepository = bankAccountsRepository;
        this.publicIDsRepository = publicIDsRepository;
        this.resourceBillsRepository = resourceBillsRepository;
    }

    @Override
    public Long getPersonID(String publicId) {
        PublicIDsEntity publicIDsEntity = publicIDsRepository.findByPublicId(publicId);
        UsersEntity usersEntity = usersRepository.findByPublicId(publicIDsEntity.getId());
        PersonsEntity personsEntity = personsRepository.findByUserId(usersEntity.getId());
        return personsEntity.getId();
    }

    @Override
    public Long getResourceID(String publicId) {
        PublicIDsEntity publicIDsEntity = publicIDsRepository.findByPublicId(publicId);
        ResourcesEntity resourcesEntity = resourcesRepository.findResourceByPublicId(publicIDsEntity.getId());
        return resourcesEntity.getId();
    }

    @Override
    public Long getBankAccountID(String publicId) {
        PublicIDsEntity publicIDsEntity = publicIDsRepository.findByPublicId(publicId);
        BankAccountsEntity bankAccountsEntity = bankAccountsRepository.findByPublicId(publicIDsEntity.getId());
        return bankAccountsEntity.getId();
    }

    @Override
    public Long getResourceBillID(String resourcePublicId, String publicId) {
        ResourceBillsEntity resourceBillsEntity = resourceBillsRepository.findByResourceIdAndPublicId(getResourceID(resourcePublicId)
                , publicIDsRepository.findByPublicId(publicId).getId());
        return resourceBillsEntity.getId();
    }

    @Override
    public BigDecimal getResourceBillPricePerSeconds(String resourcePublicId, String resourceBillPublicId) {
        ResourceBillsEntity resourceBillsEntity = resourceBillsRepository.findByResourceIdAndPublicId(getResourceID(resourcePublicId),
                publicIDsRepository.findByPublicId(resourceBillPublicId).getId());
        return resourceBillsEntity.getPricePerSecond();
    }

}
