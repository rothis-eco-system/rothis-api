package ir.mohrazzr.dev.rothis.server.api.billing.service.DTO;

import java.io.Serializable;

/**
 * Created by IntelliJ IDEA at Wednesday in 2019/11/06 - 11:50 PM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : Rothis
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Email : mhrz.dev@gmail.com
 * Class Name          : EmailsDTO
 **/

public class EmailsDTO implements Serializable {
    private static final long serialVersionUID = -3008853038727460301L;

    private Long id;
    private Long userId;
    private String email;
    private Boolean isActive;
    private String emailVerificationToken;
    private Boolean isPrimary;

    public EmailsDTO() {
    }

    public EmailsDTO(Long id, Long userId, String email, Boolean isActive, String emailVerificationToken, Boolean isPrimary) {
        this.id = id;
        this.userId = userId;
        this.email = email;
        this.isActive = isActive;
        this.emailVerificationToken = emailVerificationToken;
        this.isPrimary = isPrimary;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public String getEmailVerificationToken() {
        return emailVerificationToken;
    }

    public void setEmailVerificationToken(String emailVerificationToken) {
        this.emailVerificationToken = emailVerificationToken;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public Boolean getIsPrimary() {
        return isPrimary;
    }

    public void setIsPrimary(Boolean isPrimary) {
        this.isPrimary = isPrimary;
    }

    @Override
    public String toString() {
        return "EmailsDTO{" +
                "id=" + id +
                ", userId=" + userId +
                ", email='" + email + '\'' +
                ", isActive=" + isActive +
                ", emailVerificationToken='" + emailVerificationToken + '\'' +
                ", isPrimary=" + isPrimary +
                '}';
    }
}
