package ir.mohrazzr.dev.rothis.server.api.billing.service.DTO;

import java.io.Serializable;

/**
 * Created by IntelliJ IDEA at Wednesday in 2019/11/06 - 8:24 PM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : Rothis
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Email : mhrz.dev@gmail.com
 * Class Name          : OperationStatusDTO
 **/

public class OperationStatusDTO implements Serializable {
    private static final long serialVersionUID = 8955447096891400744L;

    private String operationName;
    private String operationStatus;
    private String operationCode;
    private String description;

    public OperationStatusDTO() {
    }

    public OperationStatusDTO(String operationName, String operationStatus, String operationCode, String description) {
        this.operationName = operationName;
        this.operationStatus = operationStatus;
        this.operationCode = operationCode;
        this.description = description;
    }

    public String getOperationCode() {
        return operationCode;
    }

    public void setOperationCode(String operationCode) {
        this.operationCode = operationCode;
    }

    public String getOperationName() {
        return operationName;
    }

    public void setOperationName(String operationName) {
        this.operationName = operationName;
    }

    public String getOperationStatus() {
        return operationStatus;
    }

    public void setOperationStatus(String operationStatus) {
        this.operationStatus = operationStatus;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "OperationStatusDTO{" +
                "operationName='" + operationName + '\'' +
                ", operationStatus='" + operationStatus + '\'' +
                ", operationCode='" + operationCode + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
