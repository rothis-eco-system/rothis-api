package ir.mohrazzr.dev.rothis.server.api.billing.service.service.iservice;

import ir.mohrazzr.dev.rothis.server.api.billing.service.DTO.PersonsDTO;
import ir.mohrazzr.dev.rothis.server.api.billing.service.DTO.OperationStatusDTO;

import java.util.List;

/**
 * Created by IntelliJ IDEA at Friday in 2019/11/08 - 10:56 PM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : Rothis
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Email : mhrz.dev@gmail.com
 * Class Name          : IPersonsService
 **/

public interface IPersonsService {
    PersonsDTO createPerson(PersonsDTO inDTO, String publicUserId);
    PersonsDTO createPerson(PersonsDTO inDTO, Integer userRole) throws Exception;
    PersonsDTO createPerson(PersonsDTO inDTO) throws Exception;
    PersonsDTO readPerson(String publicUserId);
    List<PersonsDTO> readAllPersons(int page, int limit);
    PersonsDTO updatePerson(PersonsDTO inDTO, String publicUserId);
    OperationStatusDTO deletePerson(String publicUserId);
    PersonsDTO findPersonByPublicUserId(String publicUserId);
    PersonsDTO findById(Long id);
}
