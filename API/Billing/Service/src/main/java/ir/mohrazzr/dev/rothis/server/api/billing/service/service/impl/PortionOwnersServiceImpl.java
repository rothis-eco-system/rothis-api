package ir.mohrazzr.dev.rothis.server.api.billing.service.service.impl;

import ir.mohrazzr.dev.rothis.server.api.billing.domain.entity.PortionOwnersEntity;
import ir.mohrazzr.dev.rothis.server.api.billing.domain.repository.PortionOwnersRepository;
import ir.mohrazzr.dev.rothis.server.api.billing.exception.handler.RothisExceptionService;
import ir.mohrazzr.dev.rothis.server.api.billing.exception.message.Errors;
import ir.mohrazzr.dev.rothis.server.api.billing.service.DTO.*;
import ir.mohrazzr.dev.rothis.server.api.billing.service.DTO.enums.OperationCode;
import ir.mohrazzr.dev.rothis.server.api.billing.service.DTO.enums.OperationName;
import ir.mohrazzr.dev.rothis.server.api.billing.service.DTO.enums.OperationStatus;
import ir.mohrazzr.dev.rothis.server.api.billing.service.service.iservice.*;
import ir.mohrazzr.dev.rothis.server.api.utils.calculator.ResourcesCalculation;
import ir.mohrazzr.dev.rothis.server.api.utils.generator.Generator;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by IntelliJ IDEA at Saturday in 2019/11/16 - 4:57 PM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : Rothis
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Email : mhrz.dev@gmail.com
 * Class Name          : PortionOwnersServiceImpl
 **/
@Service
public class PortionOwnersServiceImpl implements IPortionOwnersService {
    private PortionOwnersRepository portionOwnersRepository;
    private IResolverService idResolverService;
    private IResourceManagerService resourceManagerService;
    private IPublicIDsService publicIDsService;
    private IResourcesService resourcesService;
    private IPersonsService personsService;
    private Generator generator;

    @Autowired
    public PortionOwnersServiceImpl(PortionOwnersRepository portionOwnersRepository, IResolverService idResolverService, IResourceManagerService resourceManagerService, IPublicIDsService publicIDsService, IResourcesService resourcesService, IPersonsService personsService, Generator generator) {
        this.portionOwnersRepository = portionOwnersRepository;
        this.idResolverService = idResolverService;
        this.resourceManagerService = resourceManagerService;
        this.publicIDsService = publicIDsService;
        this.resourcesService = resourcesService;
        this.personsService = personsService;
        this.generator = generator;
    }

    @Override
    public PortionOwnersDTO createPortion(PortionOwnersDTO inDTO, String publicUserId, String publicResourceId, String ownerPublicUserId) {
        PortionOwnersEntity portionOwnersEntity = new PortionOwnersEntity();
        PortionOwnersDTO portionOwnersDTO = new PortionOwnersDTO();
        ModelMapper modelMapper = new ModelMapper();
        IdResolverDTO idResolverDTO = idResolver(publicUserId, publicResourceId, ownerPublicUserId);
        if (portionOwnersRepository.findByPersonIdAndResourceId(idResolverDTO.getOwnerId(), idResolverDTO.getResourceId()) == null) {
            if (resourceManagerService.findResourceByManagerId(idResolverDTO.getManagerId(), idResolverDTO.getResourceId()) != null) {
                ResourcesDTO resourcesDTO = resourcesService.readResource(publicUserId, publicResourceId);
                if (resourcesDTO.getTotalUnallocatedResourcePortion() > 0 && resourcesDTO.getTotalUnallocatedResourcePortion() >= inDTO.getPortionValue()) {
                    PublicIDsDTO publicIDsDTO = publicIDsService.generatePublicId(40);
                    modelMapper.map(inDTO, portionOwnersEntity);
                    portionOwnersEntity.setResourceId(idResolverDTO.getResourceId());
                    portionOwnersEntity.setPersonId(idResolverDTO.getOwnerId());
                    portionOwnersEntity.setPublicId(publicIDsDTO.getId());
                    portionOwnersEntity.setPortionIdentifierCode(generator.generateRandomNumber(10));
                    portionOwnersEntity.setPortionValue(inDTO.getPortionValue());
                    portionOwnersEntity.setPortionSeconds(ResourcesCalculation.getPortionSeconds(portionOwnersEntity.getPortionValue(), resourcesDTO.getResourceSecondsPerPortion()));
                    PortionOwnersEntity resultPortionOwnersEntity = portionOwnersRepository.save(portionOwnersEntity);
                    if (resultPortionOwnersEntity != null) {
                        modelMapper.map(resultPortionOwnersEntity, portionOwnersDTO);
                        resourcesDTO.setTotalUnallocatedResourcePortion(ResourcesCalculation.getTotalUnallocatedResourcePortion(resourcesDTO.getTotalResourcePortion(), portionOwnersDTO.getPortionValue()));
                        resourcesService.updateUnallocatedResourcePortionValue(idResolverDTO.getManagerId(), idResolverDTO.getResourceId(), resultPortionOwnersEntity.getPortionValue());
                        portionOwnersDTO.setPublicIdString(publicIDsDTO.getPublicId());
                        portionOwnersDTO.setOwner(personsService.readPerson(ownerPublicUserId));
                        return portionOwnersDTO;
                    }
                } else
                    throw new RothisExceptionService(Errors.COULD_NOT_INSERT_RECORD.getMessage() + "," + Errors.TOTAL_UNALLOCATED_RESOURCE_PORTION_IS_ZERO.getMessage());
            }
        } else
            throw new RothisExceptionService(Errors.COULD_NOT_INSERT_RECORD.getMessage() + "," + Errors.PORTION_OWNER_IS_DUPLICATED_IN_THIS_RESOURCE.getMessage());
        return null;
    }

    @Override
    public PortionOwnersDTO readPortion(String ownerPublicUserId, String publicResourceId, String portionPublicId) {
        PortionOwnersEntity portionOwnersEntity = new PortionOwnersEntity();
        PortionOwnersDTO portionOwnersDTO = new PortionOwnersDTO();
        ModelMapper modelMapper = new ModelMapper();
        PublicIDsDTO publicIDsDTO = publicIDsService.findByPublicId(portionPublicId);
        if (publicIDsDTO != null) {
            portionOwnersEntity = portionOwnersRepository.findByPersonIdAndResourceIdAndPublicId(
                    idResolverService.getPersonID(ownerPublicUserId),
                    idResolverService.getResourceID(publicResourceId),
                    publicIDsDTO.getId());
            if (portionOwnersEntity != null) {
                modelMapper.map(portionOwnersEntity, portionOwnersDTO);
                portionOwnersDTO.setOwner(personsService.readPerson(ownerPublicUserId));
                portionOwnersDTO.setPublicIdString(portionPublicId);
                return portionOwnersDTO;
            } else throw new RothisExceptionService(Errors.NOT_FOUND_PORTION_OWNER_IN_THIS_RESOURCE.getMessage());
        }
        throw new RothisExceptionService(Errors.NOT_FOUND_PORTION_WITH_THIS_PUBLIC_ID.getMessage());
    }

    @Override
    public List<PortionOwnersDTO> readAllPortion(String publicResourceId, String managerPublicUserId, int page, int limit) {
        List<PortionOwnersDTO> portionOwnersDTOList = new ArrayList<>();
        IdResolverDTO idResolverDTO = idResolver(managerPublicUserId, publicResourceId);
        ResourceManagersDTO resourceManagersDTO = resourceManagerService.findResourceByManagerId(idResolverDTO.getManagerId(), idResolverDTO.getResourceId());
        if (resourceManagersDTO != null) {
            page -= 1;
            Pageable pageable = PageRequest.of(page, limit);
            Page<PortionOwnersEntity> portionOwnersEntityPage = portionOwnersRepository.findAllByResourceId(resourceManagersDTO.getResourceId(), pageable);
            List<PortionOwnersEntity> portionOwnersEntityList = portionOwnersEntityPage.getContent();
            for (PortionOwnersEntity entity : portionOwnersEntityList) {
                PortionOwnersDTO portionOwnersDTO = new PortionOwnersDTO();
                PublicIDsDTO publicIDsDTO = publicIDsService.findById(entity.getPublicId());
                PersonsDTO personsDTO = personsService.findById(entity.getPersonId());
                ModelMapper modelMapper = new ModelMapper();
                modelMapper.map(entity, portionOwnersDTO);
                portionOwnersDTO.setPublicIdString(publicIDsDTO.getPublicId());
                portionOwnersDTO.setOwner(personsDTO);
                System.out.println(portionOwnersDTO.getPortionValue());
                portionOwnersDTOList.add(portionOwnersDTO);
            }
            return portionOwnersDTOList;
        } else
            throw new RothisExceptionService(Errors.NOT_FOUND_RESOURCE_WHIT_THIS_ID_OR_MANAGER.getMessage());
    }


    @Override
    public PortionOwnersDTO updatePortion(PortionOwnersDTO inDTO, String publicUserId, String publicResourceId, String ownerPublicUserId, String portionPublicId) {
        PortionOwnersDTO portionOwnersDTO = new PortionOwnersDTO();
        ModelMapper modelMapper = new ModelMapper();
        PortionOwnersEntity portionOwnersEntity = new PortionOwnersEntity();
        IdResolverDTO idResolverDTO = idResolver(publicUserId, publicResourceId, ownerPublicUserId);
        PublicIDsDTO portionPublicIDsDTO = publicIDsService.findByPublicId(portionPublicId);
        if (resourceManagerService.findResourceByManagerId(idResolverDTO.getManagerId(), idResolverDTO.getResourceId()) != null) {
            PortionOwnersEntity foundedPortionOwnersEntity = portionOwnersRepository.findByPersonIdAndResourceIdAndPublicId(idResolverDTO.getOwnerId(), idResolverDTO.getResourceId(), portionPublicIDsDTO.getId());
            if (foundedPortionOwnersEntity != null) {
                ResourcesDTO resourcesDTO = resourcesService.readResource(publicUserId, publicResourceId);
                modelMapper.map(foundedPortionOwnersEntity, portionOwnersEntity);
                portionOwnersEntity.setPortionValue(inDTO.getPortionValue());
                portionOwnersEntity.setPortionSeconds(ResourcesCalculation.getPortionSeconds(portionOwnersEntity.getPortionValue(), resourcesDTO.getResourceSecondsPerPortion()));
                PortionOwnersEntity resultPortionOwnersEntity = portionOwnersRepository.save(portionOwnersEntity);
                if (resultPortionOwnersEntity != null) {
                    PublicIDsDTO publicIDsDTO = publicIDsService.updatePublicId(portionPublicIDsDTO.getId());
                    modelMapper.map(resultPortionOwnersEntity, portionOwnersDTO);
                    PersonsDTO personsDTO = personsService.readPerson(ownerPublicUserId);
                    resourcesService.updateUnallocatedResourcePortionValue(idResolverDTO.getManagerId(), idResolverDTO.getResourceId(), foundedPortionOwnersEntity.getPortionValue(), inDTO.getPortionValue());
                    portionOwnersDTO.setPublicIdString(publicIDsDTO.getPublicId());
                    portionOwnersDTO.setOwner(personsDTO);
                    return portionOwnersDTO;
                } else
                    throw new RothisExceptionService(Errors.COULD_NOT_INSERT_RECORD.getMessage() + "," + Errors.TOTAL_UNALLOCATED_RESOURCE_PORTION_IS_ZERO.getMessage());
            }
        } else
            throw new RothisExceptionService(Errors.USER_NOT_VERIFIED.getMessage() + "," + Errors.YOUR_NOT_ADMIN_OF_THIS_RESOURCE.getMessage());
        return null;
    }

    @Override
    public PortionOwnersDTO findById(Long id) {
        Optional<PortionOwnersEntity> portionOwnersEntity = portionOwnersRepository.findById(id);
        PortionOwnersDTO ownersDTO = new PortionOwnersDTO();
        portionOwnersEntity.ifPresent(e -> {
            ModelMapper modelMapper = new ModelMapper();
            modelMapper.map(e, ownersDTO);
        });
        return ownersDTO;
    }

    @Override
    public OperationStatusDTO deletePortion(String managerPublicUserId, String publicResourceId, String ownerPublicUserId, String portionPublicId) {
        ResourcesDTO resourcesDTO = resourcesService.readResource(managerPublicUserId, publicResourceId);
        if (resourcesDTO != null) {
            IdResolverDTO idResolverDTO = idResolver(managerPublicUserId, publicResourceId, ownerPublicUserId);
            PublicIDsDTO portionPublicIDsDTO = publicIDsService.findByPublicId(portionPublicId);
            PortionOwnersEntity foundedPortionOwnersEntity = portionOwnersRepository.findByPersonIdAndResourceIdAndPublicId(idResolverDTO.getOwnerId(), idResolverDTO.getResourceId(), portionPublicIDsDTO.getId());
            if (foundedPortionOwnersEntity != null) {
                resourcesService.updateUnallocatedResourcePortionValue(idResolverDTO.getManagerId(), idResolverDTO.getResourceId(), foundedPortionOwnersEntity.getPortionValue(), 0F);
                portionOwnersRepository.delete(foundedPortionOwnersEntity);
                return new OperationStatusDTO(OperationName.DELETE.name(),
                        OperationStatus.SUCCESS.name(), OperationCode.CODE200.name(),
                        Errors.DELETE_RECORD_SUCCESSFULLY.getMessage());
            }
        }
        return new OperationStatusDTO(OperationName.DELETE.name(),
                OperationStatus.ERROR.name(), OperationCode.CODE400.name(),
                Errors.COULD_NOT_DELETE_RECORD.getMessage());
    }

    @Override
    public int countByResourceId(Long id) {
        return portionOwnersRepository.countByResourceId(id);
    }

    private IdResolverDTO idResolver(String managerPublicUserId, String publicResourceId) {
        IdResolverDTO idResolverDTO = new IdResolverDTO();
        idResolverDTO.setManagerId(idResolverService.getPersonID(managerPublicUserId));
        idResolverDTO.setResourceId(idResolverService.getResourceID(publicResourceId));
        return idResolverDTO;
    }

    private IdResolverDTO idResolver(String managerPublicUserId, String publicResourceId, String ownerPublicUserId) {
        IdResolverDTO idResolverDTO = idResolver(managerPublicUserId, publicResourceId);
        idResolverDTO.setOwnerId(idResolverService.getPersonID(ownerPublicUserId));
        return idResolverDTO;
    }
}

