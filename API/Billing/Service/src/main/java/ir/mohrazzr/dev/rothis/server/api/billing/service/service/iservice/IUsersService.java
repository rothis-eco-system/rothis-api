package ir.mohrazzr.dev.rothis.server.api.billing.service.service.iservice;

import ir.mohrazzr.dev.rothis.server.api.billing.service.DTO.OperationStatusDTO;
import ir.mohrazzr.dev.rothis.server.api.billing.service.DTO.UsersDTO;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;

/**
 * Created by IntelliJ IDEA at Wednesday in 2019/10/30 - 7:08 PM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : Rothis
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Email : mhrz.dev@gmail.com
 * Interface Name      : IUsers
 **/
public interface IUsersService extends UserDetailsService {
    UsersDTO createUser(UsersDTO user, Boolean autoGenerate) throws Exception;
    UsersDTO createUser(UsersDTO user, Integer userRole, Boolean autoGenerate) throws Exception;
    UsersDTO createUser(UsersDTO user) throws Exception;
    UsersDTO readUser(String id);
    UsersDTO updateUser(String id, UsersDTO dto) throws Exception;
    OperationStatusDTO deleteUser(String id) throws Exception;
    UsersDTO findUserByEmail(String email);
    UsersDTO findUserByUserName(String userName);
    UsersDTO findUserByPublicUserId(String publicUserId);
    List<UsersDTO> readAllUsers(int page, int limit);
    UsersDTO findById(Long userId);
}
