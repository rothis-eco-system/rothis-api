package ir.mohrazzr.dev.rothis.server.api.billing.service.DTO;

import java.io.Serializable;

/**
 * Created by IntelliJ IDEA at Thursday in 2019/11/14 - 12:28 PM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : Rothis
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Email : mhrz.dev@gmail.com
 * Class Name          : ResourcesDTO
 **/

public class ResourcesDTO implements Serializable {
    private static final long serialVersionUID = 7302239132441629422L;
    private Long id;
    private Long publicId;
    private String publicIdString;
    private String name;
    private String identifierCode;
    private Long resourcesAddressId;
    private Float totalResourcePortion;
    private Float totalUnallocatedResourcePortion;
    private Integer dongValue;
    private Long resourceSecondsPerPortion;
    private Long totalResourceSeconds;
    private Integer circuitOfTheDay;

    public ResourcesDTO() {
    }

    public ResourcesDTO(Long id, Long publicId, String publicIdString, String name, String identifierCode, Long resourcesAddressId, Float totalResourcePortion, Float totalUnallocatedResourcePortion, Integer dongValue, Long resourceSecondsPerPortion, Long totalResourceSeconds, Integer circuitOfTheDay) {
        this.id = id;
        this.publicId = publicId;
        this.publicIdString = publicIdString;
        this.name = name;
        this.identifierCode = identifierCode;
        this.resourcesAddressId = resourcesAddressId;
        this.totalResourcePortion = totalResourcePortion;
        this.totalUnallocatedResourcePortion = totalUnallocatedResourcePortion;
        this.dongValue = dongValue;
        this.resourceSecondsPerPortion = resourceSecondsPerPortion;
        this.totalResourceSeconds = totalResourceSeconds;
        this.circuitOfTheDay = circuitOfTheDay;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPublicId() {
        return publicId;
    }

    public void setPublicId(Long publicId) {
        this.publicId = publicId;
    }

    public String getPublicIdString() {
        return publicIdString;
    }

    public void setPublicIdString(String publicIdString) {
        this.publicIdString = publicIdString;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIdentifierCode() {
        return identifierCode;
    }

    public void setIdentifierCode(String identifierCode) {
        this.identifierCode = identifierCode;
    }


    public Long getResourcesAddressId() {
        return resourcesAddressId;
    }

    public void setResourcesAddressId(Long resourcesAddressId) {
        this.resourcesAddressId = resourcesAddressId;
    }

    public Float getTotalResourcePortion() {
        return totalResourcePortion;
    }

    public void setTotalResourcePortion(Float totalResourcePortion) {
        this.totalResourcePortion = totalResourcePortion;
    }

    public Float getTotalUnallocatedResourcePortion() {
        return totalUnallocatedResourcePortion;
    }

    public void setTotalUnallocatedResourcePortion(Float totalUnallocatedResourcePortion) {
        this.totalUnallocatedResourcePortion = totalUnallocatedResourcePortion;
    }

    public Integer getDongValue() {
        return dongValue;
    }

    public void setDongValue(Integer dongValue) {
        this.dongValue = dongValue;
    }

    public Long getResourceSecondsPerPortion() {
        return resourceSecondsPerPortion;
    }

    public void setResourceSecondsPerPortion(Long resourceSecondsPerPortion) {
        this.resourceSecondsPerPortion = resourceSecondsPerPortion;
    }

    public Long getTotalResourceSeconds() {
        return totalResourceSeconds;
    }

    public void setTotalResourceSeconds(Long totalResourceSeconds) {
        this.totalResourceSeconds = totalResourceSeconds;
    }

    public Integer getCircuitOfTheDay() {
        return circuitOfTheDay;
    }

    public void setCircuitOfTheDay(Integer circuitOfTheDay) {
        this.circuitOfTheDay = circuitOfTheDay;
    }

    @Override
    public String toString() {
        return "ResourcesDTO{" +
                "id=" + id +
                ", publicId=" + publicId +
                ", publicIdString='" + publicIdString + '\'' +
                ", name='" + name + '\'' +
                ", resourcesAddressId=" + resourcesAddressId +
                ", totalResourcePortion=" + totalResourcePortion +
                ", totalUnallocatedResourcePortion=" + totalUnallocatedResourcePortion +
                ", dongValue=" + dongValue +
                ", totalResourceSeconds=" + totalResourceSeconds +
                ", circuitOfTheDay=" + circuitOfTheDay +
                '}';
    }
}
