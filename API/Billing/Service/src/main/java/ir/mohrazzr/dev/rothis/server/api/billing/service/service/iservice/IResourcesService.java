package ir.mohrazzr.dev.rothis.server.api.billing.service.service.iservice;

import ir.mohrazzr.dev.rothis.server.api.billing.service.DTO.OperationStatusDTO;
import ir.mohrazzr.dev.rothis.server.api.billing.service.DTO.ResourcesDTO;

import java.util.List;

/**
 * Created by IntelliJ IDEA at Thursday in 2019/11/14 - 1:46 PM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : Rothis
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Email : mhrz.dev@gmail.com
 * Class Name          : IResourcesService
 **/

public interface IResourcesService {
    ResourcesDTO createResource(ResourcesDTO inDTO, String publicUserId);
    ResourcesDTO readResource(String publicUserId, String publicResourceId);
    List<ResourcesDTO> readAllResourcesByManagerPublicId(String publicUserId);
    ResourcesDTO updateResource(ResourcesDTO inDTO, String publicUserId, String publicResourceId);
    OperationStatusDTO deleteResource(String publicUserId, String publicResourceId);

    ResourcesDTO updateUnallocatedResourcePortionValue(Long managerId, Long resourceId, Float newAllocatedValue);

    ResourcesDTO updateUnallocatedResourcePortionValue(Long managerId, Long resourceId, Float lastAllocatedValue, Float newAllocatedValue);
}
