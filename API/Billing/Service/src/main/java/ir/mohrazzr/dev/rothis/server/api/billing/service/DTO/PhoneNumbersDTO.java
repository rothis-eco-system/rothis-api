package ir.mohrazzr.dev.rothis.server.api.billing.service.DTO;

import java.io.Serializable;

/**
 * Created by IntelliJ IDEA at Friday in 2019/11/08 - 7:20 PM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : Rothis
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Email : mhrz.dev@gmail.com
 * Class Name          : PhoneNumbersDTO
 **/

public class PhoneNumbersDTO implements Serializable {
    private static final long serialVersionUID = 7897402854451882359L;
    private Integer id;
    private Long personsId;
    private Long publicId;
    private String publicPhoneNumberId;
    private Boolean isPrimary;
    private String phoneNumber;
    private Integer typeCode;

    public PhoneNumbersDTO() {
    }

    public PhoneNumbersDTO(Integer id, Long personsId, Long publicId, String publicPhoneNumberId, Boolean isPrimary, String phoneNumber, Integer typeCode) {
        this.id = id;
        this.personsId = personsId;
        this.publicId = publicId;
        this.publicPhoneNumberId = publicPhoneNumberId;
        this.isPrimary = isPrimary;
        this.phoneNumber = phoneNumber;
        this.typeCode = typeCode;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Long getPersonsId() {
        return personsId;
    }

    public void setPersonsId(Long personsId) {
        this.personsId = personsId;
    }

    public Long getPublicId() {
        return publicId;
    }

    public void setPublicId(Long publicId) {
        this.publicId = publicId;
    }

    public String getPublicPhoneNumberId() {
        return publicPhoneNumberId;
    }

    public void setPublicPhoneNumberId(String publicPhoneNumberId) {
        this.publicPhoneNumberId = publicPhoneNumberId;
    }

    public Boolean getPrimary() {
        return isPrimary;
    }

    public void setPrimary(Boolean primary) {
        isPrimary = primary;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Integer getTypeCode() {
        return typeCode;
    }

    public void setTypeCode(Integer typeCode) {
        this.typeCode = typeCode;
    }

}
