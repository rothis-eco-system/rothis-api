package ir.mohrazzr.dev.rothis.server.api.billing.service.service.iservice;

import java.math.BigDecimal;

/**
 * Created by IntelliJ IDEA at Tuesday in 2019/11/12 - 10:57 AM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : Rothis
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Email : mhrz.dev@gmail.com
 * Class Name          : IResolverService
 **/

public interface IResolverService {
    Long getPersonID(String publicId);
    Long getResourceID(String publicId);
    Long getBankAccountID(String id);
    Long getResourceBillID(String resourcePublicId, String publicId);
    BigDecimal getResourceBillPricePerSeconds(String resourcePublicId, String resourceBillPublicId);
}
