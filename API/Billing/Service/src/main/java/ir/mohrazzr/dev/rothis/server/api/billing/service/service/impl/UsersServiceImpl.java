package ir.mohrazzr.dev.rothis.server.api.billing.service.service.impl;

import ir.mohrazzr.dev.rothis.server.api.billing.domain.entity.EmailsEntity;
import ir.mohrazzr.dev.rothis.server.api.billing.domain.entity.RolesEntity;
import ir.mohrazzr.dev.rothis.server.api.billing.domain.entity.UsersEntity;
import ir.mohrazzr.dev.rothis.server.api.billing.domain.repository.EmailsRepository;
import ir.mohrazzr.dev.rothis.server.api.billing.domain.repository.RolesRepository;
import ir.mohrazzr.dev.rothis.server.api.billing.domain.repository.UsersRepository;
import ir.mohrazzr.dev.rothis.server.api.billing.exception.handler.RothisExceptionService;
import ir.mohrazzr.dev.rothis.server.api.billing.exception.message.Errors;
import ir.mohrazzr.dev.rothis.server.api.billing.service.DTO.OperationStatusDTO;
import ir.mohrazzr.dev.rothis.server.api.billing.service.DTO.PublicIDsDTO;
import ir.mohrazzr.dev.rothis.server.api.billing.service.DTO.UsersDTO;
import ir.mohrazzr.dev.rothis.server.api.billing.service.DTO.enums.OperationCode;
import ir.mohrazzr.dev.rothis.server.api.billing.service.DTO.enums.OperationName;
import ir.mohrazzr.dev.rothis.server.api.billing.service.DTO.enums.OperationStatus;
import ir.mohrazzr.dev.rothis.server.api.billing.service.service.iservice.IPublicIDsService;
import ir.mohrazzr.dev.rothis.server.api.billing.service.service.iservice.IUsersService;
import ir.mohrazzr.dev.rothis.server.api.utils.encoder.PasswordEncoder;
import ir.mohrazzr.dev.rothis.server.api.utils.generator.Generator;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by IntelliJ IDEA at Wednesday in 2019/10/30 - 7:14 PM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : Rothis
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Email : mhrz.dev@gmail.com
 * Class Name          : UsersServiceImpl
 **/
@Service
public class UsersServiceImpl implements IUsersService {

    private UsersRepository usersRepository;
    private IPublicIDsService publicIDsService;
    private EmailsRepository emailsRepository;
    private RolesRepository rolesRepository;
    private Generator generator;
    private PasswordEncoder passwordEncoder;

    @Autowired
    public UsersServiceImpl(UsersRepository usersRepository, PasswordEncoder passwordEncoder, IPublicIDsService iPublicIDsService, Generator generator, EmailsRepository emailsRepository, RolesRepository rolesRepository) {
        this.rolesRepository = rolesRepository;
        this.usersRepository = usersRepository;
        this.passwordEncoder = passwordEncoder;
        this.publicIDsService = iPublicIDsService;
        this.generator = generator;
        this.emailsRepository = emailsRepository;
    }

    @Override
    public UsersDTO createUser(UsersDTO user, Boolean autoGenerate) throws RothisExceptionService {
        if (autoGenerate) {
            user.setUserName("@" + generator.generateUserName(2, 6));
            user.setUserRole(5);
            while (usersRepository.findByUserName(user.getUserName()) != null) {
                user.setUserName("@" + generator.generateUserName(2, 6));
            }
        }
        return getUsersDTO(user);
    }

    @Override
    public UsersDTO createUser(UsersDTO user, Integer userRole, Boolean autoGenerate) throws Exception {
        if (autoGenerate) {
            user.setUserName("@" + generator.generateUserName(2, 6));
            if (userRole > 2)
                user.setUserRole(userRole);
            while (usersRepository.findByUserName(user.getUserName()) != null) {
                user.setUserName("@" + generator.generateUserName(2, 6));
            }
        }
        return getUsersDTO(user);
    }

    @Override
    public UsersDTO createUser(UsersDTO user) throws RothisExceptionService {
        return getUsersDTO(user);
    }

    @SuppressWarnings(value = "Duplicates")
    private UsersDTO getUsersDTO(UsersDTO user) throws RothisExceptionService {
        UsersDTO dto = new UsersDTO();
        UsersEntity entity = new UsersEntity();

        if (!user.getUserName().contains("@")) {
            user.setUserName("@" + user.getUserName());
        }

        if (usersRepository.findByUserName(user.getUserName()) == null) {
            if (user.getUserRole() == null) {
                BeanUtils.copyProperties(user, entity);
                entity.setEncryptedPassword(passwordEncoder.bCryptPasswordEncoder().encode(user.getPassword()));
                entity.setUserRole(2);
                PublicIDsDTO publicIDsDTO = publicIDsService.generatePublicId(30);
                entity.setPublicId(publicIDsDTO.getId());
                UsersEntity storedUserDetails = usersRepository.save(entity);
                BeanUtils.copyProperties(storedUserDetails, dto);
                dto.setPublicUserIdString(publicIDsDTO.getPublicId());
                return dto;
            } else if (user.getUserRole() == 404) {
                throw new RothisExceptionService(Errors.NOT_ALLOW_TO_TAKE_USER_ROLE.getMessage());
            } else {
                if (rolesRepository.findById(user.getUserRole()).isPresent()) {
                    BeanUtils.copyProperties(user, entity);
                    entity.setEncryptedPassword(passwordEncoder.bCryptPasswordEncoder().encode(user.getPassword()));
                    Optional<RolesEntity> rolesEntity = rolesRepository.findById(user.getUserRole());
                    PublicIDsDTO publicIDsDTO = publicIDsService.generatePublicId(30);
                    entity.setPublicId(publicIDsDTO.getId());
                    rolesEntity.ifPresent(en -> entity.setUserRole(en.getId()));
                    UsersEntity storedUserDetails = usersRepository.save(entity);
                    BeanUtils.copyProperties(storedUserDetails, dto);
                    dto.setPublicUserIdString(publicIDsDTO.getPublicId());
                    return dto;

                } else {
                    throw new RothisExceptionService(Errors.NO_USER_ROLE_FOUND.getMessage());
                }
            }
        } else {
            throw new RothisExceptionService(Errors.USER_NAME_EXISTS.getMessage());
        }
    }

    @Override
    public UsersDTO readUser(String userName) {
        UsersDTO usersDTO = findUserByUserName(userName);


        if (usersDTO.getPublicId() != null) {
            PublicIDsDTO publicIDsDTO = publicIDsService.findById(usersDTO.getPublicId());
            usersDTO.setPublicUserIdString(publicIDsDTO.getPublicId());
            EmailsEntity emailsEntity = emailsRepository.findByUserId(usersDTO.getId());
            if (emailsEntity != null)
                usersDTO.setEmail(emailsEntity.getEmail());
        }
        return usersDTO;
    }

    @Override
    public UsersDTO findUserByEmail(String email) {
        UsersDTO usersDTO = new UsersDTO();
        EmailsEntity emailsEntity = emailsRepository.findByEmail(email);
        if (email != null) {
            Optional<UsersEntity> foundedUsersEntity = usersRepository.findById(emailsEntity.getUserId());
            foundedUsersEntity.ifPresent(entity -> {
                BeanUtils.copyProperties(entity, usersDTO);
                PublicIDsDTO publicIDsDTO = publicIDsService.findById(usersDTO.getPublicId());
                usersDTO.setPublicUserIdString(publicIDsDTO.getPublicId());
            });
            return usersDTO;
        } else {
            throw new RothisExceptionService(Errors.NO_USER_FOUND_WITH_EMAIL.getMessage() + email);
        }
    }

    @Override
    public UsersDTO findUserByUserName(String userName) {
        UsersDTO usersDTO = new UsersDTO();
        UsersEntity usersEntity;
        if (!userName.contains("@"))
            userName = "@" + userName;
        usersEntity = usersRepository.findByUserName(userName);
        if (usersEntity != null) {
            BeanUtils.copyProperties(usersEntity, usersDTO);
            PublicIDsDTO publicIDsDTO = publicIDsService.findById(usersDTO.getPublicId());
            usersDTO.setPublicUserIdString(publicIDsDTO.getPublicId());
            return usersDTO;
        } else {
            throw new RothisExceptionService(Errors.NO_USER_FOUND_WITH_USERNAME.getMessage() + userName);
        }
    }

    @Override
    public UsersDTO findUserByPublicUserId(String publicUserId) {
        UsersDTO usersDTO = new UsersDTO();
        PublicIDsDTO publicIDsDTO = publicIDsService.findByPublicId(publicUserId);
        UsersEntity entity = usersRepository.findByPublicId(publicIDsDTO.getId());
        if (entity != null) {
            BeanUtils.copyProperties(entity, usersDTO);
            usersDTO.setPublicUserIdString(publicIDsDTO.getPublicId());
        }
        return usersDTO;

    }

    @Override
    @SuppressWarnings("Duplicates")
    public UsersDTO updateUser(String publicUserId, UsersDTO user) throws RothisExceptionService {
        UsersDTO dto = new UsersDTO();
        PublicIDsDTO publicIDsDTO = publicIDsService.findByPublicId(publicUserId);
        if (publicIDsDTO != null) {
            UsersEntity entity = usersRepository.findByPublicId(publicIDsDTO.getId());
            if (entity != null) {

                if (usersRepository.findByUserName(user.getUserName()) == null) {
                    if (user.getUserName() != null) {
                        if (!user.getUserName().contains("@")) {
                            user.setUserName("@" + user.getUserName());
                        }
                        entity.setUserName(user.getUserName());
                    }
                    entity.setUserRole(user.getUserRole());
                    entity.setNickName(user.getNickName());

                    if (rolesRepository.findById(user.getUserRole()).isPresent()) {
                        entity.setEncryptedPassword(passwordEncoder.bCryptPasswordEncoder().encode(user.getPassword()));
                        Optional<RolesEntity> rolesEntity = rolesRepository.findById(user.getUserRole());
                        PublicIDsDTO updatedPublicIDsDTO = publicIDsService.updatePublicId(publicUserId);
                        entity.setPublicId(updatedPublicIDsDTO.getId());
                        rolesEntity.ifPresent(en -> entity.setUserRole(en.getId()));
                        UsersEntity resultEntity = usersRepository.save(entity);
                        BeanUtils.copyProperties(resultEntity, dto);
                        dto.setPublicUserIdString(updatedPublicIDsDTO.getPublicId());
                        return dto;
                    } else {
                        throw new RothisExceptionService(Errors.NO_USER_ROLE_FOUND.getMessage());
                    }

                } else {
                    throw new RothisExceptionService(Errors.USER_NAME_EXISTS.getMessage() + user.getUserName());
                }
            }
            return dto;
        } else {
            throw new RothisExceptionService(Errors.NO_USER_FOUND_WITH_USERNAME.getMessage() + publicUserId);
        }
    }

    @Override
    public OperationStatusDTO deleteUser(String publicUserId) throws RothisExceptionService {
        PublicIDsDTO publicIDsDTO = publicIDsService.findByPublicId(publicUserId);
        UsersEntity entity = usersRepository.findByPublicId(publicIDsDTO.getId());
        if (entity != null) {
            usersRepository.delete(entity);
            publicIDsService.deletePublicId(publicIDsDTO.getId());
            return new OperationStatusDTO(OperationName.DELETE.name(),
                    OperationStatus.SUCCESS.name(), OperationCode.CODE200.name(),
                    Errors.DELETE_RECORD_SUCCESSFULLY.getMessage());
        }

        return new OperationStatusDTO(OperationName.DELETE.name(),
                OperationStatus.ERROR.name(), OperationCode.CODE400.name(),
                Errors.COULD_NOT_DELETE_RECORD.getMessage());
    }

    @Override
    @SuppressWarnings(value = "Duplicates")
    public List<UsersDTO> readAllUsers(int page, int limit) {
        List<UsersDTO> dtoList = new ArrayList<>();
        Pageable pageable = PageRequest.of(page, limit);
        Page<UsersEntity> usersEntityPage = usersRepository.findAll(pageable);
        List<UsersEntity> usersEntityList = usersEntityPage.getContent();
        EmailsEntity emailsEntity = new EmailsEntity();
        for (UsersEntity usersEntity : usersEntityList) {
            PublicIDsDTO publicIDsDTO = publicIDsService.findById(usersEntity.getPublicId());
            emailsEntity = emailsRepository.findByUserId(usersEntity.getId());
            UsersDTO dto = new UsersDTO();
            BeanUtils.copyProperties(usersEntity, dto);
            dto.setPublicUserIdString(publicIDsDTO.getPublicId());
            if (emailsEntity != null)
                if (emailsEntity.getIsActive())
                    if (emailsEntity.getIsPrimary())
                        dto.setEmail(emailsEntity.getEmail());
            dtoList.add(dto);

        }
        return dtoList;
    }

    @Override
    public UsersDTO findById(Long userId) {
        UsersDTO usersDTO = new UsersDTO();
        Optional<UsersEntity> usersEntity = usersRepository.findById(userId);
        usersEntity.ifPresent(entity -> {
            BeanUtils.copyProperties(entity, usersDTO);
            usersDTO.setPublicUserIdString(publicIDsService.findById(entity.getPublicId()).getPublicId());
        });
        return usersDTO;
    }

    @Override
    public UserDetails loadUserByUsername(String key) throws RothisExceptionService {
        UsersEntity foundedUser = usersRepository.findByUserName(key);
        if (foundedUser != null) {
            return new User(foundedUser.getUserName(), foundedUser.getEncryptedPassword(), new ArrayList<>());
        } else
            throw new RothisExceptionService(Errors.NO_USER_FOUND_WITH_USERNAME.getMessage() + key);
    }
}
