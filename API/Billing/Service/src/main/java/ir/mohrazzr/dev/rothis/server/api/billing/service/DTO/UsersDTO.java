package ir.mohrazzr.dev.rothis.server.api.billing.service.DTO;

import java.io.Serializable;

/**
 * Created by IntelliJ IDEA at Wednesday in 2019/10/30 - 7:00 PM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : Rothis
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @userName : mhrz.dev@gmail.com
 * Class Name          : UsersDTO
 **/

public class UsersDTO implements Serializable {
    private static final long serialVersionUID = 8955447096891400744L;
    private Long id;
    private Long publicId;
    private String publicUserIdString;
    private String nickName;
    private String userName;
    private String password;
    private String encryptedPassword;
    private Integer userRole;
    private String email;


    public UsersDTO() {
    }

    public UsersDTO(String nickName, String password) {
        this.nickName = nickName;
        this.password = password;
    }

    public UsersDTO(Long id, Long publicId, String publicUserIdString, String nickName, String userName, String password, String encryptedPassword, Integer userRole, String email) {
        this.id = id;
        this.publicId = publicId;
        this.publicUserIdString = publicUserIdString;
        this.nickName = nickName;
        this.userName = userName;
        this.password = password;
        this.encryptedPassword = encryptedPassword;
        this.userRole = userRole;
        this.email = email;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPublicId() {
        return publicId;
    }

    public void setPublicId(Long publicId) {
        this.publicId = publicId;
    }

    public String getPublicUserIdString() {
        return publicUserIdString;
    }

    public void setPublicUserIdString(String publicUserIdString) {
        this.publicUserIdString = publicUserIdString;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEncryptedPassword() {
        return encryptedPassword;
    }

    public void setEncryptedPassword(String encryptedPassword) {
        this.encryptedPassword = encryptedPassword;
    }

    public Integer getUserRole() {
        return userRole;
    }

    public void setUserRole(Integer userRole) {
        this.userRole = userRole;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
