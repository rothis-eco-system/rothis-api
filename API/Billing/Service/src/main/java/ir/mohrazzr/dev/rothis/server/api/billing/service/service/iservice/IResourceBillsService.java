package ir.mohrazzr.dev.rothis.server.api.billing.service.service.iservice;

import ir.mohrazzr.dev.rothis.server.api.billing.service.DTO.ResourceBillsDTO;
import ir.mohrazzr.dev.rothis.server.api.billing.service.DTO.OperationStatusDTO;

import java.util.List;

/**
 * Created by IntelliJ IDEA at Monday in 2019/11/25 - 7:26 PM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : Rothis
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Email : mhrz.dev@gmail.com
 * Class Name          : IResourceBillsService
 **/

public interface IResourceBillsService {
    ResourceBillsDTO createResourceBill(ResourceBillsDTO inDTO, String resourcePublicId, String managerPublicUserId, String bankAccountPublicId);
    ResourceBillsDTO readResourceBill(String resourcePublicId, String managerPublicUserId, String resourceBillPublicId, String resourceBillIdentityCode);
    ResourceBillsDTO readResourceBillByPublicId(String resourcePublicId, String managerPublicUserId, String resourceBillPublicId);
    ResourceBillsDTO readResourceBillByIdentityCode(String resourcePublicId, String managerPublicUserId, String resourceBillByIdentityCode);
    ResourceBillsDTO updateResourceBill(ResourceBillsDTO inDTO, String resourcePublicId, String managerPublicUserId, String resourceBillPublicId, String bankAccountPublicId);
    OperationStatusDTO deleteResourceBill(String resourcePublicId, String managerPublicUserId, String resourceBillPublicId);
    List<ResourceBillsDTO> readAllResourceBills(String resourcePublicId, String managerPublicUserId, int page, int limit);
}
