package ir.mohrazzr.dev.rothis.server.api.billing.service.service.iservice;

import ir.mohrazzr.dev.rothis.server.api.billing.service.DTO.BankAccountOwnersDTO;

import java.util.List;

/**
 * Created by IntelliJ IDEA at Friday in 2019/11/22 - 4:41 PM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : Rothis
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Email : mhrz.dev@gmail.com
 * Class Name          : IBankAccountOwnersService
 **/

public interface IBankAccountOwnersService {
    BankAccountOwnersDTO    setOwnerToAccount(String managerPublicUserId, String bankAccountPublicId, String resourcePublicId, String ownerPublicUserId);
    List<BankAccountOwnersDTO> readAllOwnerOfAccount(String managerPublicUserId, String resourcePublicId, String bankAccountPublicId);
    BankAccountOwnersDTO updateOwnerToAccount(String managerPublicUserId, String oldOwnerPublicId, String newOwnerPublicId, String bankAccountPublicId, String resourcePublicId);
}
