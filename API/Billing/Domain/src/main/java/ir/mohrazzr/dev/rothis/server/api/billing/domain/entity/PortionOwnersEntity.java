package ir.mohrazzr.dev.rothis.server.api.billing.domain.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "tbl_portion_owners")
@Data
public class PortionOwnersEntity implements Serializable {
    private static final long serialVersionUID = -3181142723671811940L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", insertable = false, nullable = false)
    private Long id;
    @Column(name = "public_id")
    private Long publicId;
    @Column(name = "resource_id", nullable = false)
    private Long resourceId;
    @Column(name = "person_id", nullable = false)
    private Long personId;
    @Column(name = "portion_identifier_code", nullable = false)
    private String portionIdentifierCode;
    @Column(name = "portion_value", nullable = false)
    private Float portionValue;
    @Column(name = "portion_seconds", nullable = false)
    private Long portionSeconds;
}