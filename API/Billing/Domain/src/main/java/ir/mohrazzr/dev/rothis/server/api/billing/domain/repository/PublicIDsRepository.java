package ir.mohrazzr.dev.rothis.server.api.billing.domain.repository;

import ir.mohrazzr.dev.rothis.server.api.billing.domain.entity.PublicIDsEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PublicIDsRepository extends CrudRepository<PublicIDsEntity, Long> {
    PublicIDsEntity findByPublicId(String id);
    Optional<PublicIDsEntity> findById(Long publicUserId);
    void deleteById(Long id);
}