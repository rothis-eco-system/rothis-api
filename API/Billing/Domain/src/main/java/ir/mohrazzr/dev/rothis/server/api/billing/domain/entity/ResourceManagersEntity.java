package ir.mohrazzr.dev.rothis.server.api.billing.domain.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "tbl_resource_managers")
@Data
public class ResourceManagersEntity implements Serializable {
  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id", insertable = false, nullable = false)
  private Long id;

  @Column(name = "person_id", nullable = false)
  private Long personId;

  @Column(name = "resource_id", nullable = false)
  private Long resourceId;

}