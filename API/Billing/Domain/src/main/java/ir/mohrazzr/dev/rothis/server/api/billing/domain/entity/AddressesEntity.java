package ir.mohrazzr.dev.rothis.server.api.billing.domain.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Table(name = "tbl_addresses")
@Data
@Entity
public class AddressesEntity implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", insertable = false, nullable = false)
    private Long id;
    @Column(name = "persons_id")
    private Long personsId;
    @Column(name = "public_id")
    private Long publicId;
    @Column(name = "is_primary",nullable = false)
    private Boolean isPrimary;
    @Column(name = "city_code", nullable = false)
    private Integer cityCode;
    @Column(name = "postal_code")
    private String postalCode = "NULL";
    @Column(name = "address")
    private String address = "NULL";

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPersonsId() {
        return personsId;
    }

    public void setPersonsId(Long personsId) {
        this.personsId = personsId;
    }

    public Boolean getPrimary() {
        return isPrimary;
    }

    public void setPrimary(Boolean primary) {
        isPrimary = primary;
    }

    public Integer getCityCode() {
        return cityCode;
    }

    public void setCityCode(Integer cityCode) {
        this.cityCode = cityCode;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}