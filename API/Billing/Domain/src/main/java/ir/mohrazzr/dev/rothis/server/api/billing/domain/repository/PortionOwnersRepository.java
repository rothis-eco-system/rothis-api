package ir.mohrazzr.dev.rothis.server.api.billing.domain.repository;

import ir.mohrazzr.dev.rothis.server.api.billing.domain.entity.PortionOwnersEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PortionOwnersRepository extends PagingAndSortingRepository<PortionOwnersEntity, Long>, JpaSpecificationExecutor<PortionOwnersEntity> {
    PortionOwnersEntity findByPersonIdAndResourceIdAndPublicId(Long personId, Long resourceId, Long publicId);
    PortionOwnersEntity findByPersonIdAndResourceId(Long ownerPersonId, Long resourceId);
    Page<PortionOwnersEntity> findAllByResourceId(Long resourceId, Pageable pageable);
    Integer countByResourceId(Long id);
}