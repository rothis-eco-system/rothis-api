package ir.mohrazzr.dev.rothis.server.api.billing.domain.repository;

import ir.mohrazzr.dev.rothis.server.api.billing.domain.entity.AddressesEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AddressesRepository extends JpaRepository<AddressesEntity, Integer>, JpaSpecificationExecutor<AddressesEntity> {
    List<AddressesEntity> findAllByPersonsId(Long personId);
    AddressesEntity findByPersonsIdAndPublicId(Long personId, Long publicId);
    AddressesEntity findByPublicId(Long publicId);
}