package ir.mohrazzr.dev.rothis.server.api.billing.domain.repository;

import ir.mohrazzr.dev.rothis.server.api.billing.domain.entity.UsersEntity;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UsersRepository extends PagingAndSortingRepository<UsersEntity,Long> {
      UsersEntity findByUserName(String userName);
      Optional<UsersEntity> findById(Long id);
      UsersEntity findByPublicId(Long id);
}