package ir.mohrazzr.dev.rothis.server.api.billing.domain.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;

@Table(name = "tbl_resource_bills")
@Entity
@Data
public class ResourceBillsEntity implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", insertable = false, nullable = false)
    private Long id;
    @Column(name = "public_id")
    private Long publicId;
    @Column(name = "resource_id", nullable = false)
    private Long resourceId;
    @Column(name = "bank_account_id", nullable = false)
    private Long bankAccountId;
    @Column(name = "exporter_manager_id")
    private Long exporterManagerId;
    @Column(name = "resource_bill_identity_code",nullable = false,length = 8)
    private String resourceBillIdentityCode;
    @Column(name = "total_price", nullable = false)
    private BigDecimal totalPrice;
    @Column(name = "price_per_second", nullable = false)
    private BigDecimal pricePerSecond;
    @Column(name = "bill_count")
    private Long billCount;
    @Column(name = "round_count", nullable = false)
    private Integer roundCount;
    @Column(name = "export_time", nullable = false)
    private Timestamp exportTime;
    @Column(name = "payment_dead_line")
    private Date paymentDeadLine;
    @Column(name = "concern", nullable = false)
    private String concern;
    @Column(name = "descriptions")
    private String descriptions = "NULL";
}