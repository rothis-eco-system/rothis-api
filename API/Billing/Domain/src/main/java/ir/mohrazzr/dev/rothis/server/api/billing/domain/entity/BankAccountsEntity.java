package ir.mohrazzr.dev.rothis.server.api.billing.domain.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Table(name = "tbl_bank_accounts")
@Entity
@Data
public class BankAccountsEntity implements Serializable {
    private static final long serialVersionUID = 3799721977434328665L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", insertable = false, nullable = false)
    private Long id;
    @Column(name = "public_id")
    private Long publicId;
    @Column(name = "resource_id", nullable = false)
    private Long resourceId;
    @Column(name = "bank_name", nullable = false)
    private String bankName;
    @Column(name = "account_number", nullable = false)
    private String accountNumber;
    @Column(name = "name", nullable = false)
    private String name;
    @Column(name = "shaba_number")
    private String shabaNumber = "NULL";
    @Column(name = "card_number")
    private String cardNumber = "NULL";
    @Column(name = "branch_code")
    private String branchCode = "NULL";
    @Column(name = "branch_name")
    private String branchName = "NULL";
    @Column(name = "branch_address")
    private String branchAddress = "NULL";
    @Column(name = "description")
    private String description="NULL";
}