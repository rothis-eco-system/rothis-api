package ir.mohrazzr.dev.rothis.server.api.billing.domain.repository;

import ir.mohrazzr.dev.rothis.server.api.billing.domain.entity.ResourcesEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface ResourcesRepository extends JpaRepository<ResourcesEntity, Long>, JpaSpecificationExecutor<ResourcesEntity> {
    ResourcesEntity findResourceByPublicId(Long publicId);
    ResourcesEntity findResourceByIdentifierCode(String identifierCode);
}