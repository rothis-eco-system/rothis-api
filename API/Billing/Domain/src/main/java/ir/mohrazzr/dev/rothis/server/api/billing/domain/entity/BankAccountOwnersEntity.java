package ir.mohrazzr.dev.rothis.server.api.billing.domain.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "tbl_bank_account_owners")
@Data
public class BankAccountOwnersEntity implements Serializable {
  private static final long serialVersionUID = 1L;
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id", insertable = false, nullable = false)
  private Long id;
  @Column(name = "bank_account_id", nullable = false)
  private Long bankAccountId;
  @Column(name = "user_id", nullable = false)
  private Long userId;
}