package ir.mohrazzr.dev.rothis.server.api.billing.domain.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "tbl_resources")
@Data
public class ResourcesEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", insertable = false, nullable = false)
    private Long id;
    @Column(name = "public_id")
    private Long publicId;
    @Column(name = "name", nullable = false)
    private String name;
    @Column(name = "identifier_code", unique = true)
    private String identifierCode;
    @Column(name = "resource_address_id")
    private Long resourceAddressId;
    @Column(name = "total_resource_portion", nullable = false)
    private Float totalResourcePortion;
    @Column(name = "total_unallocated_resource_portion", nullable = false)
    private Float totalUnallocatedResourcePortion;
    @Column(name = "dong_value", nullable = false)
    private Integer dongValue;
    @Column(name = "total_resource_seconds", nullable = false)
    private Long totalResourceSeconds;
    @Column(name = "resource_seconds_per_portion", nullable = false)
    private Long resourceSecondsPerPortion;
    @Column(name = "circuit_of_the_day", nullable = false)
    private Integer circuitOfTheDay;
}