package ir.mohrazzr.dev.rothis.server.api.billing.domain.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity(name = "roles")
@Data
@Table(name = "tbl_roles")
public class RolesEntity implements Serializable {
  private static final long serialVersionUID = 1L;
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id", insertable = false, nullable = false)
  private Integer id;
  @Column(name = "role_name", nullable = false)
  private String roleName;

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getRoleName() {
    return roleName;
  }

  public void setRoleName(String roleName) {
    this.roleName = roleName;
  }
}