package ir.mohrazzr.dev.rothis.server.api.billing.domain.repository;

import ir.mohrazzr.dev.rothis.server.api.billing.domain.entity.PersonsEntity;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonsRepository extends PagingAndSortingRepository<PersonsEntity, Long>, JpaSpecificationExecutor<PersonsEntity> {
    PersonsEntity findByUserId(Long userId);
    PersonsEntity findByNationalCode(String nationalCode);
}