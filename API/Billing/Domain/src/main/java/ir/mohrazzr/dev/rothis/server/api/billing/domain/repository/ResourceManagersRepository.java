package ir.mohrazzr.dev.rothis.server.api.billing.domain.repository;

import ir.mohrazzr.dev.rothis.server.api.billing.domain.entity.ResourceManagersEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ResourceManagersRepository extends JpaRepository<ResourceManagersEntity, Long>, JpaSpecificationExecutor<ResourceManagersEntity> {
    Boolean existsByPersonIdAndResourceId(Long personId, Long resourceId);
    ResourceManagersEntity findByPersonIdAndResourceId(Long personId, Long resourceId);
    List<ResourceManagersEntity> findAllByPersonId(Long personId);
}