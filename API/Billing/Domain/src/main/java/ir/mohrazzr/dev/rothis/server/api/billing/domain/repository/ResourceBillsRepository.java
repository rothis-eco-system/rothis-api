package ir.mohrazzr.dev.rothis.server.api.billing.domain.repository;

import ir.mohrazzr.dev.rothis.server.api.billing.domain.entity.ResourceBillsEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ResourceBillsRepository extends PagingAndSortingRepository<ResourceBillsEntity, Long>, JpaSpecificationExecutor<ResourceBillsEntity> {
    List<ResourceBillsEntity> findAllByResourceId(Long resourceId);
    Long countByResourceId(Long resourceId);
    Integer countByExporterManagerId(Long exporterManagerId);
    ResourceBillsEntity findByResourceIdAndPublicId(Long resourceId, Long publicId);
    ResourceBillsEntity findByResourceIdAndResourceBillIdentityCode(Long resourceId, String resourceBillIdentityCode);
    Page<ResourceBillsEntity> findAllByResourceId(Long resourceId, Pageable pageable);
}