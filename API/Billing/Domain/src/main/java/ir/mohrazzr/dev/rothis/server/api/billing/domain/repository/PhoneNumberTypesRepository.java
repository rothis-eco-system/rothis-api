package ir.mohrazzr.dev.rothis.server.api.billing.domain.repository;

import ir.mohrazzr.dev.rothis.server.api.billing.domain.entity.PhoneNumberTypesEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface PhoneNumberTypesRepository extends JpaRepository<PhoneNumberTypesEntity, Integer>, JpaSpecificationExecutor<PhoneNumberTypesEntity> {

}