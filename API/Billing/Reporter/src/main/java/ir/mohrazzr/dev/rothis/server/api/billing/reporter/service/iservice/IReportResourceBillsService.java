package ir.mohrazzr.dev.rothis.server.api.billing.reporter.service.iservice;

import ir.mohrazzr.dev.rothis.server.api.billing.reporter.model.ResourceBillsReportResponse;

/**
 * Created by IntelliJ IDEA at Tuesday in 2019/12/31 - 10:40 AM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : Rothis
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Email : mhrz.dev@gmail.com
 * Class Name          : IReportResourceBillsService
 **/

public interface IReportResourceBillsService {
    ResourceBillsReportResponse generateResourceBillsListReport(String resourcePublicId, String managerPublicUserId, String resourceBillPublicId, String resourceBillIdentityCode, String reportTemplatesPath, String generatedReportPath, String billIdentityCode, String message);
}
