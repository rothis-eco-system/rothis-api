package ir.mohrazzr.dev.rothis.server.api.billing.reporter.model;

/**
 * Created by IntelliJ IDEA at Saturday in 2020/01/11 - 10:19 AM
 * Copyright (c) 2020 MoHRaZ
 * Project Name        : Rothis
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Email : mhrz.dev@gmail.com
 * Class Name          : ResourceBillsReportResponse
 **/

public class ResourceBillsReportResponse {
    private String generatedPath;

    public ResourceBillsReportResponse() {
    }

    public ResourceBillsReportResponse(String generatedPath) {
        this.generatedPath = generatedPath;
    }

    public String getGeneratedPath() {
        return generatedPath;
    }

    public void setGeneratedPath(String generatedPath) {
        this.generatedPath = generatedPath;
    }

    @Override
    public String toString() {
        return "ResourceBillsReportResponse{" +
                "generatedPath='" + generatedPath + '\'' +
                '}';
    }
}
