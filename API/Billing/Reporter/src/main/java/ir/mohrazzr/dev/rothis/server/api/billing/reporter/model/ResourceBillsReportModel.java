package ir.mohrazzr.dev.rothis.server.api.billing.reporter.model;

import java.math.BigDecimal;

/**
 * Created by IntelliJ IDEA at Wednesday in 2020/01/01 - 9:47 AM
 * Copyright (c) 2020 MoHRaZ
 * Project Name        : Rothis
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Email : mhrz.dev@gmail.com
 * Class Name          : ResourceBillsReportModel
 **/

public class ResourceBillsReportModel {
    //Resource bill details
    private String resourceName;
    private String resourceBillIdentityCode;
    private String paymentDeadLineDays;
    private String paymentDeadLine;
    private String paymentDeadLinePersianDate;
    private String totalPrice;
    private BigDecimal pricePerSecond;
    private BigDecimal pricePerPortion;
    private String totalPriceInAlphabet;
    private String pricePerPortionInAlphabet;
    private Long billCount;
    private Integer roundCount;
    private String roundCountInAlphabet;
    private String exportTime;
    private String exportTimePersianDate;
    private String concern;
    private String descriptions;
    private String resourceBillMessage;
    private Integer circuitOfTheDay;
    //Bills details
    private String createdAt;
    private String createdAtPersianDate;
    private BigDecimal billPrice;
    private String billPriceInAlphabet;
    private String billIdentityCode;
    private Integer billNumber;
    private Float ownerPortionValue;
    private String ownerPortionValueDayLightTime;
    private String sumDayLightTime;
    private BigDecimal sumBillPrice;
    private Float sumOwnerPortionValue;
    //Owner details
    private String userName;
    private String firstName;
    private String lastName;
    private String fatherName;
    private String nationalCode;
    private String passPortCode;
    private String gender;
    private String dateOfBirth;
    //Owner address details
    private String postalCode;
    private String address;
    //Owner phone number details
    private String phoneNumber;
    //Resource bank account details
    private String bankName;
    private String accountNumber;
    private String accountName;
    private String shabaNumber;
    private String cardNumber;
    private String branchCode;
    private String branchName;
    private String branchAddress;
    private String description;
    //exporter name
    private String exporterNameFamily;
    
    public ResourceBillsReportModel() {
    }

    public ResourceBillsReportModel(String resourceName, String resourceBillIdentityCode, String paymentDeadLineDays, String paymentDeadLine, String paymentDeadLinePersianDate, String totalPrice, BigDecimal pricePerSecond, BigDecimal pricePerPortion, String totalPriceInAlphabet, String pricePerPortionInAlphabet, Long billCount, Integer roundCount, String roundCountInAlphabet, String exportTime, String exportTimePersianDate, String concern, String descriptions, String resourceBillMessage, Integer circuitOfTheDay, String createdAt, String createdAtPersianDate, BigDecimal billPrice, String billPriceInAlphabet, String billIdentityCode, Integer billNumber, Float ownerPortionValue, String ownerPortionValueDayLightTime, String sumDayLightTime, BigDecimal sumBillPrice, Float sumOwnerPortionValue, String userName, String firstName, String lastName, String fatherName, String nationalCode, String passPortCode, String gender, String dateOfBirth, String postalCode, String address, String phoneNumber, String bankName, String accountNumber, String accountName, String shabaNumber, String cardNumber, String branchCode, String branchName, String branchAddress, String description, String exporterNameFamily) {
        this.resourceName = resourceName;
        this.resourceBillIdentityCode = resourceBillIdentityCode;
        this.paymentDeadLineDays = paymentDeadLineDays;
        this.paymentDeadLine = paymentDeadLine;
        this.paymentDeadLinePersianDate = paymentDeadLinePersianDate;
        this.totalPrice = totalPrice;
        this.pricePerSecond = pricePerSecond;
        this.pricePerPortion = pricePerPortion;
        this.totalPriceInAlphabet = totalPriceInAlphabet;
        this.pricePerPortionInAlphabet = pricePerPortionInAlphabet;
        this.billCount = billCount;
        this.roundCount = roundCount;
        this.roundCountInAlphabet = roundCountInAlphabet;
        this.exportTime = exportTime;
        this.exportTimePersianDate = exportTimePersianDate;
        this.concern = concern;
        this.descriptions = descriptions;
        this.resourceBillMessage = resourceBillMessage;
        this.circuitOfTheDay = circuitOfTheDay;
        this.createdAt = createdAt;
        this.createdAtPersianDate = createdAtPersianDate;
        this.billPrice = billPrice;
        this.billPriceInAlphabet = billPriceInAlphabet;
        this.billIdentityCode = billIdentityCode;
        this.billNumber = billNumber;
        this.ownerPortionValue = ownerPortionValue;
        this.ownerPortionValueDayLightTime = ownerPortionValueDayLightTime;
        this.sumDayLightTime = sumDayLightTime;
        this.sumBillPrice = sumBillPrice;
        this.sumOwnerPortionValue = sumOwnerPortionValue;
        this.userName = userName;
        this.firstName = firstName;
        this.lastName = lastName;
        this.fatherName = fatherName;
        this.nationalCode = nationalCode;
        this.passPortCode = passPortCode;
        this.gender = gender;
        this.dateOfBirth = dateOfBirth;
        this.postalCode = postalCode;
        this.address = address;
        this.phoneNumber = phoneNumber;
        this.bankName = bankName;
        this.accountNumber = accountNumber;
        this.accountName = accountName;
        this.shabaNumber = shabaNumber;
        this.cardNumber = cardNumber;
        this.branchCode = branchCode;
        this.branchName = branchName;
        this.branchAddress = branchAddress;
        this.description = description;
        this.exporterNameFamily = exporterNameFamily;
    }

    public String getResourceBillIdentityCode() {
        return resourceBillIdentityCode;
    }

    public void setResourceBillIdentityCode(String resourceBillIdentityCode) {
        this.resourceBillIdentityCode = resourceBillIdentityCode;
    }

    public String getPaymentDeadLineDays() {
        return paymentDeadLineDays;
    }

    public void setPaymentDeadLineDays(String paymentDeadLineDays) {
        this.paymentDeadLineDays = paymentDeadLineDays;
    }

    public String getPaymentDeadLine() {
        return paymentDeadLine;
    }

    public void setPaymentDeadLine(String paymentDeadLine) {
        this.paymentDeadLine = paymentDeadLine;
    }

    public String getPaymentDeadLinePersianDate() {
        return paymentDeadLinePersianDate;
    }

    public void setPaymentDeadLinePersianDate(String paymentDeadLinePersianDate) {
        this.paymentDeadLinePersianDate = paymentDeadLinePersianDate;
    }

    public String getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(String totalPrice) {
        this.totalPrice = totalPrice;
    }

    public BigDecimal getPricePerSecond() {
        return pricePerSecond;
    }

    public void setPricePerSecond(BigDecimal pricePerSecond) {
        this.pricePerSecond = pricePerSecond;
    }

    public BigDecimal getPricePerPortion() {
        return pricePerPortion;
    }

    public void setPricePerPortion(BigDecimal pricePerPortion) {
        this.pricePerPortion = pricePerPortion;
    }

    public String getTotalPriceInAlphabet() {
        return totalPriceInAlphabet;
    }

    public void setTotalPriceInAlphabet(String totalPriceInAlphabet) {
        this.totalPriceInAlphabet = totalPriceInAlphabet;
    }

    public String getPricePerPortionInAlphabet() {
        return pricePerPortionInAlphabet;
    }

    public void setPricePerPortionInAlphabet(String pricePerPortionInAlphabet) {
        this.pricePerPortionInAlphabet = pricePerPortionInAlphabet;
    }

    public Long getBillCount() {
        return billCount;
    }

    public void setBillCount(Long billCount) {
        this.billCount = billCount;
    }

    public Integer getRoundCount() {
        return roundCount;
    }

    public void setRoundCount(Integer roundCount) {
        this.roundCount = roundCount;
    }

    public String getRoundCountInAlphabet() {
        return roundCountInAlphabet;
    }

    public void setRoundCountInAlphabet(String roundCountInAlphabet) {
        this.roundCountInAlphabet = roundCountInAlphabet;
    }

    public String getExportTime() {
        return exportTime;
    }

    public void setExportTime(String exportTime) {
        this.exportTime = exportTime;
    }

    public String getExportTimePersianDate() {
        return exportTimePersianDate;
    }

    public void setExportTimePersianDate(String exportTimePersianDate) {
        this.exportTimePersianDate = exportTimePersianDate;
    }

    public String getConcern() {
        return concern;
    }

    public void setConcern(String concern) {
        this.concern = concern;
    }

    public String getDescriptions() {
        return descriptions;
    }

    public void setDescriptions(String descriptions) {
        this.descriptions = descriptions;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getCreatedAtPersianDate() {
        return createdAtPersianDate;
    }

    public void setCreatedAtPersianDate(String createdAtPersianDate) {
        this.createdAtPersianDate = createdAtPersianDate;
    }

    public BigDecimal getBillPrice() {
        return billPrice;
    }

    public void setBillPrice(BigDecimal billPrice) {
        this.billPrice = billPrice;
    }

    public String getBillPriceInAlphabet() {
        return billPriceInAlphabet;
    }

    public void setBillPriceInAlphabet(String billPriceInAlphabet) {
        this.billPriceInAlphabet = billPriceInAlphabet;
    }

    public String getBillIdentityCode() {
        return billIdentityCode;
    }

    public void setBillIdentityCode(String billIdentityCode) {
        this.billIdentityCode = billIdentityCode;
    }

    public Integer getBillNumber() {
        return billNumber;
    }

    public void setBillNumber(Integer billNumber) {
        this.billNumber = billNumber;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFatherName() {
        return fatherName;
    }

    public void setFatherName(String fatherName) {
        this.fatherName = fatherName;
    }

    public String getNationalCode() {
        return nationalCode;
    }

    public void setNationalCode(String nationalCode) {
        this.nationalCode = nationalCode;
    }

    public String getPassPortCode() {
        return passPortCode;
    }

    public void setPassPortCode(String passPortCode) {
        this.passPortCode = passPortCode;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String name) {
        this.accountName = name;
    }

    public String getShabaNumber() {
        return shabaNumber;
    }

    public void setShabaNumber(String shabaNumber) {
        this.shabaNumber = shabaNumber;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getBranchCode() {
        return branchCode;
    }

    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String getBranchAddress() {
        return branchAddress;
    }

    public void setBranchAddress(String branchAddress) {
        this.branchAddress = branchAddress;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getExporterNameFamily() {
        return exporterNameFamily;
    }

    public void setExporterNameFamily(String exporterNameFamily) {
        this.exporterNameFamily = exporterNameFamily;
    }

    public String getResourceName() {
        return resourceName;
    }

    public void setResourceName(String resourceName) {
        this.resourceName = resourceName;
    }

    public Float getOwnerPortionValue() {
        return ownerPortionValue;
    }

    public void setOwnerPortionValue(Float ownerPortionValue) {
        this.ownerPortionValue = ownerPortionValue;
    }

    public String getOwnerPortionValueDayLightTime() {
        return ownerPortionValueDayLightTime;
    }

    public void setOwnerPortionValueDayLightTime(String ownerPortionValueDayLightTime) {
        this.ownerPortionValueDayLightTime = ownerPortionValueDayLightTime;
    }

    public String getSumDayLightTime() {
        return sumDayLightTime;
    }

    public void setSumDayLightTime(String sumDayLightTime) {
        this.sumDayLightTime = sumDayLightTime;
    }

    public Float getSumOwnerPortionValue() {
        return sumOwnerPortionValue;
    }

    public void setSumOwnerPortionValue(Float sumOwnerPortionValue) {
        this.sumOwnerPortionValue = sumOwnerPortionValue;
    }

    public BigDecimal getSumBillPrice() {
        return sumBillPrice;
    }

    public void setSumBillPrice(BigDecimal sumBillPrice) {
        this.sumBillPrice = sumBillPrice;
    }

    public String getResourceBillMessage() {
        return resourceBillMessage;
    }

    public void setResourceBillMessage(String resourceBillMessage) {
        this.resourceBillMessage = resourceBillMessage;
    }

    public Integer getCircuitOfTheDay() {
        return circuitOfTheDay;
    }

    public void setCircuitOfTheDay(Integer circuitOfTheDay) {
        this.circuitOfTheDay = circuitOfTheDay;
    }
}
