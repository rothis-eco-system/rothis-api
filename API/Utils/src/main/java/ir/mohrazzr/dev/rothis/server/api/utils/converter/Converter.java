package ir.mohrazzr.dev.rothis.server.api.utils.converter;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Optional;
import java.util.Spliterator;
import java.util.concurrent.TimeUnit;

/**
 * Created by IntelliJ IDEA at Friday in 2019/11/29 - 12:17 AM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : Rothis
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Email : mhrz.dev@gmail.com
 * Class Name          : Converter
 **/
@Component
public class Converter extends NumbersToPersianAlphabets {
    public String calculateTimeToAlphabet(long seconds) {
        Time time = new Time(seconds).invoke();
        ArrayList<String> arrayList = new ArrayList<>();
        if (time.getDay() != 0) {
            arrayList.add(getParsedString(String.valueOf(time.getDay())).concat(" روز "));
        }

        if (time.getHours() != 0) {
            arrayList.add(getParsedString(String.valueOf(time.getHours())).concat(" ساعت "));
        }

        if (time.getMinute() != 0) {
            arrayList.add(getParsedString(String.valueOf(time.getMinute())).concat(" دقیقه "));
        }

        if (time.getSecond() != 0) {
            arrayList.add(getParsedString(String.valueOf(time.getSecond())).concat(" ثانیه "));
        }

        Spliterator<String> splitter = arrayList.spliterator();

        ArrayList<String> resultList = new ArrayList<>();

        splitter.forEachRemaining(s -> resultList.add(s.concat(" و ")));

        return Optional.of(Arrays.toString(resultList.toArray()))
                .filter(sStr -> sStr.length() != 0)
                .map(sStr -> sStr.substring(0, sStr.length() - 3))
                .orElse(Arrays.toString(resultList.toArray()));
    }

    public String calculateDayLightTime(long seconds) {
        Time time = new Time(seconds).invoke();
        return ("( " + time.getDay() + " ) " + time.getClock());

    }

    public String calculateTimeToMinutesAlphabet(Long seconds) {
        return getParsedString(String.valueOf(TimeUnit.SECONDS.toMinutes(seconds))) + " دقیقه ";
    }

    public long calculateTimeToMinutes(Long seconds) {
        return TimeUnit.SECONDS.toMinutes(seconds);
    }

    public String convertEnglishNumbersToFarsi(String input) {
        String[][] mChars = new String[][]{
                {"0", "۰"},
                {"1", "۱"},
                {"2", "۲"},
                {"3", "۳"},
                {"4", "۴"},
                {"5", "۵"},
                {"6", "۶"},
                {"7", "۷"},
                {"8", "۸"},
                {"9", "۹"},
                { ".", "٫"}
        };

        for (String[] num : mChars) {

            input = input.replace(num[0], num[1]);

        }

        return input;
    }

    public String convertFarsiNumbersToEnglish(String input) {
        String[][] mChars = new String[][]{
                {"۰", "0"},
                {"۱", "1"},
                {"۲", "2"},
                {"۳", "3"},
                {"۴", "4"},
                {"۵", "5"},
                {"۶", "6"},
                {"۷", "7"},
                {"۸", "8"},
                {"۹", "9"},
                {"٫", "."}
        };

        for (String[] num : mChars) {

            input = input.replace(num[0], num[1]);

        }

        return input;
    }

    private class Time {
        private long seconds;
        private int day;
        private long hours;
        private long minute;
        private long second;

        Time(long seconds) {
            this.seconds = seconds;
        }

        int getDay() {
            return day;
        }

        long getHours() {
            return hours;
        }

        long getMinute() {
            return minute;
        }

        long getSecond() {
            return second;
        }

        String getClock() {
            return getHours() + ":" + getMinute() + ":" + getSecond();
        }

        Time invoke() {
            day = (int) TimeUnit.SECONDS.toDays(seconds);
            hours = TimeUnit.SECONDS.toHours(seconds) - (day * 24);
            minute = TimeUnit.SECONDS.toMinutes(seconds) - (TimeUnit.SECONDS.toHours(seconds) * 60);
            second = TimeUnit.SECONDS.toSeconds(seconds) - (TimeUnit.SECONDS.toMinutes(seconds) * 60);
            return this;
        }
    }
}
