package ir.mohrazzr.dev.rothis.server.api.utils.calculator;

import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.MathContext;

/**
 * Created by IntelliJ IDEA at Friday in 2019/11/15 - 12:07 PM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : Rothis
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Email : mhrz.dev@gmail.com
 * Class Name          : ResourcesCalculation
 **/
@Component
public class ResourcesCalculation {
    //setTotalResourcePortion(dong * 16)
    //setTotalResourceMilliseconds(portionValue * 24 * 60 * 60 * 60);
    //setTotalResourceSeconds(portionValue * 24 * 60 * 60);
    //setTotalUnallocatedResourcePortion(lastTotalAllocatedValue - currentValue)
    //setUpdatedTotalUnallocatedResourcePortion(lastTotalAllocatedValue + lastValue - currentValue)
    public static Float getTotalResourcePortion(Integer dongValue) {
        return (float) dongValue * 16;
    }

    public static Long getTotalResourceSeconds(Integer circuitOfDay) {
        Integer seconds = circuitOfDay * 24 * 60 * 60;
        return seconds.longValue();
    }

    public static Long getResourceSecondsPerPortion(Long totalSeconds, Long totalPortion) {
        return totalSeconds / totalPortion;
    }

    public static Long getPortionSeconds(Float portionValue, Long secondsPerPortion) {
        Float seconds = portionValue * secondsPerPortion;
        return seconds.longValue();
    }

    public static Float getTotalUnallocatedResourcePortion(Float lastTotalAllocatedValue, Float currentValue) {
        return lastTotalAllocatedValue - currentValue;
    }

    public static Float getTotalUnallocatedResourcePortion(Float lastTotalAllocatedValue, Float lastValue, Float currentValue) {
        if (!lastValue.equals(currentValue)) {
            float restValue = lastTotalAllocatedValue + lastValue;
            return (restValue - currentValue);
        } else {
            return lastTotalAllocatedValue;
        }
    }

    public static BigDecimal getPricePerPortion(Long portionSeconds, BigDecimal pricePerPortion) {
        return pricePerPortion.divide(new BigDecimal(portionSeconds), MathContext.DECIMAL128);
    }

    public static BigDecimal getPortionOwnerTotalPrice(BigDecimal pricePerPortion, Long portionSeconds) {
        return new BigDecimal(portionSeconds).multiply(pricePerPortion,MathContext.DECIMAL128);
    }
}
