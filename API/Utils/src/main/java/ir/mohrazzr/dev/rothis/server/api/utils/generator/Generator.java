package ir.mohrazzr.dev.rothis.server.api.utils.generator;

import org.springframework.stereotype.Component;

import java.security.SecureRandom;
import java.util.Random;

/**
 * Created by IntelliJ IDEA at Friday in 2019/11/01 - 4:07 PM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : Rothis
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Email : mhrz.dev@gmail.com
 * Class Name          : PublicIdGenerator
 **/
@Component
public class Generator {
    private final Random RANDOM = new SecureRandom();
    private static final String UPPER_ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private static final String LOWER_ALPHABET = "abcdefghijklmnopqrstuvxyz";
    private static final String NUMBERS = "123456789";


    private String getAlphaNumericString(int n) {
        StringBuilder result = new StringBuilder(n);
        for (int i = 0; i < n; i++) {
            String ALPHANUMERIC = UPPER_ALPHABET + LOWER_ALPHABET + NUMBERS;
            result.append(ALPHANUMERIC.charAt(RANDOM.nextInt(ALPHANUMERIC.length())));
        }
        return new String(result);
    }


    private String getRandomAlphabetString(int n) {
        return getRandom(n, LOWER_ALPHABET);
    }

    private String getRandomNumericString(int n) {
        return getRandom(n, NUMBERS);
    }

    private String getRandom(int n, String strings) {
        StringBuilder result = new StringBuilder(n);
        for (int i = 0; i < n; i++) {
            result.append(strings.charAt(RANDOM.nextInt(strings.length())));
        }
        return new String(result);
    }

    public String generateUserName(int alphabetsLength, int numbersLength) {
        return getRandomAlphabetString(alphabetsLength).toLowerCase() + getRandomNumericString(numbersLength);
    }

    public String generatePublicId(int length) {
        return getAlphaNumericString(length);
    }

    public String generateRandomNumber(int i) {
        return getRandomNumericString(i);
    }
}
