package ir.mohrazzr.dev.rothis.server.api.utils.times;

import com.github.mfathi91.time.PersianDate;

import java.sql.Date;
import java.sql.Timestamp;

/**
 * Created by IntelliJ IDEA at Friday in 2019/11/29 - 5:41 PM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : Rothis
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Email : mhrz.dev@gmail.com
 * Class Name          : TimeProfile
 **/

public class TimeProfile {
    private Integer paymentDeadLineDays;
    private PersianDate persianDate;
    private Timestamp timestamp;
    private Date date;

    public TimeProfile() {
    }

    public TimeProfile(PersianDate persianDate) {
        this.persianDate = persianDate;
    }

    public TimeProfile(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    public TimeProfile(Date date) {
        this.date = date;
    }

    public TimeProfile(Integer paymentDeadLineDays, PersianDate persianDate, Timestamp timestamp, Date date) {
        this.paymentDeadLineDays = paymentDeadLineDays;
        this.persianDate = persianDate;
        this.timestamp = timestamp;
        this.date = date;
    }

    public TimeProfile(PersianDate persianDate, Timestamp timestamp) {
        this(null,persianDate,timestamp,null);
    }

    public PersianDate getPersianDate() {
        return persianDate;
    }

    public void setPersianDate(PersianDate persianDate) {
        this.persianDate = persianDate;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    public Integer getPaymentDeadLineDays() {
        return paymentDeadLineDays;
    }

    public void setPaymentDeadLineDays(Integer paymentDeadLineDays) {
        this.paymentDeadLineDays = paymentDeadLineDays;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
