package ir.mohrazzr.dev.rothis.server.api.utils.times;

import com.github.mfathi91.time.PersianDate;
import org.springframework.expression.ParseException;
import org.springframework.stereotype.Component;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Created by IntelliJ IDEA at Friday in 2019/11/29 - 11:53 AM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : Rothis
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Email : mhrz.dev@gmail.com
 * Class Name          : TimeUtil
 **/
@Component
public class TimeUtil extends TimeProfile {

    public TimeUtil() {
    }

    public TimeUtil(PersianDate persianDate) {
        super(persianDate);
    }

    public TimeUtil(Timestamp timestamp) {
        super(timestamp);
    }

    public TimeUtil(PersianDate persianDate, Timestamp timestamp) {
        super(persianDate, timestamp);
    }

    public boolean isDateExpired(String input, Date expiration) throws ParseException, java.text.ParseException {
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        java.util.Date date = dateFormat.parse(input);
        return date.after(expiration);
    }

    public TimeProfile convertTimestampToPersianDate() {
        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        gregorianCalendar.setTimeInMillis(getTimestamp().getTime());
        PersianDate persianDate = PersianDate.fromGregorian(gregorianCalendar.toZonedDateTime().toLocalDate());
        return new TimeProfile(persianDate);
    }

    public TimeProfile convertDateToPersianDate() {
        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        gregorianCalendar.setTimeInMillis(getDate().getTime());
        PersianDate persianDate = PersianDate.fromGregorian(gregorianCalendar.toZonedDateTime().toLocalDate());
        return new TimeProfile(persianDate);
    }

    public Date getPaymentDeadLine() {
        Integer days = getPaymentDeadLineDays();
        Timestamp exportTime = getTimestamp();
        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        gregorianCalendar.setTimeInMillis(exportTime.getTime());
        gregorianCalendar.add(Calendar.DAY_OF_MONTH, days);
        return Date.valueOf(gregorianCalendar.toZonedDateTime().toLocalDate());

    }


}
